//
//  AppDelegate.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-13.
//  Copyright © 2015 atasoft. All rights reserved.
// Push fix?

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarDelegate {

    var window: UIWindow?
    static let rotateLockWidth = CGFloat(375)


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //self.window!.rootViewController = ToolboxTabBarController();
        //self.window!.makeKeyAndVisible()
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        PauseFlags.cashCounter = .Paused
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        PauseFlags.cashCounter = .UnPaused
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}


/*
// Don't know where else to put this
extension UITabBarController {
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIScreen.mainScreen().bounds.height < AppDelegate.rotateLockWidth ||
            UIScreen.mainScreen().bounds.width < AppDelegate.rotateLockWidth {
            return UIInterfaceOrientationMask.Landscape
            
        } else {
            return UIInterfaceOrientationMask.AllButUpsideDown
        }
    }
}
*/


/*
extension UINavigationController {
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if visibleViewController != nil {
            return visibleViewController!.supportedInterfaceOrientations()
        }
        return UIInterfaceOrientationMask.All
    }
    
}
*/

