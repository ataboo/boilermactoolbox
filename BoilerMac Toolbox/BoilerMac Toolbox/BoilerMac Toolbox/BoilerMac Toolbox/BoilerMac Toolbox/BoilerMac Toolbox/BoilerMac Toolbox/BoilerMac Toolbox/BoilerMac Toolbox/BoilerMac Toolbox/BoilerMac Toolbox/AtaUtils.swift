
//
//  AtaMathUtils.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-27.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

public class AtaUtils{
    public static func clamp(val: Float, floor: Float, ceiling: Float) -> Float {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
    
    public static func clamp(val: Double, floor: Double, ceiling: Double) -> Double {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
    
    public static func clampInt(val: Int, floor: Int, ceiling: Int) -> Int {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
}
