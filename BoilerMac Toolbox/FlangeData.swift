//
//  FlangeData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-08.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlangeData: NSObject, NSCoding{
    enum SaveKeys: String {
        case SIZE = "selectedSize"
        case RATING = "selectedRating"
    }
    
    enum JSONKeys: String {
        case RATINGS = "fRatings"
        case SIZES = "fSizes"
        case STUD_SIZES_ORDERED = "studSizeOrdered"
        case STUD_SIZES = "studSizes"
        case STATS_150 = "fStats150"
        case STATS_300 = "fStats300"
        case STATS_400 = "fStats400"
        case STATS_600 = "fStats600"
        case STATS_900 = "fStats900"
        case STATS_1500 = "fStats1500"
        
        static let allKeys = [RATINGS, SIZES, STUD_SIZES_ORDERED, STUD_SIZES, STATS_150, STATS_300, STATS_400, STATS_600, STATS_900, STATS_1500]
        static let fRatings = [STATS_150, STATS_300, STATS_400, STATS_600, STATS_900, STATS_1500]
    }

    struct FlangeVals {
        var sizes: [String]!
        var sizesStyled: [String] {
            get{
                var retArr = [String]()
                for size: String in sizes{
                    retArr.append(size + "\"")
                }
                return retArr
            }
        }
        var ratings: [String]!
        var ratingsStyled: [String] {
            get{
                var retArr = [String]()
                for rating: String in ratings{
                    retArr.append(rating + " lb")
                }
                return retArr
            }
        }
        
        var size = "err"
        var rating = "err"
        var studCount = "err"
        var studDiameter = "err"
        var wrenchSize = "err"
        var driftPinSize = "err"
        var b7mStrength = "err"
        var b7Strength = "err"
        
        func setDisplaySizes(_ sizes: [String]){
            var sizes = [String]()
            for sizeStr: String in sizes{
                sizes.append(sizeStr + " \"")
            }
        }
        
        func setDisplayRatings(_ ratings: [String]){
        }
    }
    
    // MARK: Properties
    fileprivate var rootJSON: JSON
    
    fileprivate var sizeIndex = 0
    fileprivate var ratingIndex = 0
    fileprivate var studSizeOrdered: [String]
    
    var displayVals: FlangeVals
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("flangedata")
    
    // MARK: Initilization
    override init(){
        displayVals = FlangeVals()
        self.rootJSON = FlangeData.parseJSON("FlangeValues")
        //FlangeData.lightValidation(rootJSON)
        self.displayVals.sizes = rootJSON[JSONKeys.SIZES.rawValue].arrayObject as! [String]
        self.displayVals.ratings = rootJSON[JSONKeys.RATINGS.rawValue].arrayObject as! [String]
        self.studSizeOrdered = rootJSON[JSONKeys.STUD_SIZES_ORDERED.rawValue].arrayObject as! [String]
        
        super.init()
        
        updateDisplayVals()
    }
    
    
    // MARK: Actions
    
    func setSize(_ index: Int){
        self.sizeIndex = index
        // updatePossibleRatings
        updateDisplayVals()
    }
    
    func setRating(_ index: Int){
        self.ratingIndex = index
        updateDisplayVals()
    }
    
    // MARK: Calculations
    
    fileprivate func updateDisplayVals(){
        displayVals.size = displayVals.sizesStyled[sizeIndex]
        displayVals.rating = displayVals.ratingsStyled[ratingIndex]
        
        let fStats = rootJSON[JSONKeys.fRatings[ratingIndex].rawValue]
        
        if let sizeStats = fStats[displayVals.sizes[sizeIndex]].arrayObject as? [String]{
            let studStats = rootJSON[JSONKeys.STUD_SIZES.rawValue][sizeStats[0]].arrayObject as! [String]
            displayVals.studCount = sizeStats[2]
            displayVals.studDiameter = sizeStats[0]
            displayVals.wrenchSize = studStats[0]
            displayVals.driftPinSize = studStats[1]
            displayVals.b7mStrength = studStats[2]
            displayVals.b7Strength = studStats[3]
            
        } else {
            fatalError("FlangeData: error parsing from Flange JSON")
        }
    }
    
    // MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        
        // Int will default to 0 if not found which is fine.
        self.sizeIndex = aDecoder.decodeInteger(forKey: SaveKeys.SIZE.rawValue)
        self.ratingIndex = aDecoder.decodeInteger(forKey: SaveKeys.RATING.rawValue)
        
        updateDisplayVals()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(sizeIndex, forKey: SaveKeys.SIZE.rawValue)
        aCoder.encode(ratingIndex, forKey: SaveKeys.RATING.rawValue)
    }
    
    static fileprivate func parseJSON(_ fileName: String) -> JSON {
        guard let path = Bundle.main.path(forResource: fileName, ofType: "json") else {
            fatalError("Flange JSON not in path.")
        }
        
        do{
            let data = try Data.init(contentsOf: URL(fileURLWithPath: path))
            let rootDic = JSON(data: data)
            return rootDic
        } catch {
            fatalError("Error with parsing Flange JSON.")
        }
        fatalError("Error with parsing Flange JSON.")
    }
    
    static fileprivate func lightValidation(_ masterObject: NSDictionary){
        for key: JSONKeys in JSONKeys.allKeys{
            if masterObject[key.rawValue] == nil{
                fatalError("Flange Parse Error: Couldn't find \(key.rawValue) in masterObject.")
            }
        }
    }

    
}
