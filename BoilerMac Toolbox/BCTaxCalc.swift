import Foundation

class BCTaxCalc: TaxCalc {
    var province: TaxManager.Province = .BC
    var fullName: String = "British Columbia"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
    
    func provTax(anGross: Double) -> Double {
        let provVal = provTaxVal
        
        let basicTax = basicProvincial(anGross: anGross)
        let redBrackets = [Double(provVal.taxRed![0]), Double(provVal.taxRed![1])]
        let redCredit = Double(provVal.taxRed![2])
        let redDropRate = Double(provVal.taxRed![3])
        
        var taxRed = anGross < redBrackets[0] ? redCredit: redCredit - redDropRate * (anGross - redBrackets[0])
        taxRed = max(taxRed, 0.0)
        
        return basicTax - taxRed
    }
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
}
