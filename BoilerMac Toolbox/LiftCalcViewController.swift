//
//  LiftCalcViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-17.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class LiftCalcViewController: UITableViewController {
    class FieldInput{
        var textField: UITextField?
        var label: UILabel?
        var cell: UITableViewCell?
        var minVal: Float
        var maxVal: Float
        var value: Float {
            didSet {
                if value > maxVal {
                    value = maxVal
                }
                if value < minVal {
                    value = minVal
                }
            }
        }
        
        init(range: (min: Float, max: Float), value: Float){
            if range.min > range.max {
                fatalError("LiftValcVC: minVal greater than maxVal")
            }
            self.minVal = range.min
            self.maxVal = range.max
            self.value = value
        }
        
        func updateLabels() {
            if textField != nil {
                textField!.text = String(format: "%.1f", value)
            }
            if label != nil {
                label!.text = String(format: "%.1f", value)
            }
        }
    }
    @IBOutlet weak var imageButton: UIButton!
    
    @IBOutlet weak var slingLengthLabel: UILabel!
    @IBOutlet weak var loadHeightLabel: UILabel!
    @IBOutlet weak var slingAngleLabel: UILabel!
    @IBOutlet weak var loadWeightLabel: UILabel!
    @IBOutlet weak var tensionLabel: UILabel!

    @IBOutlet weak var tensionCell: UITableViewCell!
    @IBOutlet weak var tensionField: UITextField!
    
    @IBOutlet weak var slingLengthCell: UITableViewCell!
    @IBOutlet weak var slingLengthField: UITextField!
    
    @IBOutlet weak var loadHeightCell: UITableViewCell!
    @IBOutlet weak var loadHeightField: UITextField!
    
    @IBOutlet weak var slingAngleCell: UITableViewCell!
    @IBOutlet weak var slingAngleField: UITextField!
    
    @IBOutlet weak var loadWeightCell: UITableViewCell!
    @IBOutlet weak var loadWeightField: UITextField!
    
    var lengthFieldInput = FieldInput(range: (min: 0.01, max: 10000), value: 10)
    var heightFieldInput = FieldInput(range: (min: 0.01, max: 10000), value: 7.1)
    var angleFieldInput = FieldInput(range: (min: 0.01, max: 90), value: 45)
    var weightFieldInput = FieldInput(range: (min: 0.01, 1000000), value: 2500)
    var tensionFieldInput = FieldInput(range: (min: 0.01, max: 1000000), value: 1760)
    var fieldInputs: Array<FieldInput>!
    
    var fieldToolbar: UIToolbar!
    var activeField: FieldInput?
    var lastIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupFields()
        updateLabels()
    }
    
    fileprivate func setupFields(){
        fieldInputs = [lengthFieldInput, heightFieldInput, angleFieldInput, weightFieldInput, tensionFieldInput]
        
        lengthFieldInput.textField = slingLengthField
        lengthFieldInput.label = slingLengthLabel
        lengthFieldInput.cell = slingLengthCell
        
        heightFieldInput.textField = loadHeightField
        heightFieldInput.label = loadHeightLabel
        heightFieldInput.cell = loadHeightCell
        
        angleFieldInput.textField = slingAngleField
        angleFieldInput.label = slingAngleLabel
        angleFieldInput.cell = slingAngleCell
        
        weightFieldInput.textField = loadWeightField
        weightFieldInput.label = loadWeightLabel
        weightFieldInput.cell = loadWeightCell
        
        tensionFieldInput.textField = tensionField
        tensionFieldInput.label = tensionLabel
        tensionFieldInput.cell = tensionCell
        
        fieldToolbar = UIToolbar()
        fieldToolbar.barStyle = UIBarStyle.default
        fieldToolbar.isTranslucent = true
        fieldToolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(LiftCalcViewController.doneField))
        let toolbarSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        fieldToolbar.setItems([doneButton, toolbarSpace], animated: false)
        fieldToolbar.isUserInteractionEnabled = true
        
        for fieldInput: FieldInput in fieldInputs {
            setupField(fieldInput)
        }
    }
    
    @IBAction func pushedImage() {
        doneField()
    }
    
    
    @objc func doneField(){
        
        if activeField != nil {
            
            processFieldValue(activeField)
            
            activeField!.textField!.resignFirstResponder()
            activeField = nil
        }
        
        if lastIndexPath != nil {
            tableView.deselectRow(at: lastIndexPath!, animated: true)
        }
        
        updateLabels()
    }
    
    func updateLabels(){
        for fieldInput: FieldInput in fieldInputs {
            fieldInput.updateLabels()
        }
    }
    
    fileprivate func setupField(_ fieldInput: FieldInput){
        if fieldInput.textField == nil {
            return
        }
        
        fieldInput.textField!.delegate = self
        fieldInput.textField!.inputAccessoryView = fieldToolbar
        fieldInput.textField!.keyboardType = .decimalPad
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        lastIndexPath = indexPath
        
        if let selectingField = fieldFromCell(indexPath) {
            selectingField.becomeFirstResponder()
        }
    }
    
    fileprivate func fieldFromCell(_ indexPath: IndexPath) -> UITextField? {
        for fieldInput: FieldInput in fieldInputs {
            if indexPath == tableView.indexPath(for: fieldInput.cell!){
                return fieldInput.textField
            }
        }
        
        print("Failed to find field from cell.")
        
        return nil
    }
    
    fileprivate func inputFieldFromField(_ textField: UITextField) -> FieldInput? {
        for fieldInput: FieldInput in fieldInputs {
            if let inputField = fieldInput.textField , textField === inputField {
                return fieldInput
            }
        }
        
        print("LiftCalcVC can't match textField.")
        return nil
    }
    
    func processFieldValue(_ fieldInput: FieldInput!){
        let floatVal: Float = Float(fieldInput.textField!.text!) ?? 0
        
        fieldInput.value = floatVal
        
        // if sling length or height changes
        if fieldInput === lengthFieldInput || fieldInput === heightFieldInput {
            if heightFieldInput.value > lengthFieldInput.value {
                heightFieldInput.value = lengthFieldInput.value
            }
            
            angleFieldInput.value = asin(heightFieldInput.value / lengthFieldInput.value) * 180 / 3.14159
        } else {
            if fieldInput === angleFieldInput {
                let angleRads = angleFieldInput.value / 180 * 3.14159
                
                heightFieldInput.value = sin(angleRads) * lengthFieldInput.value
            } else {
                if fieldInput === tensionFieldInput {
                    weightFieldInput.value = tensionFieldInput.value * 2 *
                        heightFieldInput.value / lengthFieldInput.value
                    
                    return
                }
            }
        }
        
        tensionFieldInput.value = weightFieldInput.value / 2
            * lengthFieldInput.value / heightFieldInput.value
    }
    
    
}

extension LiftCalcViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = inputFieldFromField(textField)
        lastIndexPath = tableView.indexPath(for: activeField!.cell!)
        tableView.selectRow(at: lastIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.none)
        
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        doneField()
        
        return true
    }
    
    
}
