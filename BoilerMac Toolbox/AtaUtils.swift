
//
//  AtaMathUtils.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-27.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

open class AtaUtils{
    open static func clamp(_ val: Float, floor: Float, ceiling: Float) -> Float {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
    
    open static func clamp(_ val: Double, floor: Double, ceiling: Double) -> Double {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
    
    open static func clampInt(_ val: Int, floor: Int, ceiling: Int) -> Int {
        let checkFloor = ceiling < floor ? ceiling : floor
        let checkCeiling = ceiling < floor ? floor: ceiling
        
        if val < checkFloor{
            return checkFloor
        }
        
        if checkCeiling < val {
            return checkCeiling
        }
        
        return val
    }
}

extension Double {
    func cage(floor: Double, ceiling: Double) -> Double {
        return max(floor, min(ceiling, self))
    }
}

extension Float {
    func cage(floor: Float, ceiling: Float) -> Float {
        return max(floor, min(ceiling, self))
    }
}

extension UIColor {
    static var mercury: UIColor {
        return UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
    }
}

extension Collection {
    
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
