//
//  File.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation

@objc
protocol TableData {
    func fullNameList() -> Array<String>
    func nameAtIndex(_ index: Int) -> String
    func lastSelectedIndex() -> Int
}
