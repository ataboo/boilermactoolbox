//
//  SidePanelItem.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-15.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

@objc
class SidePanelTool: NSObject {
    let title: String
    let subTitle: String
    let icon: UIImage
    let storyboardName: String
    var viewController: UIViewController?
    var ordinal: Int!
    
    
    init(title: String, subTitle: String, storyboardName: String, iconName: String){
        self.title = title
        self.storyboardName = storyboardName
        self.subTitle = subTitle
        self.icon = UIImage(named: iconName)!
    }
    
    static let payCalcTool = SidePanelTool(title: "Pay Calculator",
        subTitle: "A week's wages and deductions.",
        storyboardName: "PayCalc",
        iconName: "payIcon")
    static let taxCalcTool = SidePanelTool(title: "Tax Calculator",
        subTitle: "Taxes based on gross weekly earnings.",
        storyboardName: "GrossCalc",
        iconName: "taxIcon")
    static let flangeTool = SidePanelTool(title: "Flange Hardware",
        subTitle: "Reference flange hardware from NPS size and rating.",
        storyboardName: "FlangeCalc",
        iconName: "flangeIcon")
    static let torqueTool = SidePanelTool(title: "Torque Pattern",
        subTitle: "Generates a bolt pattern sequence for torquing.",
        storyboardName: "TorquePattern",
        iconName: "torqueIcon")
    static let liftTool = SidePanelTool(title: "Sling Lift Estimate",
        subTitle: "Calculate weight on each sling in a 2 leg lift.",
        storyboardName: "LiftCalc",
        iconName: "liftIcon")
    static let ropeTool = SidePanelTool(title: "Rope Stats",
        subTitle: "Attributes of different types of rope.",
        storyboardName: "RopeStats",
        iconName: "ropeIcon")
    static let cashCounter = SidePanelTool(title: "Cash Counter",
        subTitle: "Animates earnings based on the work week.",
        storyboardName: "CashCounter",
        iconName: "counterIcon")
    static let calloutTool = SidePanelTool(title: "146 Callout",
        subTitle: "View current calls on the 146 site.",
        storyboardName: "CalloutViewer",
        iconName: "ic_boot")
    
    
    class func panelToolList() -> Array<SidePanelTool> {
        let list = [payCalcTool, calloutTool, taxCalcTool, flangeTool, torqueTool, liftTool, ropeTool, cashCounter]
        
        if list[0].ordinal == nil {
            for (index, tool) in list.enumerated() {
                tool.ordinal = index
            }
        }
        
        return list
    }
    
    class func toolViewController(_ tool: SidePanelTool) -> UIViewController? {
    
        if tool.viewController == nil {
            tool.viewController = UIStoryboard.entryViewController(tool.storyboardName)
        }

        return tool.viewController
    }
}

func ==(lhs: SidePanelTool, rhs: SidePanelTool) -> Bool {
    return lhs.storyboardName == rhs.storyboardName && lhs.title == rhs.title
}


private extension UIStoryboard {
    class func entryViewController(_ storyboardName: String) -> UIViewController {
        return entryViewController(UIStoryboard(name: storyboardName, bundle: nil))
    }
    
    class func entryViewController(_ storyboard: UIStoryboard) -> UIViewController {
        return storyboard.instantiateViewController(withIdentifier: "EntryPoint")
    }
}
