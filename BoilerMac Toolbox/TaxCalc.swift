//
//  ProvCalc.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2017-04-08.
//  Copyright © 2017 atasoft. All rights reserved.
//

import Foundation

protocol TaxCalc {
    var province: TaxManager.Province {get}
    var fullName: String {get}
    var taxFromProvince: TaxManager.Province? {get}
    var wageFromProvince: TaxManager.Province? {get}
    var yearString: String {get set}
    
    mutating func calcTax(taxableGross: Double, taxExempt: Double) -> (fedTax: Double, provTax: Double, cppVal: Double, eiVal: Double)
    func fedTax(anGross: Double) -> Double
    func provTax(anGross: Double) -> Double
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double)
    mutating func setYear(yearString: String)
    
}

extension TaxCalc {    
    public var provTaxVal: TaxVal {
        get {
            return TaxValRepo.instance.getProv(prov: self, year: yearString)
        }
    }
    
    public var fedTaxVal: TaxVal {
        get {
            return TaxValRepo.instance.getFed(year: yearString)
        }
    }
    
    public func getCSVPath() -> String? {
        return Bundle.main.path(forResource: "ToolboxGrid - \(province.rawValue)", ofType: "csv")
    }
    
    public mutating func setYear(yearString: String) {
        guard TaxCSVParser().parseYears().contains(yearString) else {
            fatalError("Year String: \(yearString) is invalid.")
        }
        self.yearString = yearString
    }
    
    public mutating func calcTax(taxableGross: Double, taxExempt: Double) -> (fedTax: Double, provTax: Double, cppVal: Double, eiVal: Double) {
        let grossEarnings = max(taxableGross, 0.0)
        let taxableEarnings = max(taxableGross - taxExempt, 0.0)
        let anGross = grossEarnings * 52.0
        let anTaxable = taxableEarnings * 52.0
        
        let fed = max(fedTax(anGross: anTaxable) / 52.0, 0.0)
        let prov = max(provTax(anGross: anGross) / 52.0, 0.0)
        let cppEi = getWeekCppEi(anGross: anGross)
        
        return (fed, prov, cppEi.cppVal, cppEi.eiVal)
    }
    
    internal func basicProvincial(anGross: Double) -> Double {
        let brackIndex = getBracketIndex(anGross, brackets: provTaxVal.brackets!)
        let basicTax = anGross * Double(provTaxVal.rates![brackIndex])
        let constKVal = provTaxVal.constK![brackIndex]
        let taxCredit = getProvTaxCredit(anGross: anGross)
        
        return (basicTax - Double(constKVal) - taxCredit)
    }
    
    internal func basicFedTax(anGross: Double) -> Double {
        let fedTax = fedTaxVal
        
        let bracketIndex = getBracketIndex(anGross, brackets: fedTax.brackets!)
        let taxRate = Double(fedTax.rates![bracketIndex])
        let constKVal = Double(fedTax.constK![bracketIndex])
        let basicAmount = anGross * taxRate
        
        let taxCred = getFedTaxCredit(anGross: anGross)
        
        return max(basicAmount - taxCred - constKVal, 0.0)
    }
    
    internal func getFedTaxCredit(anGross: Double) -> Double {
        let fedVal = fedTaxVal
        
        return getTaxCredit(anGross: anGross, claimAmount: Double(fedVal.claimAmount!), lowestRate: Double(fedVal.rates![0]))
    }
    
    internal func getProvTaxCredit(anGross: Double) -> Double {
        let provVal = provTaxVal
        
        return getTaxCredit(anGross: anGross, claimAmount: Double(provVal.claimAmount!), lowestRate: Double(provVal.rates![0]))
    }
    
    internal func getTaxCredit(anGross: Double, claimAmount: Double, lowestRate: Double) -> Double {
        let fedVal = fedTaxVal
        
        let cppEiVals = cppEi(anGross: anGross, fedVal: fedVal)
        var cppContribution = cppEiVals.cppVal
        var eiContribution = cppEiVals.eiVal
        let maxCppContribution = Double(fedVal.cppEi![3])
        let maxEiContribution = Double(fedVal.cppEi![4])
        cppContribution = min(maxCppContribution, cppContribution)
        eiContribution = min(maxEiContribution, eiContribution)
        
        let taxCredit = (claimAmount + cppContribution + eiContribution) * lowestRate
        return taxCredit
    }
    
    /// Returns index of appropriate tax bracket for gross
    internal func getBracketIndex(_ gross: Double, brackets: [Float]) -> Int {
        if gross < 0 {
            print("TaxManager/getBracketIndex: gross can not be less than 0.")
            return 0
        }
        if brackets.count == 0 {
            fatalError("TaxManager/getBracketIndes: brackets array needs to be longer than 0.")
        }
        let grossFloat = Float(gross)
        
        for index in 0...brackets.count - 1 {
            let reverseIndex = brackets.count - 1 - index
            
            if grossFloat > brackets[reverseIndex] {
                return reverseIndex
            }
        }
        
        return 0
    }
    
    /// Returns annual cpp and ei contribution based on gross
    internal func basicCppEi(_ anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double){
        let cppRate = Double(fedVal.cppEi![0])
        let cppExempt = Double(fedVal.cppEi![1])
        let eiRate = Double(fedVal.cppEi![2])
        
        var cppVal = (anGross - cppExempt) * cppRate
        if cppVal < 0 {cppVal = 0}
        let eiVal = anGross * eiRate
        
        return (cppVal, eiVal)
    }
    
    internal func getWeekCppEi(anGross: Double) -> (cppVal: Double, eiVal: Double) {
        let cppEiVals = cppEi(anGross: anGross, fedVal: fedTaxVal)
        let cppMax = Double(fedTaxVal.cppEi![3])
        let eiMax = Double(fedTaxVal.cppEi![4])
        
        var weekCpp = cppEiVals.cppVal / 52.0
        if weekCpp > cppMax {weekCpp = cppMax}
        
        var weekEi = cppEiVals.eiVal / 52.0
        if weekEi > eiMax {weekEi = eiMax}
        
        return (weekCpp, weekEi)
    }

}
