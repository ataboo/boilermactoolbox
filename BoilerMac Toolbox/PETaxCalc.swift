import Foundation

class PETaxCalc: TaxCalc {
    var province: TaxManager.Province = .PE
    var fullName: String = "Prince Edward Island"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = .NS
    var yearString: String = TaxManager.defaultYearName
    
    func provTax(anGross: Double) -> Double {
        let provVal = provTaxVal
        
        let basicTax = basicProvincial(anGross: anGross)
        // surtax[bracket, rate]
        // tax of (rate) on tax amount over (bracket)
        var surtax = (basicTax - Double(provVal.surtax![0])) * Double(provVal.surtax![1])
        if surtax < 0 { surtax = 0 }
        
        return basicTax + surtax
    }
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
}
