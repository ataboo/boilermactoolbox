//
//  PauseFlags.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-27.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

// Changed by AppDelegate on active/inactive methods.
open class PauseFlags {
    public enum PauseState {
        case paused
        case handled
        case unPaused
    }
    
    open static var cashCounter: PauseState = .handled
    
}
