//
//  ToggleSwitch.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-23.
//  Copyright © 2015 atasoft. All rights reserved.
//
// Init from http://iphonedev.tv/blog/2014/12/15/create-an-ibdesignable-uiview-subclass-with-code-from-an-xib-file-in-xcode-6 tutorial

import UIKit

@IBDesignable class ToggleSwitch: UIView {
    // MARK: Properties
    
    let nibName = "ToggleSwitch"
    
    var isOn: Bool {
        get{
            return frontSwitch.isOn
        }
    }
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var frontSwitch: UISwitch!
    
    /// Text of background button title
    @IBInspectable var buttonText: String? {
        get{
            return backButton.titleLabel!.text
        }
        set(labelText){
            backButton.setTitle(labelText, for: UIControlState())
        }
    }
    /// Font size of background button
    @IBInspectable var fontSize: Int {
        get{
            return Int(backButton.titleLabel!.font.pointSize)
            
        }
        set(fontSize){
            let cgFont = CGFloat(fontSize)
            backButton.titleLabel!.font = UIFont.systemFont(ofSize: cgFont)
        }
    }
    /// Sets if switch starts on
    @IBInspectable var startOn: Bool {
        get{
            return frontSwitch.isOn
        }
        set(isOn){
            frontSwitch.setOn(isOn, animated: false)
        }
    } // Testing out tooltip

    
    // MARK: Initialization
    fileprivate func xibSetup() {
        let view = loadViewFromNib()
        view.frame = bounds
        
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
    }
    
    fileprivate func loadViewFromNib() -> UIView {
        let view = UINib(nibName: nibName, bundle: Bundle.main).instantiate(withOwner: self, options: nil).first as! UIView
        dump(view)
        return view
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    // MARK: Actions
    @IBAction func backButtonClick(_ sender: AnyObject) {
        if sender === backButton {
            frontSwitch.setOn(!frontSwitch.isOn, animated: true)
        }
        
    }
    
    func setButtonAction(_ viewController: UIViewController, selector: Selector){
        backButton.addTarget(viewController, action: selector, for: UIControlEvents.touchUpInside)
        frontSwitch.addTarget(viewController, action: selector, for: UIControlEvents.touchUpInside)
    }
  
    func setOn(_ setOn: Bool, animated: Bool){
        frontSwitch.setOn(setOn, animated: animated)
    }
    

}
