//
//  CalloutDataManager.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation
import UIKit

class CalloutDataManager: NSObject {
    enum SortMode {
        case contractor
        case jobLocation
    }
    
    let calloutClient = CalloutClient.instance()
    var callDictionary = [String: [CalloutObject]]()
    var rawCallouts: [CalloutObject]?
    var sortedCallKeys = [String]()
    let calloutTVC: CalloutTVC
    var filterCount = 0
    
    init(calloutTVC: CalloutTVC) {
        self.calloutTVC = calloutTVC
        
        calloutTVC.tableView.register(UINib(nibName: "CalloutCallCell", bundle: nil), forCellReuseIdentifier: "CallCell")
        calloutTVC.tableView.register(UINib(nibName: "CalloutTitleCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderCell")
        
        super.init()
    }
    
    func getCalls() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        calloutClient.getCallout { callObjs in
            self.setCalls(callObjs)
        }
    }
    
    fileprivate func setCalls(_ newCalls: [CalloutObject]) {
        //TODO: no calls found error.
        
        self.rawCallouts = newCalls
    
        updateFilterMatches(newCalls)
        makeCallsDictionary(newCalls)
        
        // Using transaction to prevent refresh icon from stopping too suddenly
        CATransaction.begin()
        CATransaction.setCompletionBlock({ () -> Void in
            self.calloutTVC.tableView.reloadData()
        })
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        calloutTVC.refreshControl?.endRefreshing()
        CATransaction.commit()
    }
    
    fileprivate func callCountCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        var callCountCell = tableView.dequeueReusableCell(withIdentifier: "CallCountCell")
        
        if callCountCell == nil {
            callCountCell = UITableViewCell(style: .subtitle, reuseIdentifier: "CallCountCell")
        }
        
        if let callouts = rawCallouts, callouts.count > 0 {
            callCountCell?.textLabel?.text = "Filtering \(filterCount) Calls."
        } else {
            callCountCell?.textLabel?.text = NetworkMessenger.isReachable() ? "No Calls Found." : "Server Not Reachable."
        }
        let pullTime = rawCallouts?.first?.pullTime.getFormatted("MM/dd/yy - HH:mm") ?? "n/a"
        callCountCell?.detailTextLabel?.text = "Last Server Update: \(pullTime)"
        //callCountCell?.textLabel?.textColor = CalloutConst.grayMed
        callCountCell?.backgroundView = nil
        callCountCell?.backgroundColor = UIColor(white: 1.0, alpha: 0.0)
        callCountCell?.isUserInteractionEnabled = false
        return callCountCell!
    }
    
    fileprivate func makeCallsDictionary(_ callObjs: [CalloutObject]) {
        var callDic = [String: [CalloutObject]]()
        
        let calloutSettings = CalloutDataManager.getCalloutSettings()
        let hideNonMatching = calloutSettings.hideNonMatching
        let hideExcluded = calloutSettings.hideExcluded
        
        let filteredObjs = callObjs.filter({
            switch($0.matchLevel!) {
            case .someInclusive: fallthrough
            case .all:
                return true
            case .none:
                return !hideNonMatching
            case .someExclusive:
                return !hideExcluded
            }
        })
        
        filterCount = callObjs.count - filteredObjs.count
        
        for call in filteredObjs {
            // TODO: Change me for different sorting
            //call.sortTerm = ...
            let sectionTerm = call.contractor
            
            if callDic.keys.contains(sectionTerm) {
                callDic[sectionTerm]!.append(call)
            } else {
                callDic[sectionTerm] = [call]
            }
        }
        
        sortedCallKeys = callDic.keys.sorted()
        for key in sortedCallKeys {
            callDic[key] = callDic[key]!.sort()
        }
        
        self.callDictionary = callDic
    }
    
    fileprivate func updateFilterMatches(_ callouts: [CalloutObject]) {
        let filterTerms = CalloutDataManager.getFilterTerms()
        for call in callouts {
            let matchStats = CallFilterTerm.getCallMatches(call, filters: filterTerms)
            call.matchLevel = matchStats.matchLevel
            call.matchMap = matchStats.matchMap
        }
    }
    
    // Called on return from Settings
    func refreshFilterMatches() {
        if let callouts = self.rawCallouts {
            setCalls(callouts)
        } else {
            print("No Callouts to update filters... getting calls.")
            getCalls()
        }
    }
    
    func callForIndexPath(_ indexPath: IndexPath) -> CalloutObject {
        return callDictionary[dicKeyForSection(indexPath.section)]![indexPath.row]
    }
    
    func dicKeyForSection(_ section: Int) -> String {
        return sortedCallKeys[section]
    }
    
    static func commitCalloutSettings(hideNonMatching: Bool, hideExcluded: Bool, notificationsActive: Bool) {
        let defaults = UserDefaults.standard
        
        defaults.set(hideNonMatching, forKey: CalloutConst.DefaultKeys.hideNonMatching)
        defaults.set(hideExcluded, forKey: CalloutConst.DefaultKeys.hideExcluded)
        defaults.set(notificationsActive, forKey: CalloutConst.DefaultKeys.notification)
        
        defaults.synchronize()
    }
    
    static func commitFilterTerms(_ filterTerms: [CallFilterTerm]) {
        let defaults = UserDefaults.standard
        
        let filterData: Data = NSKeyedArchiver.archivedData(withRootObject: filterTerms)
        defaults.set(filterData, forKey: CalloutConst.DefaultKeys.filters)
        
        defaults.synchronize()
        
        print("Saved Filter Terms!!!")
    }
    
    static func getCalloutSettings() -> (hideNonMatching: Bool, hideExcluded: Bool, notificationsActive: Bool) {
        let defaults = UserDefaults.standard
        
        defaults.synchronize()
        
        let hideNonMatching = defaults.bool(forKey: CalloutConst.DefaultKeys.hideNonMatching)
        let hideExcluded = defaults.bool(forKey: CalloutConst.DefaultKeys.hideExcluded)
        let notifications = defaults.bool(forKey: CalloutConst.DefaultKeys.notification)
        
        return (hideNonMatching: hideNonMatching,
                hideExcluded: hideExcluded,
                notificationsActive: notifications)
    }
    
    static func getFilterTerms() -> [CallFilterTerm] {
        let defaults = UserDefaults.standard
        
        var filters: [CallFilterTerm] = []
        if let filterData = defaults.object(forKey: CalloutConst.DefaultKeys.filters) as? Data,
            let filtersLoaded = NSKeyedUnarchiver.unarchiveObject(with: filterData) as? [CallFilterTerm] {
            filters = filtersLoaded
        }
        
        return filters
    }
}

extension CalloutDataManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.section < sortedCallKeys.count else {
            return callCountCell(tableView, indexPath: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CallCell", for: indexPath) as! CalloutCallCell
        
        let callObject = callForIndexPath(indexPath)
        cell.textLabel?.text = callObject.jobName.capitalizeNonAcronyms()
        cell.detailTextLabel?.text = callObject.dateShiftSchedule.capitalizeNonAcronyms()
        cell.matchLevel = callObject.matchLevel
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sortedCallKeys.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard section < sortedCallKeys.count else {
            return 1
        }
        
        return callDictionary[dicKeyForSection(section)]!.count
    }
}

extension CalloutDataManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard section < sortedCallKeys.count else {
            return nil
        }
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
        
        cell?.textLabel?.text = sortedCallKeys[section].capitalizeNonAcronyms()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let callObj = callForIndexPath(indexPath)
        calloutTVC.showCalloutDetail(callObj)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
