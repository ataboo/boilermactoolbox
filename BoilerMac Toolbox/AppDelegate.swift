//
//  AppDelegate.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-13.
//  Copyright © 2015 atasoft. All rights reserved.

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UITabBarDelegate {

    var window: UIWindow?
    static let rotateLockWidth = CGFloat(375)
    
    var apnListener: APNListener?
    let networkMessenger = NetworkMessenger()

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window!.makeKeyAndVisible()
        // Override point for customization after application launch.
        
        if let launchOptions = launchOptions {
            if CalloutAPNS.handleNotificationLaunch(launchOptions) {
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        CalloutAPNS.handleNotificationRunning(userInfo, isActive: application.applicationState == .active)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        PauseFlags.cashCounter = .paused
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        PauseFlags.cashCounter = .unPaused
        
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // MARK Push Notification stuff:
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        apnListener?.saveNotificationKey(deviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote Notifications err:")
        print(error.localizedDescription)
        
        apnListener?.notificationsDenied()
    }
    
    func addAPNListener(listener: APNListener) {
        self.apnListener = listener
    }
}

