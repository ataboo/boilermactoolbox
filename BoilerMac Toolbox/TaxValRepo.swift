import Foundation

class TaxValRepo {
    private static var repoInstance: TaxValRepo?
    
    public static var instance: TaxValRepo {
        get {
            if repoInstance == nil {
                repoInstance = TaxValRepo()
            }
            
            return repoInstance!
        }
    }
    
    private var fedTaxVal: TaxVal?
    public let taxYears: [String]
    
    private var provTaxVals = Dictionary<TaxManager.Province, TaxVal>()
    
    init() {
        self.taxYears = TaxCSVParser().parseYears()
    }
    
    public func getProv(prov: TaxCalc, year: String) -> TaxVal {
        let yearIndex = getYearIndex(yearName: year)
        
        return getProv(prov: prov, yearIndex: yearIndex)
    }
    
    public func getProv(prov: TaxCalc, yearIndex: Int) -> TaxVal {
        if provTaxVals[prov.province] == nil || provTaxVals[prov.province]!.yearIndex != yearIndex {
            provTaxVals[prov.province] = TaxCSVParser().parseTaxVal(taxCalc: prov, year: yearIndex)
        }
        
        return provTaxVals[prov.province]!
    }
    
    public func getFed(year: String) -> TaxVal {
        let yearIndex = getYearIndex(yearName: year)
        
        if fedTaxVal == nil || fedTaxVal!.yearIndex != yearIndex {
            fedTaxVal = TaxCSVParser().parseTaxVal(taxCalc: FedTaxCalc(), year: yearIndex)
        }
        
        return fedTaxVal!
    }
    
    public func getYearIndex(yearName: String) -> Int {
        guard let index = taxYears.index(of: yearName) else {
            fatalError("Can't get index of year: \(yearName)")
        }
        
        return index
    }
}
