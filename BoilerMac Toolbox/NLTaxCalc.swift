import Foundation

class NLTaxCalc: TaxCalc {
    var province: TaxManager.Province = .NL
    var fullName: String = "Newfoundland"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func provTax(anGross: Double) -> Double {
        var provTax = basicProvincial(anGross: anGross)
        
        if self.provTaxVal.yearIndex > 4 {
            provTax += deficitLevy(anGross: anGross)
        }
        
        return provTax
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
    
    // Newfoundland Deficit levy
    fileprivate func deficitLevy(anGross: Double) -> Double {
        let bracket = getBracketIndex(anGross, brackets: self.provTaxVal.levyBrackets!)
        
        let baseVal = self.provTaxVal.levyBase![bracket]
        var calcVal = (Float(anGross) - self.provTaxVal.levyBrackets![bracket]) * self.provTaxVal.levyRate!
        if bracket > 0 {
            calcVal += self.provTaxVal.levyBase![bracket - 1]
        }
        
        return Double(min(baseVal, calcVal))
    }
}
