//
//  LoopAnimation.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-24.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class LoopAnimation: NSObject{
    open var active = true
    open let sprite: SKSpriteNode
    
    fileprivate var textures = Array<SKTexture>()
    fileprivate let frameLength: Int
    fileprivate let prefix: String
    fileprivate let atlas: SKTextureAtlas
    fileprivate let animationLength: Int
    
    
    public init(atlas: SKTextureAtlas, prefix: String, frameLength: Int, sprite: SKSpriteNode) {
        self.sprite = sprite
        self.prefix = prefix
        self.frameLength = frameLength
        self.atlas = atlas
        textures = SpriteUtils.texturesFromAtlas(atlas, prefix: prefix)
        animationLength = textures.count * frameLength
    }

}
extension LoopAnimation: SpriteObj {
    
    public func keyForTime(_ currentTime: Int) -> SKTexture? {
        if textures.count == 0 {
            fatalError("\(prefix) has texture array length of 0.")
        }
            
        let remainderInt: Int = currentTime % animationLength
        let textureIndex = remainderInt * textures.count / animationLength
        //print("SpriteAnimation returned textureIndex: \(textureIndex)")
            
        return  textures[textureIndex]
    }
    
    public func setIsActive(_ active: Bool) {
        self.active = active
    }
    
    public func updatePosSize(_ centerPos: CGPoint, size: CGSize){
        sprite.position = centerPos
        sprite.size = size
    }
    
    public func dispose(){
        sprite.removeFromParent()
    }
}

