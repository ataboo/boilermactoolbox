//
//  TaxVal.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-25.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation

// Holder for each areas tax brackets, wage vals, etc
struct TaxVal{
    var prov: TaxManager.Province
    var yearIndex: Int
    var wages: OrderedDictionary<String, Double>?
    
    var brackets: [Float]?
    var claimAmount: Float?
    var claimNs: [Float]?
    var constK : [Float]?
    var cppEi: [Float]?
    var qppRate: Float?
    var qppMax: Float?
    var qpipRate: Float?
    var qpipMax: Float?
    var empDeduction: Float?
    
    var healthBrackets: [Float]?
    var healthRates: [Float]?
    var healthAmounts: [Float]?
    var rates: [Float]?
    var surtax: [Float]?
    var taxRed: [Float]?
    var vacRate: Float?
    var fieldDuesRate: Float?
    var monthlyDuesRate: Float?
    var nightsRate: Float?
    var nightOT = false
    
    var payWeekday: [Int]?
    var payWeekend: [Int]?
    var payHoliday: [Int]?
    var payFTFriday: [Int]?
    var payFTWeekday: [Int]?
    
    var levyBrackets: [Float]?
    var levyBase: [Float]?
    var levyRate: Float?
    
    init(province: TaxManager.Province, year: Int) {
        self.prov = province
        self.yearIndex = year
    }
}
