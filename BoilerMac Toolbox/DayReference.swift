//
//  DayReference.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-18.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation
import UIKit

// Helper Class for PayCalcData to hold information for dayButtons and their pickers
class DayReference {  //Couldn't encode as struct
    var resID : String
    var name: String
    var shortHand: String
    var hours: [Float]
    var buttonLabel: String
    var isHoliday: Bool = false
    var hourPreset = [Float (0)]
    
    var isWeekend: Bool {
        get {
            return resID == PayCalcData.dayKeys[0] || resID == PayCalcData.dayKeys[6]
        }
    }
    var isFriday: Bool {
        get{
            return resID == PayCalcData.dayKeys[5]
        }
    }
    
    init(resID : String, name: String, shortHand : String){
        self.resID = resID
        self.name = name
        self.shortHand = shortHand
        self.hours = [Float(0)]
        self.buttonLabel = ""
        
        updateButtonLabel()
    }
    
    // Updates string in ref for paycalc to use, not actual button
    func updateButtonLabel() {
        var hourSum = Float(0)
        for hour: Float in hours{
            hourSum += hour
        }
        
        self.buttonLabel = "\(shortHand)\n\(hourSum)"
    }
    
    func saveToPreset(){
        self.hourPreset = hours
    }
    
    func loadFromPreset(){
        self.hours = hourPreset
    }
}