//
//  GameScene.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-26.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

class CounterGameScene: SKScene {
    
    var lastKeyTime = Float(0)
    
    var startDate: Date!
    var startSeconds: Double!
    private var centerPos: CGPoint!
    private var sceneSize: CGSize!
    private var activeScene: CounterScene!
    private var cashData: CashCounterData
    
    private var shiftStartDate: Date!
    private var shiftEndDate: Date!
    
    var lastPayTime: Double!
    var lastCoarseTime: Double!
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("aDecoder init not supported.")
    }
    
    init(size: CGSize, cashData: CashCounterData){
        self.cashData = cashData
        
        super.init(size: size)
        activeScene = cashData.activeSceneType.getScene(self)
        centerPos = CGPoint(x: size.width / 2, y: size.height / 2)
        sceneSize = getSceneSize(size, sceneAspectRatio: activeScene.aspectRatio)        
        activeScene.updateSizePos(centerPos, sceneSize: sceneSize)
        
        
        initActiveScene()
    }
    
    private func initActiveScene(){
        cashData.refreshPayCalcData()
        
        lastPayTime = timeIntoShift() + 0.01
        lastCoarseTime = lastPayTime + 10
        
        let earnings = cashData.getEarnings(Double(lastPayTime - 2.5), weekday: getWeekDays().today)
        activeScene?.setCounter(earnings.earnings, description: earnings.earningType.rawValue)

        PauseFlags.cashCounter = .handled
    }
    
    func resetScene(){
        activeScene.dispose()
        scene?.removeAllActions()
        scene?.removeAllChildren()
        
        activeScene = cashData.nextSceneType(self)
        activeScene.updateSizePos(centerPos, sceneSize: sceneSize)
        
        initActiveScene()
        
        
    }
    
    override func didMove(to view: SKView) {
        backgroundColor = SKColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        
        //activeScene = PulpMillScene(parentScene: self, centerPos: centerPos, sceneSize: imageSize)
    }
    
    
    
    override func didChangeSize(_ oldSize: CGSize) {
        if activeScene == nil{
            return
        }
        
        centerPos = CGPoint(x: size.width / 2, y: size.height / 2)
        sceneSize = getSceneSize(size, sceneAspectRatio: activeScene!.aspectRatio)
        
        activeScene.updateSizePos(centerPos, sceneSize: sceneSize)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if PauseFlags.cashCounter == .paused || cashData.lockScene {
            return
        }
        
        resetScene()
    }
    
    override func update(_ currentTime: TimeInterval) {
        if PauseFlags.cashCounter == .paused || lastPayTime == nil{
            return
        }
        
        if PauseFlags.cashCounter == .unPaused {
            cashData.saveData()
            
            initActiveScene()
        }
        
        let shiftTime = timeIntoShift()
        
        if shiftTime == 0.0 {
            let yesterShift = getShiftDays().yesterShift
            let earnings = cashData.getEarnings(Double(cashData.shiftLength(yesterShift) * 3600), weekday: yesterShift)
            
            activeScene?.setCounter(earnings.earnings, description: earnings.earningType.rawValue)
            
            activeScene?.removeAnimations()
            return
        }
        
        if shiftTime > lastPayTime{
            let nextPayTime = shiftTime + 2.5
            let payTimeInt = Int((shiftTime + 2.0) * 1000.0)
            let cashEarned = cashData.getEarnings(shiftTime, weekday: getWeekDays().today)
            activeScene?.nextFineAnim(payTimeInt, totalValue: cashEarned.earnings, description: cashEarned.earningType.rawValue)
                    
            lastPayTime = nextPayTime
        }
        
        if shiftTime > lastCoarseTime {
            let nextCoarseTime = shiftTime + 10
            let coarseTimeInt = Int((lastCoarseTime + 2.0) * 1000.0)
            
            activeScene?.nextCoarseAnim(coarseTimeInt)
            
            lastCoarseTime = nextCoarseTime
        }
                
        activeScene?.update(Int(shiftTime * 1000))
    }
    
    private func getShiftDates(_ dateNow: Date) -> (shiftStart: Date, shiftEnd: Date){
        let unitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second, .weekday]
        
        var shiftStartComponents = (Calendar.current as NSCalendar).components(unitFlags, from: dateNow)
        
        shiftStartComponents.hour = cashData.shiftStartHr
        shiftStartComponents.minute = cashData.shiftStartMin
        shiftStartComponents.second = 0
        
        var shiftStartDate = Calendar.current.date(from: shiftStartComponents)!
        var shiftEndDate = shiftStartDate.addingTimeInterval(3600 * Double(cashData.shiftLength(getWeekDays().today)))
        
        if dateNow.compare(shiftEndDate).rawValue >= 0 {
            shiftStartDate = shiftStartDate.addingTimeInterval(3600 * 24)
            shiftEndDate = shiftEndDate.addingTimeInterval(3600 * 24)
        }
        
        return (shiftStartDate, shiftEndDate)
    }
    
    private func timeIntoShift() -> Double {
        if startSeconds == nil {
            startDate = Date()
            
            let shiftDates = getShiftDates(startDate)
            shiftStartDate = shiftDates.shiftStart
            shiftEndDate = shiftDates.shiftEnd
        }
        
        let intervalNow = Date.timeIntervalSinceReferenceDate
        var intervalStart = shiftStartDate.timeIntervalSinceReferenceDate
        var intervalEnd = shiftEndDate.timeIntervalSinceReferenceDate
        
        if intervalNow >= intervalEnd {
            print("CounterGameScene: Bumped up a day...")
            
            let shiftDates = getShiftDates(Date())
            shiftStartDate = shiftDates.shiftStart
            shiftEndDate = shiftDates.shiftEnd
            
            intervalStart = shiftStartDate.timeIntervalSinceReferenceDate
            intervalEnd = shiftEndDate.timeIntervalSinceReferenceDate
        }
        
        if intervalNow >= intervalStart{
            return intervalNow - intervalStart
        }
        
        return 0.0
    }
    
    private func getSceneSize(_ viewSize: CGSize, sceneAspectRatio: CGFloat) -> CGSize{
        let viewAspect = viewSize.width / viewSize.height
        
        if viewAspect < sceneAspectRatio {
            return CGSize(width: viewSize.width, height: viewSize.width / sceneAspectRatio)
        } else {
            return CGSize(width: viewSize.height * sceneAspectRatio, height: viewSize.height)
        }
    }
    
    private func getWeekDays() -> (today: Int, yesterday: Int){
        let today = Int((Calendar.current as NSCalendar).component(.weekday, from: startDate)) - 1
        let yesterday = today == 0 ? 6: today - 1
        
        return (today: today, yesterday: yesterday)
    }
    
    private func getShiftDays() -> (thisShift: Int, yesterShift: Int) {
        if shiftStartDate == nil {
            return (thisShift: 1, yesterShift: 0)
        }
        
        let thisShift = Int((Calendar.current as NSCalendar).component(.weekday, from: shiftStartDate!)) - 1
        let yesterShift = thisShift == 0 ? 6: thisShift - 1
        
        return (thisShift: thisShift, yesterShift: yesterShift)
    }
}

