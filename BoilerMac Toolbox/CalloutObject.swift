//
//  CalloutObject.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation
import SwiftyJSON

class CalloutObject: NSObject {
    let jobId: Int
    let jobName: String
    let contractor: String
    let openTo: String
    let dateTime: Date
    let isDayshift: Bool
    let workType: String
    let hours: String
    let duration: String
    let accommodation: String
    let drugTesting: String
    let comments: String
    let pullTime: Date
    
    var nameHires: [String]
    var manpower: [String]
    
    var sortTerm: String
    
    var matchMap: [CallFilterTerm.TermType: [CallFilterTerm.MatchLevel]]!
    
    var matchLevel: CallFilterTerm.MatchLevel!
    
    var shiftString: String {
        return isDayshift ? "Days" : "Nights"
    }
    
    var dateShiftSchedule: String {
        return "\(dateTime.dateFormatted) - "
            +  "\(shiftString) - "
            +  "\(hours)"
    }
    
    var scheduleShift: String {
        return "\(hours) - "
             + "\(shiftString)"
    }
    
    
    
    init(attributes: JSON, pullTime: Date) {
        self.pullTime = pullTime
        
        if let jobString = attributes["id"].string, let jobInt = Int(jobString) {
            self.jobId = jobInt
        } else {
            self.jobId = attributes["id"].int ?? -1
        }
        self.jobName = attributes["job_name"].string ?? ""
        self.contractor = attributes["contractor"].string ?? ""
        self.openTo = attributes["open_to"].string ?? ""
        
        if let dateString =  attributes["date_time"].string, let dateTime = Date.bmCalloutParse(dateString) {
            self.dateTime = dateTime
        } else {
            print("Error parsing dateTime on jobID: \(jobId)")
            self.dateTime = Date()
        }
        
        self.isDayshift = attributes["is_dayshift"].string ?? "1" == "1"
        self.workType = attributes["work_type"].string ?? ""
        self.hours = attributes["hours"].string ?? ""
        self.duration = attributes["duration"].string ?? ""
        self.accommodation = attributes["accommodation"].string ?? ""
        self.drugTesting = attributes["drug_testing"].string ?? ""
        self.comments = attributes["comments"].string ?? ""
        self.sortTerm = jobName
        
        //TODO: might aswell just reference namehire/manpower objects instead of simplifying.
        self.nameHires = [String]()
        if let nameHiresArr = attributes["namehires"].array {
            for nameHireRaw in nameHiresArr {
                let nameHire = NameHire(attr: nameHireRaw)
                if nameHire.valid() {
                    nameHires.append(nameHire.toString())
                }
            }
        }
        
        self.manpower = [String]()
        if let manpowersArr = attributes["manpowers"].array {
            for manpowerRaw in manpowersArr {
                let manpowerObj = Manpower(attr: manpowerRaw)
                if manpowerObj.valid() {
                    manpower.append(manpowerObj.toString())
                }
            }
        }
    }
    
    static func parseJSON(_ json: JSON) -> [CalloutObject] {
        var calloutObjs = [CalloutObject]()
        var dateTime = Date()
        
        if let pullDate = json["pull_time"].string, let parsed = Date.bmCalloutParse(pullDate) {
            dateTime = parsed
        }
        if let data = json["data"].array {
            for jArray in data {
                calloutObjs.append(CalloutObject(attributes: jArray, pullTime: dateTime))
            }
        }
        
        return calloutObjs
    }
}

extension Array where Element:CalloutObject {
    func sort() -> [CalloutObject] {
        return sorted {$0.sortTerm < $1.sortTerm}
    }
}

extension Date {
    static func bmCalloutParse(_ dateString: String) -> Date? {
        let dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = dateFormat
        
        let date = dateFormatter.date(from: dateString)
        
        return date
    }
    
    var dateFormatted: String {
        return getFormatted("MM/dd/yy")
    }
    
    var timeFormatted: String {
        return getFormatted("HH:mm:ss")
    }
    
    func getFormatted(_ format: String) -> String {
        return DateFormatter(fromString: format).string(from: self)
    }
}

extension DateFormatter {
    convenience init(fromString: String) {
        self.init()
        
        self.dateFormat = fromString
    }
}
