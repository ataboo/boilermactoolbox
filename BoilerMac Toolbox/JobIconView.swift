//
//  JobIconView.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-10.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

@IBDesignable
open class JobIconView: UIView {
    
    var mainColor: UIColor = CalloutConst.grayLight
    //@IBInspectable var fontSize: CGFloat = 12.0
    var textColor: UIColor = CalloutConst.white
    var icImage: UIImage? = UIImage(named: "stinger")
    
    fileprivate var iconString = "?"
    
    open var highlightState: Int = 0 {
        didSet {
            mainColor = highlightState == 0 ? CalloutConst.grayMed : highlightState == 1 ? CalloutConst.green : CalloutConst.red
            self.setNeedsDisplay()
        }
    }
    
    open func setJobType(_ jobType: JobJect.JobType) {
        if let jobClass = jobType.jobClass, let jobQual = jobType.qualification {
            self.iconString = jobQual.rawValue
            self.icImage = UIImage(named: jobClass.rawValue)
        }
        
        self.setNeedsDisplay()
    }
    
    open override func draw(_ rect: CGRect) {
        let circlePath = UIBezierPath(ovalIn: rect.insetBy(dx: 2.0, dy: 2.0))
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.cgPath
        shapeLayer.fillColor = mainColor.cgColor

        let fontSize = 3.0/5.0 * frame.size.height
        let textLayer = CATextLayer()
        textLayer.string = iconString
        textLayer.font = UIFont.boldSystemFont(ofSize: fontSize)
        textLayer.fontSize = fontSize
        textLayer.frame = rect.offsetBy(dx: 0.0, dy: rect.midY - fontSize / 2.0 * 1.25)
        textLayer.alignmentMode = kCAAlignmentCenter
        textLayer.foregroundColor = textColor.cgColor
        textLayer.shadowColor = UIColor.black.cgColor
        textLayer.shadowOffset = CGSize.zero
        textLayer.shadowOpacity = 1.0
        textLayer.shadowRadius = 1.0
        
        let imageLayer = CALayer()
        
        imageLayer.frame = rect.insetBy(dx: 6, dy: 6)
        imageLayer.contents = icImage?.cgImage
        imageLayer.opacity = 0.5
        
        layer.addSublayer(shapeLayer)
        layer.addSublayer(imageLayer)
        layer.addSublayer(textLayer)
        
        layer.shadowRadius = 1.0
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowOpacity = 0.7
    }
}
