//
//  FirstViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-13.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class PayCalcViewController: UIViewController {
    enum HourPreset {
        case zero
        case tens
        case twelves
        case thirteen
        case load_CUSTOM
        case save_CUSTOM
    }
    
    // Used by PaySettingsViewController
    /// Title of About alert
    static let aboutTitleText = "BoilerMac Toolbox v" + (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String)
    /// Content of About alert
    static let aboutBlurbText = "BoilerMac Toolbox is a collection of tools designed to help Union Boilermakers in Canada." +
        "\n\nCheck out Boilermaker Toolbox if you have an Android device." +
    "\n\nInformation in this app is meant for quick reference and shouldn't be relied on over official sources.  Feel free to make this app better by emailing suggestions or problems."
    
    
    // MARK: Properties
    /// Dummy text field used to bring up picker views
    @IBOutlet weak var pickerTextField: UITextField!
    /// View holding day buttons
    @IBOutlet weak var daysStackView: UIStackView!
    /// Label holding week total hours
    @IBOutlet weak var hourTotalLabel: UILabel!
    /// Label showing wage rate
    @IBOutlet weak var wageLabel: UILabel!
    /// Button to change wage rate via picker. Shows name of active wage.
    @IBOutlet weak var wageButton: UIButton!

    /// Button that opens picker to set meal bonus count
    @IBOutlet weak var mealButton: UIButton!
    /// Label that displays meal bonus count
    @IBOutlet weak var mealLabel: UILabel!
    /// Button that opens a picker to set subsistance count (sub == loa)
    @IBOutlet weak var subButton: UIButton!
    /// Label that displays subsistance count
    @IBOutlet weak var subLabel: UILabel!
    
    @IBOutlet weak var nightsToggle: ToggleSwitch!
    @IBOutlet weak var fourTensToggle: ToggleSwitch!
    @IBOutlet weak var weekTravelToggle: ToggleSwitch!
    @IBOutlet weak var dayTravelToggle: ToggleSwitch!
    @IBOutlet weak var grossLabel: UILabel!
    @IBOutlet weak var vacLabel: UILabel!
    @IBOutlet weak var taxToggle: ToggleSwitch!
    @IBOutlet weak var cppToggle: ToggleSwitch!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var cppLabel: UILabel!
    @IBOutlet weak var monthDuesToggle: ToggleSwitch!
    @IBOutlet weak var hourDuesToggle: ToggleSwitch!
    @IBOutlet weak var duesLabel: UILabel!
    @IBOutlet weak var deductionsLabel: UILabel!
    @IBOutlet weak var netPayLabel: UILabel!
    
    @IBInspectable var normalButtonBackground: UIImage!
    @IBInspectable var holidayButtonBackground: UIImage!
    
    var pickerViewController: PickerViewController?
    var payCalcData: PayCalcData!
    
    // MARK: Initialization
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }
    
    init(){
        super.init(nibName: nil, bundle: nil)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        //fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit(){
        self.pickerViewController = PickerViewController(payCalcViewController: self)
        
        self.payCalcData = loadPayCalcData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerViewController!.pickerTextField = pickerTextField
        pickerViewController!.viewDidLoad()
        
        setupButtons()
        updateLabels()
        updateTogglesFromData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func setupButtons(){
        // Passed to the background button and switch in the toggle
        nightsToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        fourTensToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        weekTravelToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        dayTravelToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        taxToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        cppToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        monthDuesToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        hourDuesToggle.setButtonAction(self, selector: #selector(PayCalcViewController.switchClick(_:)))
        
        //Iterate through all day buttons in the stackView
        for subview: UIView in daysStackView.subviews {
            if let subButton = subview as? UIButton{
                // Find button int day reference dictionary
                if let resID = subButton.restorationIdentifier, let dayRef = payCalcData.dayRefs[resID]{
                    //Set initial text value and enable 2 lines, alignment
                    PayCalcViewController.multiLineTextButton(subButton)
                    subButton.setTitle("\(dayRef.buttonLabel)", for: UIControlState())
                } else {
                    let titleLabel = subButton.titleLabel?.text ?? "No Title"
                    print("Couldn't get dayRef for: \(titleLabel)")
                }
            }
        }
    }
    
    /// Sets title for button to 2 line text
    static func multiLineTextButton(_ button: UIButton){
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = NSTextAlignment.center
    }
    
    // MARK: Actions
    
    /// Called by wage and bonus buttons on touch up
    @IBAction func pickerButton(_ sender: UIButton) {
        if sender.restorationIdentifier != nil {
            // Pass to PickerViewController to handle
            pickerViewController!.openPickerButton(sender)
        } else {
            print ("PayCalcViewController WARNING: pickerButton called by button without resID.")
        }
    }
    
    
    
    /// Called by parent view on touch up
    @IBAction func tappedBackground() {
        pickerViewController!.donePicker()
    }
    
    /// Called by toggles
    @IBAction func switchClick(_ sender: AnyObject) {
        switchClick()
    }
    
    /// Creates popover with choice in hour presets for days.
    @IBAction func hourPresetAlert(_ sender: UIBarButtonItem){
        let presetAlert = UIAlertController(title: "Hours Preset", message: "Set hours of the week to preset value.", preferredStyle: UIAlertControllerStyle.actionSheet)
        presetAlert.addAction(UIAlertAction(title: "Zero", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.zero)}))
        presetAlert.addAction(UIAlertAction(title: "Tens", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.tens)}))
        presetAlert.addAction(UIAlertAction(title: "Twelves", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.twelves)}))
        presetAlert.addAction(UIAlertAction(title: "Thirteens", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.thirteen)}))
        presetAlert.addAction(UIAlertAction(title: "Save Current", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.save_CUSTOM)}))
        presetAlert.addAction(UIAlertAction(title: "Load Saved", style: .default, handler: {
            (action: UIAlertAction!) in self.setHourPreset(HourPreset.load_CUSTOM)}))
        
        // Give IPad or anything else that's making it a popover a location to display
        if let popoverController = presetAlert.popoverPresentationController {
            popoverController.barButtonItem = sender
        }
        
        present(presetAlert, animated: true, completion: nil)
    }
    
    /// Updates selected wage on wage picker close
    func setSelectedWage(_ value: String){
        payCalcData.setSelectedWage(value)
        switchClick()
    }
    
    /// Sets values for meal or loa days after picker close
    func setMealLoa(_ count: Int, resID: String){
        if resID == mealButton.restorationIdentifier! {
            payCalcData.screenValues.mealDays = count
        } else if resID == subButton.restorationIdentifier! {
            payCalcData.screenValues.loaDays = count
        } else {
            print("PayCalcView Warning: setMealLoa resID didn't match meal or loa buttons.")
        }
        
        switchClick()
    }
    
    // MARK: Private Actions
    
    /// Sets day hours to preset values
    fileprivate func setHourPreset(_ preset: HourPreset){
        switch preset {
        case HourPreset.zero:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.isHoliday = false
                payCalcData.setHours(["0"], dayKey: dayRef.resID)
            }
        case HourPreset.tens:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.isHoliday = false
                payCalcData.setHours(["10"], dayKey: dayRef.resID)
            }
        case HourPreset.twelves:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.isHoliday = false
                payCalcData.setHours(["12"], dayKey: dayRef.resID)
                
            }
        case HourPreset.thirteen:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.isHoliday = false
                payCalcData.setHours(["13"], dayKey: dayRef.resID)
            }
            
        case HourPreset.load_CUSTOM:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.loadFromPreset()
            }
        case HourPreset.save_CUSTOM:
            for dayRef: DayReference in payCalcData.dayRefs.values {
                dayRef.saveToPreset()
            }
        }
        
        switchClick()
    }
    
    /// Updates calculations
    fileprivate func switchClick(){
        refreshDataFromViews()
        updateLabels()
    }
    
    /// Updates hours when DayPickTable closes
    fileprivate func updateHours(_ hours: [String], dayKey: String){
        payCalcData.setHours(hours, dayKey: dayKey)
        switchClick()
    }
    
    /// Sets data to toggle values.
    fileprivate func refreshDataFromViews(){
        //Kind of brute force to refresh all switches.
        //Might only change values that are clicked later but might be just as expensive
        payCalcData.screenValues.isNightShift = nightsToggle.isOn
        payCalcData.screenValues.isFourTens = fourTensToggle.isOn
        payCalcData.screenValues.isWeekTravel = weekTravelToggle.isOn
        payCalcData.screenValues.isDayTravel = dayTravelToggle.isOn
        payCalcData.screenValues.isTaxed = taxToggle.isOn
        payCalcData.screenValues.isCppOn = cppToggle.isOn
        payCalcData.screenValues.isHourDuesOn = hourDuesToggle.isOn
        payCalcData.screenValues.isMonthDuesOn = monthDuesToggle.isOn
    }
    
    /// sets view states from data
    fileprivate func updateLabels(){
        
        //Recalculate data
        payCalcData.updateCalc()
        savePayCalcData()
        
        // Update Day Button Labels and holiday switches
        for subView: UIView in daysStackView.subviews{
            if let subButton = subView as? UIButton, let resID = subButton.restorationIdentifier, let dayRef = payCalcData.dayRefs[resID]{
                // Set day button labels
                subButton.setTitle(dayRef.buttonLabel, for: UIControlState())
                let buttonBack = dayRef.isHoliday || dayRef.isWeekend || dayRef.isFriday && payCalcData.screenValues.isFourTens ? holidayButtonBackground: normalButtonBackground
                subButton.setBackgroundImage(buttonBack, for: UIControlState())
                
            } else {
                let titleLabel = subView.restorationIdentifier ?? "No resID"
                print("Couldn't get dayRef for: \(titleLabel)")
            }
        }
        
        // Update labels
        let hourTotals = payCalcData.screenValues.hoursTotal
        hourTotalLabel.text = "Hours = 1.0x: \(hourTotals[0]), 1.5x: \(hourTotals[1]), 2.0x: \(hourTotals[2])"
        
        grossLabel.text = String(format: "Gross: $%.2f", payCalcData.screenValues.earnings.gross)
        vacLabel.text = String(format: "Vac(%.1f%%): $%.2f", payCalcData.screenValues.activeVacRate * Float(100),
                payCalcData.screenValues.earnings.vacBonus)
        taxLabel.text = String(format: "Tax: $%.2f", payCalcData.screenValues.deductions.sumTax)
        
        cppToggle.buttonText = payCalcData.taxManager.activeProvince.province == .QC ? "QPP/QPIP" : "CPP/EI"
        cppLabel.text = String(format: "$%.2f + $%.2f", payCalcData.screenValues.deductions.cpp, payCalcData.screenValues.deductions.ei)
        hourDuesToggle.backButton.setTitle(String(format: "Dues %.2f%%", (payCalcData.screenValues.fieldDuesRate * Float(100))), for: UIControlState())
        //hourDuesToggle.backButton.setTitle("Banana", forState: .Normal)
        duesLabel.text = String(format: "Dues: %.2f", payCalcData.screenValues.deductions.sumDues)
        deductionsLabel.text = String(format: "Deduct: $%.2f", payCalcData.screenValues.deductions.total)
        netPayLabel.text = String(format: "Take Home: $%.2f", payCalcData.screenValues.earnings.net)
        
        wageButton.setTitle("\(payCalcData.screenValues.selectedWageKey!) - \(payCalcData.taxManager.activeProvince.province.rawValue)", for: UIControlState())
        wageLabel.text! = String(format: "$%.2f", payCalcData.screenValues.wageRate)
        
        mealLabel.text! = "\(payCalcData.screenValues.mealDays)"
        subLabel.text! = "\(payCalcData.screenValues.loaDays)"
    }
    
    /// Called onViewLoad or by presets
    fileprivate func updateTogglesFromData(){
        nightsToggle.setOn(payCalcData.screenValues.isNightShift, animated: false)
        fourTensToggle.setOn(payCalcData.screenValues.isFourTens, animated: false)
        weekTravelToggle.setOn(payCalcData.screenValues.isWeekTravel, animated: false)
        dayTravelToggle.setOn(payCalcData.screenValues.isDayTravel, animated: false)
        taxToggle.setOn(payCalcData.screenValues.isTaxed, animated: false)
        cppToggle.setOn(payCalcData.screenValues.isCppOn, animated: false)
        monthDuesToggle.setOn(payCalcData.screenValues.isMonthDuesOn, animated: false)
        hourDuesToggle.setOn(payCalcData.screenValues.isHourDuesOn, animated: false)
    }
    
    // MARK: NSCoding
    
    fileprivate func savePayCalcData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(payCalcData, toFile: PayCalcData.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save Pay Calc Data...")
        }
    }
    
    fileprivate func loadPayCalcData() -> PayCalcData? {
        if let loadedPayCalc = NSKeyedUnarchiver.unarchiveObject(withFile: PayCalcData.ArchiveURL.path) as? PayCalcData {
            return loadedPayCalc
        }
        print("PayCalcViewController Failed to load payCalcData... Instantiating new one.")
        return PayCalcData()
    }
    
    fileprivate func resetDataToDefault() {
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(atPath: PayCalcData.ArchiveURL.path)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        
        self.payCalcData = PayCalcData()        
        updateLabels()
        updateTogglesFromData()
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Pass payCalcData and proper dayRef to DayPickTableViewController before hour pick segue started by day button
        if let dayPickerController = segue.destination as? DayPickTableViewController,
            let senderButton = sender as? UIButton,
            let senderID = senderButton.restorationIdentifier {
                if let dayRef = payCalcData.dayRefs[senderID]{
                    dayPickerController.initData(payCalcData, dayRef: dayRef)
                } else {
                    fatalError("PayCalcViewController: couldn't match day ref for segue to day pick.")
                }
        }
        
        // Pass payCalcData to PaySettingsViewCont before show segue from settings button
        if let paySettingsController = segue.destination as? PaySettingsViewCont{
            paySettingsController.initData(payCalcData)
        }
    }
    
    @IBAction func rewindSettingsSave(_ sender: UIStoryboardSegue){
        if let paySettingsCont = sender.source as? PaySettingsViewCont {
            payCalcData.updateDataFromSettings(paySettingsCont.saveData)
            switchClick()
        }
    }
    
    @IBAction func rewindSettingsReset(_ sender: UIStoryboardSegue){
        resetDataToDefault()
        GrossCalcTVC.resetFlag = true
        FlangeTableViewController.resetFlag = true
        TorqueTableViewController.resetFlag = true
    }
    
    @IBAction func rewindDaySelect(_ sender: UIStoryboardSegue){
        if let daySelectViewController = sender.source as? DayPickTableViewController {
            if let selectedHours = daySelectViewController.selectedHours, let dayRef = daySelectViewController.dayRef {
                payCalcData.setHours(selectedHours, dayKey: dayRef.resID)
                payCalcData.dayRefs[dayRef.resID]?.isHoliday = daySelectViewController.holidayOn ?? false
                
                switchClick()
            }
        }
    }
}
