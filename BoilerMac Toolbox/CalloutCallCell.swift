//
//  CalloutCallCell.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutCallCell: UITableViewCell {
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var sideIcon: UIImageView!
    var matchLevel: CallFilterTerm.MatchLevel = .none {
        didSet {
            sideIcon.image = matchLevel == .none ? nil : matchLevel == .someExclusive ? CalloutConst.xImage : CalloutConst.checkImage
            sideIcon.image = sideIcon.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            sideIcon.tintColor = matchLevel == .someExclusive ? CalloutCallCell.xColor : CalloutCallCell.checkColor
        }
    }
    
    static let xColor = UIColor.red
    static let checkColor = UIColor.green
    

    
    override var textLabel: UILabel? {
        return mainLabel
    }
    
    override var detailTextLabel: UILabel? {
        return secondLabel
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.clipsToBounds = true
        matchLevel = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
