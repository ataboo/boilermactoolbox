//
//  PulpMillScene.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class CounterScene: NSObject {
    open var aspectRatio: CGFloat = 1024/720
    
    let atlasName: String
    let backgroundName: String
    
    let parentScene: SKScene
    var centerPos = CGPoint(x: 50, y: 50)
    var sceneSize = CGSize(width: 100, height: 100)
    
    var spriteObjects = Array<SpriteObj>()
    let earningsCounter: EarningsDisplay
    
    public required init(parentScene: SKScene, atlasName: String, backgroundName: String){
        self.parentScene = parentScene
        self.atlasName = atlasName
        self.backgroundName = backgroundName
        
        earningsCounter = EarningsDisplay(parentScene: parentScene, posOffset: CGPoint(x: -0.08, y: 0))
        super.init()
        
        let backgroundObj = StaticSprite(sprite: centeredSprite(backgroundName))
        backgroundObj.sprite.zPosition = -5
        addSpriteObject(backgroundObj)
        
        spriteObjects.append(earningsCounter)
        
    }
    
    func addSpriteObject(_ obj: SpriteObj){
        parentScene.addChild(obj.sprite!)
        spriteObjects.append(obj)
    }
    
    func centeredSprite(_ imageName: String) -> SKSpriteNode {
        let sprite = SKSpriteNode(imageNamed: imageName)
        sprite.position = centerPos
        sprite.size = sceneSize
        
        return sprite
    }

    open func updateSizePos(_ centerPos: CGPoint, sceneSize: CGSize){
        self.centerPos = centerPos
        self.sceneSize = sceneSize
        
        for spriteObject in spriteObjects {
            spriteObject.updatePosSize(centerPos, size: sceneSize)
        }
    }
    
    
    open func setCounter(_ counterVal: Float, description: String){
        earningsCounter.manualSetEarnings(counterVal, earningDescription: description)
    }
    
    open func dispose() {
        for spriteObj in spriteObjects {
            spriteObj.sprite?.removeFromParent()
        }
        
        spriteObjects.removeAll()
    }
    
    open func update(_ currentTime: Int) {
        var garbageFlag = false
        
        for spriteObj in spriteObjects {
            if !spriteObj.active{
                garbageFlag = true
                spriteObj.sprite?.removeFromParent()
            }
            
            if let nextTexture = spriteObj.keyForTime?(currentTime){
                spriteObj.sprite?.texture = nextTexture
            }
            
            if let labelDone = spriteObj.update?(currentTime){
                if labelDone{
                    garbageFlag = true
                }
            }
        }
        
        if garbageFlag{
            spriteObjects = spriteObjects.filter{$0.active}
        }
    }
    
    open func removeAnimations(){
        for obj: SpriteObj in spriteObjects{
            if obj is OnceLabel || obj is OnceAnimation {
                obj.dispose?()
            }
        }
        
        spriteObjects = spriteObjects.filter{!($0 is OnceLabel) || !($0 is OnceAnimation)}
    }
    
    open func nextFineAnim(_ startTime: Int, totalValue: Float, description: String) {
        // overriden by child
    }
    
    open func nextCoarseAnim(_ startTime: Int) {
        // overriden by child
    }
}
