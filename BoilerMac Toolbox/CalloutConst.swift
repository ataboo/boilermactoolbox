//
//  CalloutConstants.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-10.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

public class CalloutConst {
    static let acroList = ["CBI", "TEAM", "CIMS", "RFID", "GPS", "TAP", "RSAP", "CWB",
                           "TIG", "MIG", "FCAW", "GTAW", "SMAW", "SS", "TIW"]
    
    public static let grayMed = UIColor(white: 179.0/255.0, alpha: 1.0)
    public static let green = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 1.0)
    public static let tranGreen = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.3)
    public static let tranRed = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.3)
    public static let tranBlue = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 0.4)
    public static let tungsten = UIColor(white: 51.0/255.0, alpha: 1.0)
    public static let aluminum = UIColor(white: 153.0/255.0, alpha: 1.0)
    public static let red = UIColor.red
    public static let backColors: [Int: UIColor] = [-1: CalloutConst.tranRed,
                                              0: CalloutConst.white,
                                              1: CalloutConst.tranGreen]
    
    public static let white = UIColor(white: 1.0, alpha: 1.0)
    public static let grayLight = UIColor(white: 230.0/255.0, alpha: 1.0)
    public static let headerBlue = UIColor(red: 0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    
    public static let transactionArg = "transaction"
    
    public class DefaultKeys {
        // Settings Defaults
        public static let notification = "Callout_Notifications_Active"
        public static let filters = "Callout_Filters"
        public static let hideNonMatching = "callout_hide_non_matching"
        public static let hideExcluded = "callout_hide_excluded"
        
        // Filters
        public static let termType = "Callout_Filter_Term_Type"
        public static let keyWords = "Callout_Filter_Keywords"
        public static let inclusive = "Callout_Filter_Inclusive"
        
        // APNS
        public static let apnKey = "callout_apn_key"
        public static let apnNotificationsLocked = "callout_apn_locked"
    }
    
    public class KeyWords {
        public static let fitter = ["FITTER", "BOILERMAKER", "BM"]
        public static let bWelder = ["TIG", "SS", "INCO", "B"]
        public static let rigger = ["RIGGER"]
        public static let foreman = ["FOREMAN", "FM"]
        public static let apprentice = ["APPR", "APPRENTICE"]
        public static let journeyman = ["JM", "JOURNEYMAN", "J"]
        public static let apprenticeWelder = ["W1", "W2", "W3"]
        public static let apprenticeFitter = ["B1", "B2", "B3"]
        public static let welder = ["WELDER", "STICK", "ORBITAL", "CWB"]
        public static var allApprentice: [String] {
            get {
                var joinedApprentice: [String] = []
                joinedApprentice.append(contentsOf: apprentice)
                joinedApprentice.append(contentsOf: apprenticeWelder)
                joinedApprentice.append(contentsOf: apprenticeFitter)
                return joinedApprentice
            }
            
            
        }
    }
    
    public static let checkImage = UIImage(named: "ic_check_circle_48pt")
    public static let xImage = UIImage(named: "ic_clear_48pt")
    
    public static let attributions: [String] = [
        "Boot by Ival Platenac",
        "Spanner Hammer by Hea Poh Lin"
    ]
}

extension String {
    func capitalizeNonAcronyms(_ capList: [String] = CalloutConst.acroList) -> String {
        var outString = self
        self.enumerateSubstrings(in: self.startIndex..<self.endIndex, options: .byWords) { substring, substringRange, enclosingRange, stop in
            if let string = substring {
                if capList.contains(string) {
                    return
                } else {
                    outString = outString.replacingCharacters(in: substringRange, with: string.capitalized)
                    
                }
            }
        }
        
        return outString
    }
    
    func containsAnyOfTheseWords(words: [String]) -> Bool {
        var containFlag = false
        
        self.enumerateSubstrings(in: self.startIndex..<self.endIndex, options: .byWords) { substring, substringRange, enclosingRange, stop in
            if let matchWord = substring, words.contains(matchWord) {
                containFlag = true
                return
            }
        }
        
        return containFlag
    }
}

extension UIImage {
    func resizedAndTintable(_ factor: CGFloat) -> UIImage {
        
        let size = self.size.applying(CGAffineTransform(scaleX: factor, y: factor))
        let hasAlpha = true
        let scale: CGFloat = 0.0
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!.withRenderingMode(.alwaysTemplate)
    }
}
