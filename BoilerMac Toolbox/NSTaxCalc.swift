import Foundation

class NSTaxCalc: TaxCalc {
    var province: TaxManager.Province = .NS
    var fullName: String = "Nova Scotia"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func provTax(anGross: Double) -> Double {
        let brackIndex = getBracketIndex(anGross, brackets: provTaxVal.brackets!)
        let basicTax = anGross * Double(provTaxVal.rates![brackIndex])
        let constKVal = provTaxVal.constK![brackIndex]
        let taxCredit = getProvTaxCredit(anGross: anGross)
        
        return (basicTax - Double(constKVal) - taxCredit)
    }
    
    //As of 2018, basic claim amound can vary based on gross, so tax credit is calc'd differently
    internal func getProvTaxCredit(anGross: Double) -> Double {
        if self.provTaxVal.yearIndex < 5 {
            return (self as TaxCalc).getProvTaxCredit(anGross: anGross)
        } else {
            return newNsTaxCredit(anGross: anGross)
        }
    }
    
    // Basic claim amount varies based on an gross.
    fileprivate func newNsTaxCredit(anGross: Double) -> Double {
        let claimAmount = Double(getNsClaimAmount(anGross: anGross))
    
        return getTaxCredit(anGross: anGross, claimAmount: claimAmount, lowestRate: Double(provTaxVal.rates![0]))
    }
    
    fileprivate func getNsClaimAmount(anGross: Double) -> Float {
        let claimNs: [Float] = provTaxVal.claimNs!
        let flGross = Float(anGross)
        
        if flGross > claimNs[0] && flGross < claimNs[1] {
            return claimNs[2] - ((flGross - claimNs[0]) * claimNs[3])
        }
        
        return provTaxVal.claimAmount!
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
}
