//
//  ReachCenter.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SystemConfiguration
import ReachabilitySwift

public class NetworkMessenger {
    
    private let reachability = Reachability()!
    private(set) var reachable = false
    private let client = CalloutClient.instance()

    
    public static var getInstance: NetworkMessenger? {
        get {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                return appDelegate.networkMessenger
            }
            return nil
        }
    }
    
    init() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(NetworkMessenger.reachabilityChanged(_:)), name: ReachabilityChangedNotification, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("Couldn't start reachability notifier.")
        }
    }
    
    class func isReachable() -> Bool {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) { zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }) else {
            return false
        }
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return isReachable && !needsConnection
    }
    
    @objc public func reachabilityChanged(_ note: NSNotification) {
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            print("*** Is Reachable ***")
            self.reachable = true
        } else {
            print("*** Not Reachable ***")
            self.reachable = false
        }
    }
}
