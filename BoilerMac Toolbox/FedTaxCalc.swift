import Foundation

class FedTaxCalc: TaxCalc {
    var province: TaxManager.Province = .FED
    var fullName: String = "Federal"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
    
    func calcTax(taxableGross: Double, taxExempt: Double) -> (fedTax: Double, provTax: Double, cppVal: Double, eiVal: Double) {
        fatalError("Not Implemented for FED")
    }
    func fedTax(anGross: Double) -> Double {
        fatalError("Not Implemented for FED")
    }
    
    func provTax(anGross: Double) -> Double {
        fatalError("Not Implemented for FED")
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        fatalError("Not Implemented for FED")
    }
}
