//
//  SpriteAnim.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-24.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

@objc
public protocol SpriteObj {
    var active: Bool {get}
    @objc optional var sprite: SKSpriteNode {get}
    
    func setIsActive(_ active: Bool)
    
    func updatePosSize(_ centerPos: CGPoint, size: CGSize)
    
    @objc optional func keyForTime(_ currentTime: Int) -> SKTexture?
    
    @objc optional func startAnimation(_ startTime: Int)
    
    @objc optional func update(_ currentTime: Int) -> Bool
    
    @objc optional func dispose()
}

