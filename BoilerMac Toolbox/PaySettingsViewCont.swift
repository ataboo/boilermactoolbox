//
//  PayCalcSettingsViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-25.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class PaySettingsViewCont: UITableViewController {
    struct SettingsData {
        init(){
        }
        var year: String?
        var prov: TaxManager.Province?
        var weekTravRate: Float?
        var dayTravRate: Float?
        var loaRate: Float?
        var mealRate: Float?
        var customWage: Float?
        // nil if off
        var vacRate: Float?
        var monthlyDues: Float?
        var fieldDues: Float?
        var nightPremium: Float?
    }
    
    
    // MARK: Properties
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var provField: UITextField!
    @IBOutlet weak var yearField: UITextField!
    @IBOutlet weak var weekTravField: UITextField!
    @IBOutlet weak var dayTravField: UITextField!
    @IBOutlet weak var loaField: UITextField!
    @IBOutlet weak var mealField: UITextField!
    @IBOutlet weak var customVacSwitch: UISwitch!
    @IBOutlet weak var customVacField: UITextField!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var aboutButton: UIButton!
    
    @IBOutlet weak var provCell: UITableViewCell!
    @IBOutlet weak var yearCell: UITableViewCell!
    @IBOutlet weak var weakTravelCell: UITableViewCell!
    @IBOutlet weak var dayTravelCell: UITableViewCell!
    @IBOutlet weak var loaCell: UITableViewCell!
    @IBOutlet weak var mealCell: UITableViewCell!
    @IBOutlet weak var wageCell: UITableViewCell!
    @IBOutlet weak var vacCell: UITableViewCell!
    @IBOutlet weak var monthDuesCell: UITableViewCell!
    @IBOutlet weak var fieldDuesCell: UITableViewCell!
    @IBOutlet weak var nightsCell: UITableViewCell!
    @IBOutlet weak var resetTableCell: UITableViewCell!
    @IBOutlet weak var aboutTableCell: UITableViewCell!
    
    @IBOutlet weak var wageField: UITextField!
    
    @IBOutlet weak var monthDuesSwitch: UISwitch!
    @IBOutlet weak var monthDuesField: UITextField!
    
    @IBOutlet weak var fieldDuesSwitch: UISwitch!
    @IBOutlet weak var fieldDuesField: UITextField!
    @IBOutlet weak var nightSwitch: UISwitch!
    @IBOutlet weak var nightField: UITextField!
    
    var saveData = SettingsData()
    var fieldActiveColor: UIColor?
    let fieldInactiveColor = UIColor.black
    var provPicker = UIPickerView()
    var provOptions = [String]()
    var yearPicker = UIPickerView()
    var yearOptions = [String]()
    
    var payCalcData: PayCalcData!
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if payCalcData == nil{
            fatalError("PaySettingsViewCont: must run initData to set payCalcData before viewDidLoad.")
        }
        
        self.fieldActiveColor = customVacField.backgroundColor
        
        setViewsFromData()
    }
    
    func setViewsFromData(){
        let doneToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(PaySettingsViewCont.doneKeyboard))
        
        doneToolbar.items = [doneButton, flexSpace]
        doneToolbar.sizeToFit()
        
        provField.text = payCalcData.taxManager.activeProvince.province.rawValue
        
        initFieldSwitch(customVacField, switchView: customVacSwitch, val: payCalcData.screenValues.customVacRate)
        initFieldSwitch(monthDuesField, switchView: monthDuesSwitch, val: payCalcData.screenValues.custMonthDues)
        initFieldSwitch(fieldDuesField, switchView: fieldDuesSwitch, val: payCalcData.screenValues.custFieldDues)
        initFieldSwitch(nightField, switchView: nightSwitch, val: payCalcData.screenValues.custNightShift)
        
        provPicker.showsSelectionIndicator = true
        provPicker.dataSource = self
        provPicker.delegate = self
        provField.inputAccessoryView = doneToolbar
        
        yearPicker.showsSelectionIndicator = true
        yearPicker.dataSource = self
        yearPicker.delegate = self
        yearField.inputAccessoryView = doneToolbar
        
        self.yearOptions = TaxCSVParser().parseYears()
        
        let taxCalcs = TaxManager(prov: .AB).taxCalcs
        for prov: TaxManager.Province in TaxManager.Province.activeProvs {
            self.provOptions.append(taxCalcs[prov]!.fullName)
        }
    
        let yearIndex = yearOptions.index(of: payCalcData.screenValues.activeYear)!
        yearPicker.selectRow(yearIndex, inComponent: 0, animated: false)
        yearField.text = yearOptions[yearIndex]
        yearField.inputView = yearPicker
        yearField.tintColor = UIColor.clear
        
        let provIndex = TaxManager.Province.activeProvs.index(of: payCalcData.screenValues.activeProv)!
        provPicker.selectRow(provIndex, inComponent: 0, animated: false)
        provField.text = provOptions[provIndex]
        provField.inputView = provPicker
        provField.tintColor = UIColor.clear
        

        weekTravField.text = String(payCalcData.screenValues.weekTravRate)
        weekTravField.inputAccessoryView = doneToolbar
        dayTravField.text = String(payCalcData.screenValues.dayTravRate)
        dayTravField.inputAccessoryView = doneToolbar
        loaField.text = String(payCalcData.screenValues.loaRate)
        loaField.inputAccessoryView = doneToolbar
        mealField.text = String(payCalcData.screenValues.mealRate)
        mealField.inputAccessoryView = doneToolbar
        wageField.text = String(payCalcData.screenValues.custWageRate)
        wageField.inputAccessoryView = doneToolbar
        
        customVacField.inputAccessoryView = doneToolbar
        monthDuesField.inputAccessoryView = doneToolbar
        fieldDuesField.inputAccessoryView = doneToolbar
        nightField.inputAccessoryView = doneToolbar
    }
    
    // Called externally
    func initData(_ payCalcData: PayCalcData){
        self.payCalcData = payCalcData
    }
    
    // MARK: Actions
    @IBAction func fieldSwitchTouch(_ sender: UISwitch) {
        switch(sender){
        case customVacSwitch:
            setFieldActive(customVacField, isActive: customVacSwitch.isOn)
        case monthDuesSwitch:
            setFieldActive(monthDuesField, isActive: monthDuesSwitch.isOn)
        case fieldDuesSwitch:
            setFieldActive(fieldDuesField, isActive: fieldDuesSwitch.isOn)
        case nightSwitch:
            setFieldActive(nightField, isActive: nightSwitch.isOn)
        default: break
        }
    }
    
    @IBAction func resetSettingsTouch(_ sender: UIButton){
        if sender === resetButton {
            let resetAlert = UIAlertController(title: "Reset", message: "All saved preferences will be lost.", preferredStyle: UIAlertControllerStyle.alert)
            
            resetAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{(action: UIAlertAction!) in
                self.performSegue(withIdentifier: "ResetSettingsRewind", sender: self)
                
            }))
            resetAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: {(action: UIAlertAction!) in
                //print ("Pushed Cancel")
                
            }))
            present(resetAlert, animated: true, completion: nil)
        }
    }
    
    @IBAction func openAboutTouch(_ sender: UIButton){
        if sender === aboutButton {
            let aboutAlert = UIAlertController(title: PayCalcViewController.aboutTitleText, message: PayCalcViewController.aboutBlurbText, preferredStyle: UIAlertControllerStyle.alert)
            aboutAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            
            present(aboutAlert, animated: true, completion: nil)
        }
    }
    
    /// Called by done button in toolbar above keyboard or picker.
    @objc func doneKeyboard(){
        self.view.endEditing(true)
    }
    
    fileprivate func initFieldSwitch(_ field: UITextField, switchView: UISwitch, val: Float?){
        let isActive = val != nil
        switchView.setOn(isActive, animated: false)
        field.text = isActive ? String(val!): ""
        setFieldActive(field, isActive: isActive)
    }
    
    fileprivate func setFieldActive (_ field: UITextField, isActive: Bool){
        field.isEnabled = isActive
        field.tintColor = isActive ? fieldActiveColor: fieldInactiveColor
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // Gets nil from indexPathForCell from switch case for some reason
        if indexPath == tableView.indexPath(for: resetTableCell){
            resetSettingsTouch(resetButton)
            return
        }
        if indexPath == tableView.indexPath(for: aboutTableCell){
            openAboutTouch(aboutButton)
            return
        }
        if indexPath == tableView.indexPath(for: provCell){
            provField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: yearCell){
            yearField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: weakTravelCell){
            weekTravField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: dayTravelCell){
            dayTravField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: loaCell){
            loaField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: mealCell){
            mealField.becomeFirstResponder()
            return
        }
        if indexPath == tableView.indexPath(for: wageCell){
            wageField.becomeFirstResponder()
            return
        }
        
        if indexPath == tableView.indexPath(for: vacCell){
            toggleSwitch(customVacSwitch)
            return
        }
        if indexPath == tableView.indexPath(for: monthDuesCell){
            toggleSwitch(monthDuesSwitch)
            return
        }
        if indexPath == tableView.indexPath(for: fieldDuesCell){
            toggleSwitch(fieldDuesSwitch)
            return
        }
        if indexPath == tableView.indexPath(for: nightsCell){
            toggleSwitch(nightSwitch)
            return
        }
    }
    
    fileprivate func toggleSwitch(_ tSwitch: UISwitch){
        tSwitch.setOn(!tSwitch.isOn, animated: true)
        fieldSwitchTouch(tSwitch)
    }

    
    // MARK: Navigation
    
    /// Called by save or cancel buttons
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIBarButtonItem, button == saveButton {
            saveData.prov = TaxManager.Province.activeProvs[provPicker.selectedRow(inComponent: 0)]
            saveData.year = yearOptions[yearPicker.selectedRow(inComponent: 0)]
            saveData.weekTravRate = Float(weekTravField.text ?? "")
            saveData.dayTravRate = Float(dayTravField.text ?? "")
            saveData.loaRate = Float(loaField.text ?? "")
            saveData.mealRate = Float(mealField.text ?? "")
            saveData.customWage = Float(wageField.text ?? "")

        } else {
            // Cancel Button
        }
        
        // Sent regardless to keep custom values from being nil when cancelled.
        saveData.vacRate = customVacSwitch.isOn ? Float(customVacField.text ?? "") : nil
        saveData.monthlyDues = monthDuesSwitch.isOn ? Float(monthDuesField.text ?? "") : nil
        saveData.fieldDues = fieldDuesSwitch.isOn ? Float(fieldDuesField.text ?? "") : nil
        saveData.nightPremium = nightSwitch.isOn ? Float(nightField.text ?? "") : nil
    }
}

// MARK: UIPickerViewDelegate
extension PaySettingsViewCont: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView{
        case provPicker:
            provField.text = provOptions[row]
        case yearPicker:
            yearField.text = yearOptions[row]
        default: break
        }
    }
}

// MARK: UIPickerViewDataSource
extension PaySettingsViewCont: UIPickerViewDataSource {
    // Number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Number of rows in Component
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case provPicker:
            return provOptions.count
        case yearPicker:
            return yearOptions.count
        default: return 0
        }
    }
    
    // Title for Row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case provPicker:
            return provOptions[row]
        case yearPicker:
            return yearOptions[row]
        default: return nil
        }
    }
}
