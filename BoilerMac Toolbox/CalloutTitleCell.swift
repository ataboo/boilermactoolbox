//
//  CalloutTitleCell.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

public class CalloutTitleCell: UITableViewHeaderFooterView {
    @IBOutlet weak var titleLabel: UILabel!
    override public var textLabel: UILabel? {
        return titleLabel
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        
        
    }

}
