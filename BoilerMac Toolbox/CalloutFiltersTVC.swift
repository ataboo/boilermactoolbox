//
//  CalloutFilterTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-10.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutFiltersTVC: UITableViewController {
    var filterTerms: [CallFilterTerm] = []
    var filterSections: [CallFilterTerm.TermType: [CallFilterTerm]] = [:]
    var filterKeys: [CallFilterTerm.TermType] = []
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        self.filterTerms = CalloutDataManager.getFilterTerms()
        
        setupTableView()
    }
    
    fileprivate func setupTableView() {
        tableView.register(UINib(nibName: "CalloutTitleCell", bundle: nil),
                              forHeaderFooterViewReuseIdentifier: "HeaderCell")
        
        tableView.bounces = false
        
        sortFilters()
    }
    
    private func sortFilters() {
        filterSections = [:]
        
        for filter in filterTerms {
            if filterSections[filter.termType] == nil {
                filterSections[filter.termType] = []
                
            }
            filterSections[filter.termType]?.append(filter)
        }
        
        filterKeys = filterSections.keys.sorted(by: {$0.rawValue < $1.rawValue})
    }
    
    // MARK: TableView Delegate/Data
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return filterKeys[section].string
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return filterKeys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterSections[filterKeys[section]]!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "FilterCell")
        
        let filter = filterForRowAtIndexPath(indexPath)
        let accessoryImg = filter.inclusive ? CalloutConst.checkImage : CalloutConst.xImage
        
        cell?.textLabel?.text = filter.keyWord
        
        cell?.imageView?.image = accessoryImg?.resizedAndTintable(0.6)
        cell?.imageView?.tintColor = filter.inclusive ? UIColor.green : UIColor.red
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
        
        header?.textLabel?.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showEditFilter(filterForRowAtIndexPath(indexPath))
    }
    
    private func filterForRowAtIndexPath(_ indexPath: IndexPath) -> CallFilterTerm {
        return filterSections[filterKeys[(indexPath as NSIndexPath).section]]![(indexPath as NSIndexPath).row]
    }
    
    // MARK: Navigation
    
    private func showEditFilter(_ filterTerm: CallFilterTerm) {
        filterTerms = filterTerms.filter({
            $0.termType != filterTerm.termType ||
            $0.keyWord != filterTerm.keyWord
        })
        
        performSegue(withIdentifier: "ShowFilterEdit", sender: filterTerm)
    }
    
    @IBAction func rewindFromCalloutSettings(_ sender: UIStoryboardSegue) {
        if let filterEditTVC = sender.source as? CalloutFilterEditTVC {
            if let filterTerm = filterEditTVC.returnFilter {
                print("Adding filter")
                filterTerms.append(filterTerm)
            }
            
            CalloutDataManager.commitFilterTerms(filterTerms)
            
            sortFilters()
            
            self.tableView.reloadData()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let senderFilter = sender as? CallFilterTerm,
            let filterEditTVC = segue.destination as? CalloutFilterEditTVC {
            
            filterEditTVC.filterTerm = senderFilter
        }
    }
}
