//
//  PulpMillScene.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class PulpMillScene: CounterScene {
    fileprivate static let atlasName: String = "PulpMill"
    fileprivate static let timberAtlasName: String = "Timber"
    fileprivate static let backgroundName: String = "PulpMillBackground"
    
    
    fileprivate var rollTextures: Array<SKTexture>
    fileprivate var timberTextures: Array<SKTexture>
    
    public required init(parentScene: SKScene){
        
        rollTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: PulpMillScene.atlasName), prefix: "Roll_")
        
        timberTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: PulpMillScene.timberAtlasName), prefix: "Timber_")
        
        super.init(parentScene: parentScene, atlasName: PulpMillScene.atlasName, backgroundName: PulpMillScene.backgroundName)
        
        earningsCounter.posOffset = CGPoint(x: 0, y: 0.27)
        
        self.aspectRatio = 1024/780
        
        let beltLoop = LoopAnimation(atlas: SKTextureAtlas(named: PulpMillScene.atlasName), prefix: "Belt_", frameLength: 40, sprite: centeredSprite("Belt_4"))
        addSpriteObject(beltLoop)
    }
    
    public required convenience init(parentScene: SKScene, atlasName: String, backgroundName: String) {
        self.init(parentScene: parentScene)
    }
    
    override open func nextFineAnim(_ startTime: Int, totalValue: Float, description: String) {
        let difference = Int((totalValue - earningsCounter.earnings) * 100)
        
        let rollAnim = OnceAnimation(textures: rollTextures, frameLength: 40, sprite: centeredSprite("Roll_000"))
        rollAnim.startAnimation(startTime - 300)
        
        addSpriteObject(rollAnim)
        
        makeFineLabel(startTime, contents: "\(difference)¢")
        
        earningsCounter.addEarningAnimation(startTime, newValue: totalValue, earningDescription: description)
    }
    
    fileprivate func makeFineLabel(_ startTime: Int, contents: String) {
        let fineLabel = OnceLabel(animProps: LabelAnimProps.sceneLabel(centerPos, screenSize: sceneSize, sceneType: .pulpMill), startTime: startTime, parentScene: parentScene)
        fineLabel.label.zPosition = -1
        fineLabel.label.text = contents
        
        spriteObjects.append(fineLabel)
    }
    
    override open func nextCoarseAnim(_ startTime: Int) {
        let timberAnim = OnceAnimation(textures: timberTextures, frameLength: 72, sprite: centeredSprite("Timber_00"))
        
        timberAnim.sprite.zPosition = 5
        
        timberAnim.startAnimation(startTime - 500)
        
        addSpriteObject(timberAnim)
    }
}
