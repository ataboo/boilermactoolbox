//
//  CashSettingsTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-26.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CashSettingsTVC: UITableViewController {
    
    var shiftStartDate: Date?
    private var newDate: Date?
    var lockScene = false
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var timePicker: UIDatePicker!
    
    @IBOutlet weak var lockSceneToggle: ToggleSwitch!
    
    @IBAction func changedDate(_ sender: AnyObject) {
        newDate = timePicker.date
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if shiftStartDate == nil {
            print("CashSettingsTVC: Can't populate from data because shift start was not set.")
        } else {
            timePicker.setDate(shiftStartDate!, animated: false)
        }
        
        lockSceneToggle.setOn(lockScene, animated: false)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if sender as? UIBarButtonItem == saveButton {
            if newDate != nil{
                shiftStartDate = newDate
            }
            lockScene = lockSceneToggle.isOn
        }
    }
}
