//
//  TorqueTableViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-26.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class TorqueTableViewController: UITableViewController {
    
    // MARK: Properties
    @IBOutlet weak var patternSelectCell: UITableViewCell!
    @IBOutlet weak var studCountCell: UITableViewCell!
    @IBOutlet weak var studCountTextField: UITextField!
    @IBOutlet weak var sequenceLabel: UILabel!
    @IBOutlet weak var studCountLabel: UILabel!
    @IBOutlet weak var infoButton: UIBarButtonItem!
    
    var torqueData: TorqueData!
    var selectedPattern: Pattern?
    var studCountPicker: UIPickerView!
    var studCountArray = TorqueData.studCountArray()
    
    static var resetFlag = false

    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.torqueData = TorqueTableViewController.loadTorqueData()
        
        setupPicker()
        
        updateLabels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if TorqueTableViewController.resetFlag {
            resetTorqueData();
            TorqueTableViewController.resetFlag = false
        }
        
        updateLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func setupPicker(){
        self.studCountPicker = UIPickerView()
        studCountPicker.delegate = self
        studCountPicker.dataSource = self
        let pickerToolbar = UIToolbar()
        pickerToolbar.barStyle = UIBarStyle.default
        pickerToolbar.isTranslucent = true
        pickerToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(TorqueTableViewController.donePicker))
        let barSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([doneButton, barSpace], animated: false)
        pickerToolbar.isUserInteractionEnabled = true
        
        studCountTextField.inputAccessoryView = pickerToolbar
        studCountTextField.inputView = studCountPicker
        
        if let studCountIndex = studCountArray.index(of: torqueData.studCount){
            studCountPicker.selectRow(studCountIndex, inComponent: 0, animated: false)
        } else {
            print("TorqueTableViewController: Couldn't find index of ")
            studCountPicker.selectRow(0, inComponent: 0, animated: false)
        }
        
    }
    
    
    func updateLabels(){
        let pattern: Pattern = torqueData.getSelectedPattern()
        //patternSelectCell.imageView!.image = pattern.image
        patternSelectCell.textLabel!.text = pattern.title
        studCountLabel.text = "Stud Count:"
        studCountTextField.text = "\(torqueData.studCount)"
        
        updatePattern()
    }
    
    @objc func donePicker(){
        torqueData.studCount = studCountArray[studCountPicker.selectedRow(inComponent: 0)]
        let studCount = torqueData.studCount
        
        studCountTextField.text = "\(studCount)"
        studCountTextField.resignFirstResponder()
        if let studCountCellIndexPath = tableView.indexPath(for: studCountCell){
            tableView.deselectRow(at: studCountCellIndexPath, animated: true)
        }
        
        updatePattern()
        
        saveTorqueData()
    }
    
    fileprivate func updatePattern(){
        sequenceLabel.text = torqueData.makeSequence(torqueData.getSelectedPattern(), studCount: torqueData.studCount)
    }
    
    
    // MARK: - Table view functions
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell: UITableViewCell = tableView.cellForRow(at: indexPath)!
        
        if selectedCell == studCountCell{
            studCountTextField.becomeFirstResponder()
            return
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let torqueDetailController = segue.destination as? TorqueDetailTableViewController{
            torqueDetailController.torqueData = torqueData
        }
    }
    
    @IBAction func rewindFromPatternSelect(_ segue: UIStoryboardSegue){
        updateLabels()
        saveTorqueData()
    }
    
    
    
    // MARK: NSCoding
    
    func saveTorqueData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(torqueData, toFile: TorqueData.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save TorqueData...")
        }
    }
    
    static func loadTorqueData() -> TorqueData {
        if let loadedTorqueData = NSKeyedUnarchiver.unarchiveObject(withFile: TorqueData.ArchiveURL.path) as? TorqueData {
            return loadedTorqueData
        }
        print("TorqueTableViewController Failed to load TorqueData... Instantiating new one.")
        return TorqueData()
    }
    
    fileprivate func resetTorqueData() {
        print("Resetting torqueData...");
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(atPath: TorqueData.ArchiveURL.path)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        self.torqueData = TorqueData()  // Instantiate new one if file not loaded
    }
}

// MARK: UIPickerViewDelegate
extension TorqueTableViewController: UIPickerViewDelegate{
    @objc(numberOfComponentsInPickerView:)
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return studCountArray.count
    }
}

// MARK: UIPickerViewDataSource
extension TorqueTableViewController: UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let rowString =  "\(studCountArray[row])"
        return rowString
    }
}

