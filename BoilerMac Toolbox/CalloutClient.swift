//
//  Client.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation

import SwiftyJSON
import Alamofire

class CalloutClient {
    enum APNKeyTransaction: String {
        case add = "add"
        case remove = "remove"
    }
    
    enum CallError: String {
        case config = "Configuration Error"
        case network = "Network Error"
        case url = "URL Error"
        case server = "Server Error"
        case data = "Data Error"
        case authentication = "Authentication"
        case denied = "Permission Denied"
        case timeout = "Session Timed Out"
        
        static func parseErrorCode(httpErrorCode: Int) -> CallError {
            switch httpErrorCode {
            case 1009:
                return .network
            case 404:
                return .url
            case 1001:
                return .timeout
            default:
                return .server
            }
        }
    }
    
    private static var clientInstance: CalloutClient?
    
    private static let configFileName = "callout_config"
    //private static let configFileName = "callout_local"
    private let authKey: String?
    private let calloutEndpoint: String?
    private let apnEndpoint: String?
    var sessionManager: SessionManager!
    
    init() {
        let config = CalloutClient.parseConfig()
        
        self.authKey = config?["ios_auth_token"].string
        self.calloutEndpoint = config?["callout_endpoint"].string
        self.apnEndpoint = config?["apn_endpoint"].string
        sessionManager = getSessionManager()
    }
    
    public static func instance() -> CalloutClient {
        if clientInstance == nil {
            clientInstance = CalloutClient()
        }
        
        return clientInstance!
    }
    
    func getCallout(_ callback: @escaping ([CalloutObject])->()) {
        guard let authorization = authKey, let url = calloutEndpoint else {
            print("No Auth Key or Endpoint set!!!")
            callback([])
            return
        }
        
        sessionManager.request(url, method: .get, headers: getAuthHeaders(authKey: authorization))
            .validate(statusCode: 200..<300)
            .responseJSON
        { response in switch response.result
            {
            case .success(let jsonReturn):
//                print("Got JSON: \(jsonReturn)")
                callback(CalloutObject.parseJSON(JSON(jsonReturn)))
            case .failure(let error):
                print ("Request failed with error: \(error)")
                callback([])
            }
        }
    }
    
    func setAPNKey(key: String, filtersJSON: [CallFilterTerm], sendTest: Bool = false, callback: @escaping (Bool, CallError?)->Void) {
        guard let authorization = authKey,
            let apnEndpoint = apnEndpoint,
            let jsonFilters = convertFilters(filters: filtersJSON)
        else {
            print("No Auth Key or APNEndpoint set!!!")
            callback(false, .config)
            return
        }
        
        var arguments: [String: String] = ["transaction": APNKeyTransaction.add.rawValue,
                                           "apn_key": key]
        if sendTest {
            arguments["send_test"] = "true"
        }
        
        guard let url = parseURL(baseURL: apnEndpoint, arguments: arguments) else {
            callback(false, .url)
            return
        }
        
        guard let netMessenger = NetworkMessenger.getInstance, netMessenger.reachable else {
            callback(false, .network)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue(authorization, forHTTPHeaderField: "Callout-Token")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonFilters
        
        sessionManager.request(request)
            .validate(statusCode: 200..<300)
            .responseJSON { response in switch response.result
            {
            case .success(let jsonReturn):
                print("Got return \(jsonReturn) from APN key change.")
                callback(true, nil)
            case .failure(let error):
                var errorType: CallError = .server
                if let errorCode = response.response?.statusCode {
                    errorType = CallError.parseErrorCode(httpErrorCode: errorCode)
                }
                
                print ("APN change failed with error: \(error)")
                callback(false, errorType)
            }
        }
    }
    
    func sendTestAPN(key: String, filtersJSON: [CallFilterTerm], callback: @escaping (Bool, CallError?)->()) {
        setAPNKey(key: key, filtersJSON: filtersJSON, sendTest: true, callback: callback)
    }
    
    func removeAPNKey(key: String, callback: @escaping (Bool, CallError?)->Void) {
        guard let authorization = authKey,
            let apnEndpoint = apnEndpoint
            else {
                print("No Auth Key or APNEndpoint set!!!")
                callback(false, .url)
                return
        }
        
        let arguments = [CalloutConst.transactionArg: APNKeyTransaction.remove.rawValue,
                         "apn_key": key]
        
        guard let url = parseURL(baseURL: apnEndpoint, arguments: arguments) else {
            callback(false, .config)
            return
        }
        
        guard let netMessenger = NetworkMessenger.getInstance, netMessenger.reachable else {
            callback(false, .network)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue(authorization, forHTTPHeaderField: "Callout-Token")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        sessionManager.request(request)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(let jsonReturn):
                    print("Got return \(jsonReturn) from APN key Removal")
                    callback(true, nil)
                case .failure(let error) :
                    var errorType: CallError = .network
                    
                    if let statusCode = response.response?.statusCode {
                        errorType = CallError.parseErrorCode(httpErrorCode: statusCode)
                    }
                    
                    print ("APN removal failed with error: \(error.localizedDescription)")
                    callback(false, errorType)
                }
        }
    }
    
    private func getSessionManager() -> SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10
        configuration.timeoutIntervalForResource = 10
        return Alamofire.SessionManager(configuration: URLSessionConfiguration.default)
    }
    
    private func getAuthHeaders(authKey: String) -> [String: String] {
        return ["Callout-Token": authKey, "Accept": "application/json"]
    }
    
    private func getAPNParams(key: String, transaction: APNKeyTransaction) -> [String: String] {
        return ["transaction": transaction.rawValue, "apn_key": key]
    }
    
    private func convertFilters(filters: [CallFilterTerm]) -> Data? {
        var filterJSON: [JSON] = []
        for filter in filters {
            filterJSON.append(filter.json)
        }
        
        do {
            return try JSON(filterJSON).rawData()
        } catch {
            print("Failed to parse filter data.")
            return nil
        }
    }
    
    private func parseURL(baseURL: String, arguments: [String: String]) -> URL? {
        let queries: [URLQueryItem] = arguments.map({URLQueryItem(name: $0, value: $1)})
        
        do {
            if var urlComponents = URLComponents(string: baseURL) {
                urlComponents.queryItems = queries
                return try urlComponents.asURL()
        }
        } catch {
            print("Failed to parse url: \(baseURL)\n with arguments: \(arguments)")
        }
        
        return nil
    }
    
    static private func parseConfig() -> JSON? {
        guard let path = Bundle.main.path(forResource: configFileName, ofType: "json") else {
            print("Couldn't find Config file.")
            return nil
        }

        //TODO: change this?
        //let url = URL.contentsOf(path)
        if let jsonString = try? Data(contentsOf: URL(fileURLWithPath: path)) {
            return JSON(data: jsonString)
        } else {
            print("Couldn't parse Config.json.")
            return nil
        }
    }
}
