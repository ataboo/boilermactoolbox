//
//  OnceLabel.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-26.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

public struct LabelAnimProps{
    
    public var startPoint: CGPoint
    public var endPoint: CGPoint
    public var startSize: CGFloat
    public var endSize: CGFloat
    public var animationLength: Int
    public var contents: String
    
    init(startPoint: CGPoint, endPoint: CGPoint, startSize: CGFloat, endSize: CGFloat, animationLength: Int, contents: String){
        
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.startSize = startSize
        self.endSize = endSize
        self.animationLength = animationLength
        self.contents = contents
    }
    
    static func sceneLabel(_ screenCenterPos: CGPoint, screenSize: CGSize, sceneType: CashCounterData.CounterSceneType) -> LabelAnimProps {
        
        let screenOffset = screenCenterPos.sub(screenSize.width / 2, y: screenSize.height / 2)
        
        var startFactor = CGSize(width: 0.5, height: 0.5)
        var endFactor = CGSize(width: 0.5, height: 0.5)
        
        switch(sceneType){
        case .pulpMill:
            startFactor = CGSize(width: 0.74, height: 0.24)
            endFactor = CGSize(width: 1.0, height: 0.5)
        case.hydroDam:
            startFactor = CGSize(width: 0.175, height: 0.48)
            endFactor = CGSize(width: startFactor.width, height: 1.0)
        case .nukePlant:
            startFactor = CGSize(width: 0.285, height: 0.74)
            endFactor = CGSize(width: startFactor.width, height: 0.9)
        case .oilDrip:
            startFactor = CGSize(width: 0.74, height: 0.04)
            endFactor = CGSize(width: startFactor.width, height: 0.5)
            
        }
        
        var label = defaultLabel(screenCenterPos, screenSize: screenSize)
        
        label.startPoint = CGPoint(x: screenSize.width * startFactor.width, y: screenSize.height * startFactor.height).add(screenOffset)
        
        label.endPoint = CGPoint(x: screenSize.width * endFactor.width, y: screenSize.height * endFactor.height).add(screenOffset)
        
        return label
        
    }
    
    public static func defaultLabel(_ screenCenterPos: CGPoint, screenSize: CGSize) -> LabelAnimProps {
        let startPoint = screenCenterPos
        let endPoint = screenCenterPos
        let startSize = CGFloat(0.3)
        let endSize = CGFloat(5)
        let animationLength = 1500
        
        return LabelAnimProps(startPoint: startPoint, endPoint: endPoint, startSize: startSize, endSize: endSize, animationLength: animationLength, contents: "1¢")
    }
}

open class OnceLabel: NSObject {
    open var active = true
    open var label: SKLabelNode
    open var labelStart: Int
    
    fileprivate let parentScene: SKScene
    fileprivate let animProps: LabelAnimProps
    
    
    init(animProps: LabelAnimProps, startTime: Int, parentScene: SKScene){
        self.animProps = animProps
        self.parentScene = parentScene
        
        label = SKLabelNode(text: animProps.contents)
        label.position = animProps.startPoint
        label.setScale(animProps.startSize)
        label.fontColor = UIColor(red: 48/256, green: 139/256, blue: 39/256, alpha: 1)
        label.fontName = "helvetica-bold"
        
        label.zPosition = 20
        
        self.labelStart = startTime
    }
    
    
}

extension OnceLabel: SpriteObj {
    public func setIsActive(_ active: Bool) {
        
    }
    
    public func updatePosSize(_ centerPos: CGPoint, size: CGSize) {
        
    }
    
    public func update(_ currentTime: Int) -> Bool{
        if labelStart > currentTime {
            return false
        }
        
        let growAction = SKAction.scale(by: animProps.endSize, duration: Double(animProps.animationLength) / 1000.0)
        let fadeDelay = SKAction.wait(forDuration: Double(animProps.animationLength) / 2000.0)
        let fadeAction = SKAction.fadeOut(withDuration: Double(animProps.animationLength) / 2000.0)
        let fadeSequence = SKAction.sequence([fadeDelay, fadeAction])
        
        let moveAction = SKAction.move(to: animProps.endPoint, duration: Double(animProps.animationLength) / 500.0)
        let growAndFade = SKAction.group([growAction, fadeSequence, moveAction])
        parentScene.addChild(label)
        label.run(growAndFade, completion: {self.label.removeFromParent()
        })
        
        active = false
        
        return true
    }
    
    public func dispose(){
        label.removeFromParent()
    }
}
