//
//  CalloutTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutTVC: UIViewController {
    var dataManager: CalloutDataManager!
    var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeViews()
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //getCalls()
        
        CalloutAPNS.checkKeyToRemove()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(getCalls), name: Notification.Name(rawValue: CalloutAPNS.calloutRefreshKey), object: nil)
        
        print("Callout TVC did appear.")
        getCalls()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func makeViews() {
        self.tableView = UITableView(frame: CGRect.zero)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = UIColor.mercury
        self.view.addSubview(tableView)
        
        self.dataManager = CalloutDataManager(calloutTVC: self)
        tableView.dataSource = dataManager
        tableView.delegate = dataManager
        tableView.estimatedRowHeight = 68.0
        tableView.estimatedSectionHeaderHeight = 40.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView.layer.shadowRadius = 8.0
        tableView.layer.shadowColor = UIColor.black.cgColor
        tableView.layer.cornerRadius = 4
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CalloutTVC.getCalls), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        addConstraints()
    }
    
    private func addConstraints() {
        let views: [String: AnyObject] = ["tableView": tableView, "topBar": topLayoutGuide, "bottomBar": bottomLayoutGuide]
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[tableView]-10-|", options: [], metrics: nil, views: views))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[topBar]-[tableView]-[bottomBar]|", options: [], metrics: nil, views: views))
    }
    
    @objc func getCalls() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataManager.getCalls()
    }
    
    @IBAction func showCalloutDetail(_ detailData: CalloutObject) {
        let calloutDetailTVC = CalloutDetailTVC()
        calloutDetailTVC.hidesBottomBarWhenPushed = true
        calloutDetailTVC.callData = detailData
        
        self.navigationController?.pushViewController(calloutDetailTVC, animated: true)
    }
    
    @IBAction func rewindFromCalloutSettings(_ sender: UIStoryboardSegue) {
        // Need a target for the rewind from settings
        dataManager.refreshFilterMatches()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // On TableViewCell Click.
        if let detailTVC = segue.destination as? CalloutDetailTVC,
        let data = sender as? CalloutObject {
            detailTVC.callData = data
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        refreshControl.endRefreshing()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

}
