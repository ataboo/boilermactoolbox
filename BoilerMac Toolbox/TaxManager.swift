//
//  TaxManager.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-18.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation

// From http://timekl.com/blog/2014/06/02/learning-swift-ordered-dictionaries/ example

class TaxManager{
    // MARK: Properties
    enum Province: String{
        case FED = "FED"
        case BC = "BC"
        case AB = "AB"
        case SK = "SK"
        case MB = "MB"
        case ON = "ON"
        case QC = "QC"
        case NB = "NB"
        case NS = "NS"
        case CB = "CB"  //Uses NS tax and own wages
        case PE = "PE"  //Uses NS wages
        case NL = "NL"
        
        static let activeProvs = [BC, AB, SK, MB, ON, QC, NB, PE, NS, CB, NL]
    }
    
    public var taxCalcs: [Province: TaxCalc] = [
        .BC: BCTaxCalc(),
        .AB: ABTaxCalc(),
        .SK: SKTaxCalc(),
        .MB: MBTaxCalc(),
        .ON: ONTaxCalc(),
        .QC: QCTaxCalc(),
        .NB: NBTaxCalc(),
        .PE: PETaxCalc(),
        .NS: NSTaxCalc(),
        .CB: CBTaxCalc(),
        .NL: NLTaxCalc()
    ]
    
    //Update Me Please!!!
    static let defaultYearName = "2018"
    static let expectedYearCount = 6
    static let defaultWageName = "Journeyperson"
    static let frenchDefaultWageName = "Compagnon"
    
    var activeProvince: TaxCalc!
    
    // MARK: Initialization
    init(prov: Province, year: String){
        setProvYear(prov, year: year)
    }
    
    func setProvYear(_ prov: Province, year: String){
        activeProvince = taxCalcs[prov]!
        activeProvince.setYear(yearString: year)
    }
    
    convenience init(prov: Province){
        self.init(prov: prov, year: TaxManager.defaultYearName)
    }
    
    func getTax(taxableGross: Double, taxExempt: Double) -> (fedTax: Double, provTax: Double, cppVal: Double, eiVal: Double) {
        return activeProvince.calcTax(taxableGross: taxableGross, taxExempt: taxExempt)
    }
    
    func getWages() -> OrderedDictionary<String, Double>?{
        return activeProvince.provTaxVal.wages
    }
    
    func getDefaultWageKey() -> String{
        let activeProvVal = activeProvince.provTaxVal
        
        if activeProvVal.wages!.containsKey(TaxManager.defaultWageName){
            return TaxManager.defaultWageName
        }
        
        for wageName: String in activeProvVal.wages!.keys {
            if wageName.contains(TaxManager.defaultWageName) ||
                wageName.contains(TaxManager.frenchDefaultWageName) {
                return wageName
            }
        }
        
        print("Default wage rate not found in \(activeProvVal.prov.rawValue) or wages Dictionary empty")
            return activeProvVal.wages!.keys.first!
    }
    
    public func splitShiftHours(dayRef: DayReference, fourTens: Bool) -> [Float] {
        // Day is custom, so should override split.
        if dayRef.hours.count == 3 {
            return dayRef.hours
        }
        
        let taxValDay = getTaxValDay(dayRef: dayRef, fourTens: fourTens)
        
        return splitHours(hourCount: dayRef.hours[0], taxValDay: taxValDay)
    }
    
    /**
     Get an array of integers representing the maximum number of straight and OT hours in a day
     [ straight hours, ot hours ]
     */
    private func getTaxValDay(dayRef: DayReference, fourTens: Bool) -> [Int] {
        if dayRef.isWeekend {
            return activeProvince.provTaxVal.payWeekend!
        }
        
        if dayRef.isHoliday {
            return activeProvince.provTaxVal.payHoliday!
        }
        
        if fourTens {
            if dayRef.isFriday {
                return activeProvince.provTaxVal.payFTFriday!
            } else {
                return activeProvince.provTaxVal.payFTWeekday!
            }
        } else {
            return activeProvince.provTaxVal.payWeekday!
        }
    }
    
    /**
     Split a shift's hours into [Straight, OT, Double] based on 'taxValDay' limits.
    */
    private func splitHours(hourCount: Float, taxValDay: [Int]) -> [Float] {
        let straight = hourCount.cage(floor: 0, ceiling: Float(taxValDay[0]))
        let ot = (hourCount - straight).cage(floor: 0, ceiling: Float(taxValDay[1]))
        let double = max(hourCount - straight - ot, 0.0)
        
        return [straight, ot, double]
    }
}
