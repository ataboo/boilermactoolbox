//
//  FlangeTableViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-08.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class FlangeTableViewController: UITableViewController {

    // MARK: Properties
    
    @IBOutlet weak var sizeSelect: UITableViewCell!
    @IBOutlet weak var ratingSelect: UITableViewCell!

    @IBOutlet weak var studCountCell: UITableViewCell!
    @IBOutlet weak var studDiameterCell: UITableViewCell!
    @IBOutlet weak var wrenchSizeCell: UITableViewCell!
    @IBOutlet weak var driftCell: UITableViewCell!
    @IBOutlet weak var b7mCell: UITableViewCell!
    @IBOutlet weak var b7Cell: UITableViewCell!
    
    let sizeLabel = "Flange Size"
    let ratingLabel = "Flange Rating"
    let studCountLabel = "Stud Count"
    let studDiameterLabel = "Stud Diameter"
    let wrenchSizeLabel = "Wrench Size"
    let driftLabel = "Drift Pin Size"
    let b7mTorqueLabel = "B7M Torque"
    let b7TorqueLabel = "B7 Torque"
    
    var lastTouchedCell: UITableViewCell!
    var flangeData = FlangeTableViewController.loadFlangeData()
    
    static var resetFlag = false
    
    
    // MARK:  Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    // Reload labels in case dynamic font size was changed while running
    override func viewDidAppear(_ animated: Bool) {
        if FlangeTableViewController.resetFlag {
            resetFlangeData();
            FlangeTableViewController.resetFlag = false
        }
        
        updateLabels()
        updateDetails()
    }
    
    func setupViews(){
        updateDetails()
    }
    
    // MARK: Actions
    
    @IBAction func rewindFromFlangeDetail(_ sender: UIStoryboardSegue){
        if let flangeDetailCont = sender.source as? FlangeDetailTVC {
            if flangeDetailCont.selectedIndex == nil{
                return
            }
            if lastTouchedCell === sizeSelect {
                flangeData.setSize(flangeDetailCont.selectedIndex!)
            } else {
                flangeData.setRating(flangeDetailCont.selectedIndex!)
            }
            // update calc?
            updateDetails()
            saveFlangeData()
        }
    }
    
    // When dynamic font sizes are changed while app is running they sometimes remove the text
    fileprivate func updateLabels(){
        sizeSelect.textLabel!.text = sizeLabel
        ratingSelect.textLabel!.text = ratingLabel
        studCountCell.textLabel!.text = studCountLabel
        studDiameterCell.textLabel!.text = studDiameterLabel
        wrenchSizeCell.textLabel!.text = wrenchSizeLabel
        driftCell.textLabel!.text = driftLabel
        b7mCell.textLabel!.text = b7mTorqueLabel
        b7Cell.textLabel!.text = b7TorqueLabel
    }
    
    fileprivate func updateDetails(){
        sizeSelect.detailTextLabel!.text = flangeData.displayVals.size
        ratingSelect.detailTextLabel!.text = flangeData.displayVals.rating
        studCountCell.detailTextLabel!.text = flangeData.displayVals.studCount
        studDiameterCell.detailTextLabel!.text = flangeData.displayVals.studDiameter + "\""
        wrenchSizeCell.detailTextLabel!.text = flangeData.displayVals.wrenchSize + "\""
        driftCell.detailTextLabel!.text = flangeData.displayVals.driftPinSize + "\""
        b7mCell.detailTextLabel!.text = flangeData.displayVals.b7mStrength + " ft-lbs"
        b7Cell.detailTextLabel!.text = flangeData.displayVals.b7Strength + " ft-lbs"
    }
    
    // MARK: NSCoding
    
    func saveFlangeData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(flangeData, toFile: FlangeData.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save FlangeData.")
        }
    }
    
    static func loadFlangeData() -> FlangeData {
        if let loadedFlangeData = NSKeyedUnarchiver.unarchiveObject(withFile: FlangeData.ArchiveURL.path) as? FlangeData {
            return loadedFlangeData
        }
        print("FlangeTableViewController Failed to load FlangeData... Instantiating new one.")
        return FlangeData()
    }
    
    fileprivate func resetFlangeData() {
        print("Resetting flangeData...");
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(at: FlangeData.ArchiveURL)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        self.flangeData = FlangeData();
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sendCell = sender as? UITableViewCell, let flangeDetailController = segue.destination as? FlangeDetailTVC {
            self.lastTouchedCell = sendCell
            if sendCell === sizeSelect{
                flangeDetailController.selectItems = flangeData.displayVals.sizesStyled
                flangeDetailController.selectTitle = sizeLabel
            }
            if sendCell === ratingSelect{
                flangeDetailController.selectItems = flangeData.displayVals.ratingsStyled
                flangeDetailController.selectTitle = ratingLabel
            }
        }
    }
}
