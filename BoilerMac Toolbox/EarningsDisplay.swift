//
//  EarningsDisplay.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-27.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class EarningsDisplay: NSObject {
    open var active = true
    open var earningsCounter = SKLabelNode(text: "$----.--")
    open var dropShadow = SKLabelNode(text: "$----.--")
    open var rateIndicator = SKLabelNode(text: CashCounterData.EarningType.OffShift.rawValue)
    open var rateShadow = SKLabelNode(text: CashCounterData.EarningType.OffShift.rawValue)
    open var earnings: Float = 0
    open var posOffset = CGPoint(x: 0, y: 0)
    
    fileprivate var changeTimes = Array<(time: Int, value: Float)>()
    fileprivate var centerPos = CGPoint(x: 50, y: 50)
    fileprivate var sceneSize = CGSize(width: 100, height: 100)
    fileprivate var textColor = UIColor.gray
    fileprivate let parentScene: SKScene
    
    fileprivate let dropShadowOffset = CGPoint(x: -1,y: -1)
    
    init(parentScene: SKScene, posOffset: CGPoint){
        self.parentScene = parentScene
        
        earningsCounter.fontSize = 40
        earningsCounter.fontName = "helvetica-bold"
        earningsCounter.fontColor = textColor
        earningsCounter.zPosition = 5.0
        
        
        rateIndicator.fontSize = 20
        rateIndicator.fontName = earningsCounter.fontName
        rateIndicator.fontColor = textColor
        rateIndicator.zPosition = 5.0
        
        
        parentScene.addChild(earningsCounter)
        parentScene.addChild(rateIndicator)
        
        self.posOffset = posOffset
        super.init()
        
        initDropShadow(earningsCounter, shadow: dropShadow)
        parentScene.addChild(dropShadow)
        initDropShadow(rateIndicator, shadow: rateShadow)
        parentScene.addChild(rateShadow)
    }
    
    fileprivate func initDropShadow(_ label: SKLabelNode, shadow: SKLabelNode) {
        shadow.text = label.text
        shadow.position = label.position.add(dropShadowOffset)
        shadow.fontName = label.fontName
        shadow.fontSize = label.fontSize
        shadow.fontColor = UIColor.black
        shadow.zPosition = label.zPosition - 1.0
    }
    
    open func addEarningAnimation(_ time: Int, newValue: Float, earningDescription: String) {
        changeTimes.append((time: time, value: newValue))
        
        changeTimes.sort(by: {$0.time < $1.time})
        
        setEarningDescription(earningDescription)
    }
    
    open func manualSetEarnings(_ earnings: Float, earningDescription: String){
        self.earnings = earnings
        changeTimes.removeAll()
        
        earningsCounter.text = String(format: "$%.2f", self.earnings)
        dropShadow.text = earningsCounter.text
        setEarningDescription(earningDescription)
    }
    
    fileprivate func startAnimation(_ newValue: Float){
        let growAction = SKAction.scale(to: 1.1, duration: 0.05)
        let shrinkAction = SKAction.scale(to: 1, duration: 0.05)
        self.earnings = newValue
        
        let animCompletion = {(label: SKLabelNode) -> Void in
            label.text = String(format: "$%.2f", newValue)
            label.run(shrinkAction)
        }
        
        earningsCounter.run(growAction, completion: {animCompletion(self.earningsCounter)})
        
        dropShadow.run(growAction, completion: {animCompletion(self.dropShadow)})
    }
    
    fileprivate func setEarningDescription(_ description: String){
        switch description {
        case CashCounterData.EarningType.OffShift.rawValue:
            textColor = UIColor.darkGray
            break
        case CashCounterData.EarningType.StraightTime.rawValue:
            textColor = UIColor(red: 48/256, green: 139/256, blue: 39/256, alpha: 1)
            break
        case CashCounterData.EarningType.Overtime.rawValue:
            textColor = UIColor.orange
            break
        case CashCounterData.EarningType.Holiday.rawValue,
        CashCounterData.EarningType.WeekDouble.rawValue,
        CashCounterData.EarningType.WeekendDouble.rawValue:
            textColor = UIColor.yellow
            break
        default:
            break
        }

        rateIndicator.text = description
        rateIndicator.fontColor = textColor
        earningsCounter.fontColor = textColor
        
        rateShadow.text = description
    }
}

extension EarningsDisplay: SpriteObj {
    public func setIsActive(_ active: Bool){
        self.active = active
    }
    
    public func updatePosSize(_ centerPos: CGPoint, size: CGSize){
        self.centerPos = centerPos
        self.sceneSize = size
        
        earningsCounter.position = CGPoint(x: centerPos.x + posOffset.x * size.width, y: centerPos.y + posOffset.y * size.height)
        
        earningsCounter.position = centerPos.add(posOffset.x * size.width, y: posOffset.y * size.height)
        dropShadow.position = earningsCounter.position.add(dropShadowOffset)
        
        rateIndicator.position = earningsCounter.position.sub(Float(0), y: Float(20))
        rateShadow.position = rateIndicator.position.add(dropShadowOffset)
    }
    
    public func update(_ currentTime: Int) -> Bool {
        var cullFlag = false;
        
        for pair: (time: Int, value: Float) in changeTimes {
            if currentTime < pair.time {
                return false
            }
            
            startAnimation(pair.value)
            cullFlag = true
        }
        
        if cullFlag{
            changeTimes = changeTimes.filter{$0.time >= currentTime}
        }
        
        return false
    }
    
    public func dispose(){
        earningsCounter.removeFromParent()
        rateIndicator.removeFromParent()
    }
    
    
}
