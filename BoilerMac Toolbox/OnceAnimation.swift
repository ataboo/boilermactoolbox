//
//  SpriteAnimation.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-21.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class OnceAnimation: NSObject {
    open var active = false
    open var sprite: SKSpriteNode
    
    fileprivate var textures = Array<SKTexture>()
    fileprivate let frameLength: Int
    fileprivate let animationLength: Int
    fileprivate var startTime = 0
    
    init(textures: Array<SKTexture>, frameLength: Int, sprite: SKSpriteNode){
        self.sprite = sprite
        self.frameLength = frameLength
        self.textures = textures
        animationLength = textures.count * self.frameLength
    }
}

extension OnceAnimation: SpriteObj {
    public func keyForTime(_ currentTime: Int) -> SKTexture? {
        if !active || textures.count == 0 {
            return nil
        }
        
        if startTime + animationLength <= currentTime {
            active = false
            return textures[0]
        }
        
        if startTime > currentTime {
            return textures[0]
        }
        
        let textureIndex = (currentTime - startTime) * textures.count / animationLength
        
        return textures[textureIndex]
    }
    
    public func setIsActive(_ active: Bool) {
        self.active = active
    }
    
    public func startAnimation(_ startTime: Int) {
        self.startTime = startTime
        active = true
    }
    
    public func updatePosSize(_ centerPos: CGPoint, size: CGSize){
        sprite.position = centerPos
        sprite.size = size
    }
    
    public func dispose(){
        sprite.removeFromParent()
    }
}


