//
//  FlangeDetailTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-08.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class FlangeDetailTVC: UITableViewController{
    
    // MARK: Properties
    var selectedIndex: Int?
    var selectTitle: String?
    var selectItems: [String]?
    // MARK: Initilization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectTitle != nil {
            self.navigationItem.title = selectTitle
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return selectItems == nil ? 0: 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return selectItems?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCell", for: indexPath)
        // Configure the cell...
        if selectItems != nil {
            cell.textLabel?.text = selectItems![(indexPath as NSIndexPath).row]
        }
        
        return cell
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectCell = sender as? UITableViewCell {
            self.selectedIndex = (tableView.indexPath(for: selectCell)! as NSIndexPath).row
        }
    }
}
