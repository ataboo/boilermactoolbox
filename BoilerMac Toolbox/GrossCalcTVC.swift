//
//  GrossCalcTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-27.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class GrossCalcTVC: UITableViewController, UITextFieldDelegate {
    // MARK: Properties
    
    let provLabelText = "Province"
    let yearLabelText = "Tax Year"
    let grossLabelText = "Gross (week)"
    let provTaxLabelText = "Provincial Tax"
    let fedTaxLabelText = "Federal Tax"
    let netLabelText = "Net"
    
    @IBOutlet weak var provSelectionCell: UITableViewCell!
    @IBOutlet weak var yearSelectionCell: UITableViewCell!
    @IBOutlet weak var provTaxCell: UITableViewCell!
    @IBOutlet weak var fedTaxCell: UITableViewCell!
    @IBOutlet weak var netCell: UITableViewCell!
    @IBOutlet weak var calcButton: UIButton!
    
    @IBOutlet weak var grossCell: UITableViewCell!
    @IBOutlet weak var grossLabel: UILabel!
    @IBOutlet weak var grossField: UITextField!
    
    var lastTouchedCell: UITableViewCell!
    var grossData = GrossCalcTVC.loadGrossData()
    static var resetFlag = false;
        
    // MARK:  Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectAll(self)
    }

    
    func setupViews(){
        provSelectionCell.detailTextLabel!.text = grossData.taxManager.activeProvince.fullName
        yearSelectionCell.detailTextLabel!.text = grossData.activeYear
        grossField.text = "\(grossData.grossVal)"
        grossField.delegate = self

        // Add done button to grossField keyboard
        let doneToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(GrossCalcTVC.doneKeyboard))
        doneToolbar.items = [doneButton, flexSpace]
        doneToolbar.sizeToFit()

        grossField.inputAccessoryView = doneToolbar
    }
    
    // Prevents change to font size during runtime killing text in labels
    override func viewDidAppear(_ animated: Bool) {
        // Flag will be set true on PayCalcSettings reset
        if GrossCalcTVC.resetFlag{
            resetDataToDefault()
            GrossCalcTVC.resetFlag = false;
            setupViews()
        }
        
        updateLabels()
        calculateTouch(calcButton)
    }

    // MARK: Actions
    
    @IBAction func calculateTouch(_ sender: AnyObject){
        grossData.grossVal = Double(grossField.text ?? "0.0") ?? Double(0.0)
        saveGrossData()
        grossData.updateCalc()
        updateDetails()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == tableView.indexPath(for: grossCell){
            grossField.becomeFirstResponder()
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
    }
    
    /// Called by done button in toolbar above keyboard.
    @objc func doneKeyboard(){
        self.view.endEditing(true)
        calculateTouch(calcButton)
    }
    
    func updateLabels(){
        provSelectionCell.textLabel!.text = provLabelText
        yearSelectionCell.textLabel!.text = yearLabelText
        grossLabel.text = grossLabelText
        provTaxCell.textLabel!.text = provTaxLabelText
        fedTaxCell.textLabel!.text = fedTaxLabelText
        netCell.textLabel!.text = netLabelText
    }
    
    func updateDetails(){
        provSelectionCell.detailTextLabel!.text = grossData.taxManager.activeProvince.fullName
        yearSelectionCell.detailTextLabel!.text = grossData.activeYear
        
        provTaxCell.detailTextLabel!.text = String(format: "$%.2f", grossData.provTax)
        fedTaxCell.detailTextLabel!.text = String(format: "$%.2f", grossData.fedTax)
        netCell.detailTextLabel!.text = String(format: "$%.2f", grossData.netPay)
    }
    
    // MARK: NSCoding
    
    func saveGrossData(){
        grossData.saveData()
    }
    
    static func loadGrossData() -> GrossData {
        let data = GrossData()
        data.loadData()
        
        return data
    }
    
    fileprivate func resetDataToDefault() {
        print("Resetting grossData...");
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(atPath: GrossData.ArchiveURL.path)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        self.grossData = GrossData()  // Will instantiate new one if file not loaded
    }

    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sendCell = sender as? UITableViewCell , sendCell === provSelectionCell || sendCell === yearSelectionCell {
            self.lastTouchedCell = sendCell
            let detailController = segue.destination as! GrossCalcDetailTVC
            if lastTouchedCell === provSelectionCell {
                detailController.selectTitle = "Select Province"
                detailController.selectItems = grossData.provNames
            } else {
                detailController.selectTitle = "Select Year"
                detailController.selectItems = grossData.yearNames
            }
        }
    }
    
    @IBAction func rewindFromGrossDetail(_ sender: UIStoryboardSegue){
        if let grossDetailCont = sender.source as? GrossCalcDetailTVC {
            if let selectedIndex = grossDetailCont.selectedIndex, let selectedValue = grossDetailCont.selectItems?[selectedIndex]{
                //print("Got index: \(selectedIndex), value: \(selectedValue) from GrossDetailTVC")
                if lastTouchedCell === provSelectionCell{
                    grossData.activeProv = TaxManager.Province.activeProvs[selectedIndex]
                    provSelectionCell.detailTextLabel!.text = grossData.taxManager.activeProvince.fullName
                } else {
                    grossData.activeYear = selectedValue
                    yearSelectionCell.detailTextLabel!.text = grossData.activeYear
                }
                calculateTouch(calcButton)
            } else {
                //print("GrossCalcTVC: Failed to get selectedIndex or selectedValue from GrossDetailTVC.")
            }
        }
    }

}
