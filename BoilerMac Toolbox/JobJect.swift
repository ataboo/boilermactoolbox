//
//  Jobject.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-08.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation

open class JobJect {
    enum JobQualification: String {
        case Default = "?"
        case Apprentice = "A"
        case Journeyman = "J"
        case BPressure = "B"
        case Foreman = "F"
    }
    
    enum JobClass: String {
        case Default = "flangeIcon"
        case Fitter = "hammerWrench"
        case Welder = "stinger"
        case Rigger = "liftIcon"
        case Supervision = "whip"
    }
    
    open class JobType {
        let determinative: Bool
        let keyWords: [String]
        let qualification: JobQualification?
        let jobClass: JobClass?
        
        init(keyWords: [String], qualification: JobQualification?, jobClass: JobClass?, determinative: Bool = false) {
            self.determinative = determinative
            self.keyWords = keyWords
            self.qualification = qualification
            self.jobClass = jobClass
            
            if determinative && qualification == nil && jobClass == nil {
                fatalError("A determinative JobType must have class and qualification set.")
            }
        }
        
        init(jobClass: JobClass, qualification: JobQualification) {
            self.determinative = false
            self.keyWords = []
            self.qualification = qualification
            self.jobClass = jobClass
        }
        
        func checkKeywords(_ jobRaw: String) -> Bool {
            return jobRaw.containsAnyOfTheseWords(words: keyWords)
        }
    }
    
    static let jobTypes: [JobType] = [
        JobType(keyWords: CalloutConst.KeyWords.fitter,
            qualification: .Journeyman, jobClass: .Fitter, determinative: true),
        JobType(keyWords: CalloutConst.KeyWords.bWelder,
            qualification: .BPressure, jobClass: .Welder, determinative: true),
        JobType(keyWords: CalloutConst.KeyWords.rigger,
            qualification: .Journeyman, jobClass: .Rigger, determinative: true),
        JobType(keyWords: CalloutConst.KeyWords.foreman,
            qualification: .Foreman, jobClass: .Supervision, determinative: true),
        JobType(keyWords: CalloutConst.KeyWords.apprenticeFitter,
                qualification: .Apprentice, jobClass: .Fitter, determinative: true),
        JobType(keyWords: CalloutConst.KeyWords.apprenticeWelder,
            qualification: .Apprentice, jobClass: .Welder),
        JobType(keyWords: CalloutConst.KeyWords.journeyman,
                qualification: .Journeyman, jobClass: nil),
        JobType(keyWords: CalloutConst.KeyWords.apprentice,
                qualification: .Apprentice, jobClass: nil),
        JobType(keyWords: CalloutConst.KeyWords.welder,
            qualification: nil, jobClass: .Welder)
    ]
    
    let jobName: String
    let jobCount: String
    let jobType: JobType
    var matchLevel: CallFilterTerm.MatchLevel = .none
    
    init(jobDescription: String) {
        var splitDescription = jobDescription.components(separatedBy: " ")
        self.jobCount = splitDescription.removeLast()
        jobName = splitDescription.joined(separator: " ").capitalizeNonAcronyms()
        
        jobType = JobJect.parseJobType(jobDescription)
    }
    
    init(nameHire rawNameHire: String) {
        self.jobName = rawNameHire.capitalizeNonAcronyms()
        self.jobCount = ""
        
        if let nameHireStart = rawNameHire.range(of: "(")?.lowerBound {
            let nameHireDetails = rawNameHire[nameHireStart..<rawNameHire.endIndex]
            self.jobType = JobJect.parseJobType(nameHireDetails.trimmingCharacters(in: CharacterSet(charactersIn: "() ")))
        } else {
            self.jobType = JobJect.parseJobType(rawNameHire)
//            self.jobType = JobType(jobClass: .Default, qualification: .Default)
        }
    }
    
    static func parseJobType(_ jobRaw: String) -> JobType {
        var jobQual = JobQualification.Default
        var jobClass = JobClass.Default
        
        for jobType in jobTypes {
            if jobType.checkKeywords(jobRaw) {
                jobQual = jobType.qualification ?? jobQual
                jobClass = jobType.jobClass ?? jobClass
                
                if jobType.determinative {
                    return JobType(jobClass: jobClass, qualification: jobQual)
                }
            }
        }
        
        return JobType(jobClass: jobClass, qualification: jobQual)
    }
}
