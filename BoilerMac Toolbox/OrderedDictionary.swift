import Foundation

struct OrderedDictionary<Tk: Hashable, Tv>{
    var keys: Array<Tk> = []
    var values: Dictionary<Tk, Tv> = [:]
    var count: Int {
        get{
            return keys.count
        }
    }
    
    init() {}
    
    subscript(index: Int) -> Tv? {
        get {
            let key = self.keys[index]
            return self.values[key]
        }
        set(newValue){
            let key = self.keys[index]
            if (newValue != nil){
                self.values[key] = newValue
            } else {
                self.values.removeValue(forKey: key)
                self.keys.remove(at: index)
            }
        }
    }
    
    subscript(key: Tk) -> Tv? {
        get{
            return self.values[key]
        }
        set(newValue){
            if newValue == nil{
                self.values.removeValue(forKey: key)
                self.keys = self.keys.filter {$0 != key}
                return
            }
            
            let oldValue = self.values.updateValue(newValue!, forKey: key)
            if oldValue == nil {
                self.keys.append(key)
            }
        }
    }
    
    func containsKey(_ key: Tk) -> Bool {
        return keys.contains(key)
    }
}
