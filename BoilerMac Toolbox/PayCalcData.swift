//
//  PayCalcData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-15.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation
import UIKit

class PayCalcData: NSObject, NSCoding{
    
    /// Keys for saving values via NSCoder.
    enum SaveKeys: String {
        /// key for screenValues save.
        case SCREEN_VALS = "screen_vals"
        /// suffix for dayRef isHoliday key.
        case HOLIDAY = "_isholiday"
        /// suffix for dayRef hours key.
        case HOURS = "_hours"
        /// suffix for dayRef preset key.
        case PRESET = "_preset"
    }
    
    //MARK: Properties.
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    /// Path for PayCalcData Archive file.
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("paycalcdata")
    
    /// Keys that match corresponding Restoration Identifiers on day buttons.
    static let dayKeys = ["sundayButton", "mondayButton", "tuesdayButton", "wednesdayButton", "thursdayButton", "fridayButton", "saturdayButton"]
    /// Dictionary of values corresponding to week days.
    var dayRefs:Dictionary<String, DayReference> =
    [dayKeys[0]: DayReference(resID: dayKeys[0], name: "Sunday", shortHand: "Sun"),
        dayKeys[1]: DayReference(resID: dayKeys[1], name: "Monday", shortHand: "Mon"),
        dayKeys[2]: DayReference(resID: dayKeys[2], name: "Tuesday", shortHand: "Tue"),
        dayKeys[3]: DayReference(resID: dayKeys[3], name: "Wednesday", shortHand: "Wed"),
        dayKeys[4]: DayReference(resID: dayKeys[4], name: "Thursday", shortHand: "Thu"),
        dayKeys[5]: DayReference(resID: dayKeys[5], name: "Friday", shortHand: "Fri"),
        dayKeys[6]: DayReference(resID: dayKeys[6], name: "Saturday", shortHand: "Sat")]

    /// Calculates and holds values to be displayed.
    var screenValues: ScreenValues
    /// Calculates and holds values that are chosen through the active province and year.
    var taxManager: TaxManager
    /// Ordered Dictionary containing active wage names and rates.
    var wages: OrderedDictionary<String, Double>
    
    /// Returns a String descibing the current night shift rate or as the hourly rate.
    var nightRateDiscription: String {
        get{
            if !screenValues.isNightShift {
                return "Off"
            }
            if screenValues.nightOT {
                return "Overtime"
            }
            let nightRate = screenValues.custNightShift ?? taxManager.activeProvince.provTaxVal.nightsRate
            if nightRate != nil{
                return String(nightRate!)
            }
            return "Not Set"
        }
    }
    
    
    //MARK: Initialization
    
    override init(){
        self.taxManager = TaxManager(prov: TaxManager.Province.AB)
        let defaultWageKey = taxManager.getDefaultWageKey()
        screenValues = ScreenValues(defaultWageKey: defaultWageKey)
        self.wages = PayCalcData.getWages(taxManager, wageRate: screenValues.custWageRate)
        super.init()
    }
    
    // NSCoding init near bottom.
    
    
    //MARK: Actions
    
    /// Sets a day's hours from a array of strings of either [shift length] or [single, 1.5 time, 2 time].
    func setHours(_ strHours: [String], dayKey: String){
        var floatHours = [Float]()
        for stringVal: String in strHours{
            floatHours.append(Float(stringVal)!)
        }
        if let dayRef = dayRefs[dayKey] {
            dayRef.hours = floatHours
        } else {
            print("PayCalcData.setHours: dayRef not found with key: \(dayKey)")
        }
    }
    
    /// Sets active wage.
    func setSelectedWage(_ wageKey: String){
        screenValues.selectedWageKey = wageKey
    }
    
    /// Refreshes screenValues from existing data.
    func updateCalc(){
        screenValues.updateCalc(self, dayRefs: dayRefs)
    }
    
    // Called by payCalcViewController when settings window closed.
    /// Sets data from PayCalcViews.
    func updateDataFromSettings(_ saveData: PaySettingsViewCont.SettingsData){
        screenValues.activeProv = saveData.prov ?? screenValues.activeProv
        screenValues.activeYear = saveData.year ?? screenValues.activeYear
        screenValues.weekTravRate = saveData.weekTravRate ?? screenValues.weekTravRate
        screenValues.dayTravRate = saveData.dayTravRate ?? screenValues.dayTravRate
        screenValues.mealRate = saveData.mealRate ?? screenValues.mealRate
        screenValues.loaRate = saveData.loaRate ?? screenValues.loaRate
        screenValues.custWageRate = saveData.customWage ?? screenValues.custWageRate
        wages["Custom Wage"] = Double(screenValues.custWageRate)
        
        // Values are nil if off
        screenValues.customVacRate = saveData.vacRate
        screenValues.custMonthDues = saveData.monthlyDues
        screenValues.custFieldDues = saveData.fieldDues
        screenValues.custNightShift = saveData.nightPremium
        
        updateProvYear()
    }
    
    /// Updates wages and tax manager to active province.
    fileprivate func updateProvYear(){
        taxManager.setProvYear(screenValues.activeProv, year: screenValues.activeYear)
        self.wages = PayCalcData.getWages(taxManager, wageRate: screenValues.custWageRate)
        if !wages.containsKey(screenValues.selectedWageKey!) {
            screenValues.selectedWageKey = taxManager.getDefaultWageKey()
        }
    }
    
    
    // MARK: Static functions
    
    /// Returns list of wages from taxManager.
    static func getWages(_ taxManager: TaxManager, wageRate: Float) -> OrderedDictionary<String, Double>{
        var wages = taxManager.getWages()!
        wages["Custom Wage"] = Double(wageRate)
        return wages
    }

    // MARK: NSCoding
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        for (key, dayRef) in dayRefs{
            if let savedHours = aDecoder.decodeObject(forKey: key + SaveKeys.HOURS.rawValue) as? [Float] {
                dayRef.hours = savedHours
            } else {
                print("Error getting saved hours for key: \(key)")
            }
            
            if let savedPreset = aDecoder.decodeObject(forKey: key + SaveKeys.PRESET.rawValue) as? [Float] {
                dayRef.hourPreset = savedPreset
            } else {
                print("Error getting hour preset for key: \(key)")
            }
            
            let holKey = key + SaveKeys.HOLIDAY.rawValue
            // decodeBoolForKey passes false on fail so check containsValueForKey
            if aDecoder.containsValue(forKey: holKey) {
                dayRef.isHoliday = aDecoder.decodeBool(forKey: holKey)
            }
        }
        
        // default ScreenValues are instantiated in self.init() then overwritten if load successful
        if let screenDecoded = aDecoder.decodeObject(forKey: SaveKeys.SCREEN_VALS.rawValue) as? ScreenValues {
            self.screenValues = screenDecoded
//            if screenValues.selectedWageKey == nil || !wages.containsKey(screenValues.selectedWageKey!) {
//                screenValues.selectedWageKey = taxManager.getDefaultWageKey()
//            }
            updateProvYear()
            
        } else {
            print("PayCalcData: Couldn't load screenValues. Set to defaults.")
        }
    }
    
    func encode(with aCoder: NSCoder) {
        
        for (key, dayRef) in dayRefs{
            aCoder.encode(dayRef.hours, forKey: key + SaveKeys.HOURS.rawValue)
            aCoder.encode(dayRef.hourPreset, forKey: key + SaveKeys.PRESET.rawValue)
            // Weekends will have isHoliday false
            let holKey = key + SaveKeys.HOLIDAY.rawValue
            aCoder.encode(dayRef.isHoliday, forKey: holKey)
        }

        aCoder.encode(screenValues, forKey: SaveKeys.SCREEN_VALS.rawValue)
    }
}
