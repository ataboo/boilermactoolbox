//
//  SidePanelViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-15.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

@objc
protocol SidePanelViewControllerDelegate {
    func toolSelected(_ tool: SidePanelTool)
}

class SidePanelViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var logoButton: UIButton!
    var delegate: SidePanelViewControllerDelegate?
    
    var tools: Array<SidePanelTool>!
    
    struct TableView {
        struct CellIdentifiers {
            static let ToolCell = "ToolCell"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.reloadData()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func showAboutAlert(_ sender: UIButton){
        if sender !== logoButton {
            return
        }
        
        let aboutAlert = UIAlertController(title: PayCalcViewController.aboutTitleText, message: PayCalcViewController.aboutBlurbText, preferredStyle: UIAlertControllerStyle.alert)
        aboutAlert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        
        present(aboutAlert, animated: true, completion: nil)
    }
}

extension SidePanelViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableView.CellIdentifiers.ToolCell, for: indexPath) as! ToolCell
        cell.configureForTool(tools[(indexPath as NSIndexPath).row])
        return cell
    }
}


extension SidePanelViewController: UITableViewDelegate {
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedTool = tools[(indexPath as NSIndexPath).row]
        delegate?.toolSelected(selectedTool)
    }
        
}

class ToolCell: UITableViewCell {
    @IBOutlet weak var toolNameLabel: UILabel!
    @IBOutlet weak var toolSubTitle: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    func configureForTool(_ tool: SidePanelTool) {
        toolNameLabel.text = tool.title
        toolSubTitle.text = tool.subTitle
        iconImage.image = tool.icon
    }
}
