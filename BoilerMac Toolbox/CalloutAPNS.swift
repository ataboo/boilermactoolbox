//
//  CalloutAPNS.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SwiftyJSON
import UserNotifications

protocol APNListener {
    func notificationsDenied()
    func saveNotificationKey(deviceToken: Data)
}

protocol APNDelegate {
    func doneAPNCall(success: Bool, error: CalloutClient.CallError?)
}

class CalloutAPNS {
    
    var apnDelegate: APNDelegate?
    
    static let observerKey = "atasoft.boilermactoolbox.apnlaunch"
    static let popupKey = "atasoft.boilermactoolbox.popupnewcall"
    static let calloutRefreshKey = "atasoft.boilermactoolbox.calloutrefresh"
    
    init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.addAPNListener(listener: self)
    }
    
    func addAPNS() {
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert]) { (granted, err) in
            guard err == nil else {
                self.notificationsDenied()
                print("Failed to request notification auth")
                return
            }
            
            if granted {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            } else {
                self.notificationsDenied()
            }
        }
    }
    
    func removeAPNS() {
        if let apnKey = CalloutAPNS.getStoredKey() {
            CalloutClient.instance().removeAPNKey(key: apnKey) { success, error in
                if success {
                    CalloutAPNS.removeStoredKey()
                }
                self.apnDelegate?.doneAPNCall(success: success, error: error)
            }
        } else {
            print("APNS Key already removed.")
            apnDelegate?.doneAPNCall(success: true, error: .config)
        }
    }
    
    
    fileprivate func enrollToolboxApn(apnKey: String, callback: @escaping (Bool, CalloutClient.CallError?)->()) {
        CalloutClient.instance().setAPNKey(key: apnKey, filtersJSON: CalloutDataManager.getFilterTerms(), callback: callback)
    }
    
    func sendTestNotification() {
        if let apnKey = UserDefaults.standard.string(forKey: CalloutConst.DefaultKeys.apnKey),
            CalloutAPNS.notificationSettingsActive() {
            CalloutClient.instance().sendTestAPN(key: apnKey, filtersJSON: CalloutDataManager.getFilterTerms()) { success, error in
                self.apnDelegate?.doneAPNCall(success: success, error: error)
            }
        } else {
            self.apnDelegate?.doneAPNCall(success: false, error: .denied)
        }
    }
    
    static func handleNotificationLaunch(_ launchOptions: [UIApplicationLaunchOptionsKey: Any]) -> Bool {
        if launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] != nil {
            switchContainerToCallouts()
            return true
        } else {
            return false
        }
    }
    
    static func handleNotificationRunning(_ userInfo: [AnyHashable: Any], isActive: Bool) {
        if isActive {
            makeLocalNotification(userInfo)
        } else {
            switchContainerToCallouts()
        }
    }
    
    private static func makeLocalNotification(_ userInfo: [AnyHashable: Any]) {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        NotificationCenter.default.post(name: Notification.Name(rawValue: popupKey), object: self, userInfo: userInfo)
    }
    
    private static func switchContainerToCallouts() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: observerKey), object: self)
    }
    
    public static func notifyCalloutRefresh() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: calloutRefreshKey), object: self)
    }
    
    static func checkKeyToRemove() {
        if let key = getStoredKey() {
            let calloutSettings = CalloutDataManager.getCalloutSettings()
            if notificationSettingsActive() && calloutSettings.notificationsActive {
                return;
            }
            
            CalloutClient.instance().removeAPNKey(key: key) { success, error in
                if success {
                    self.removeStoredKey()
                    CalloutDataManager.commitCalloutSettings(hideNonMatching:
                        calloutSettings.hideNonMatching,
                        hideExcluded: calloutSettings.hideExcluded,
                        notificationsActive: false)
                } else {
                    print("Failed to remove apn key, keeping it in defaults.")
                }
            }
        }
    }
    
    static private func notificationSettingsActive() -> Bool {
        return UIApplication.shared.isRegisteredForRemoteNotifications
    }
    
    static private func getStoredKey() -> String? {
        return UserDefaults.standard.string(forKey: CalloutConst.DefaultKeys.apnKey)
    }
    
    static private func removeStoredKey() {
        print("Removing stored APNS key.")
        UserDefaults.standard.removeObject(forKey: CalloutConst.DefaultKeys.apnKey)
    }
    
    static fileprivate func storeKey(_ apnKey: String) {
        UserDefaults.standard.set(apnKey, forKey: CalloutConst.DefaultKeys.apnKey)
    }
    
}

extension CalloutAPNS: APNListener {
    func notificationsDenied() {
        apnDelegate?.doneAPNCall(success: false, error: .denied)
    }
    
    func saveNotificationKey(deviceToken: Data) {
        let apnKey = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Got Token: \(apnKey)")
        
        CalloutAPNS.storeKey(apnKey)
    
        enrollToolboxApn(apnKey: apnKey) { success, error in
            self.apnDelegate?.doneAPNCall(success: success, error: error)
        }
    }
}
