//
//  Manpower.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2018-02-26.
//  Copyright © 2018 atasoft. All rights reserved.
//

import Foundation
import SwiftyJSON

class Manpower {
    public let id: Int
    public let title: String
    public let detail: String
    
    public init(attr: JSON) {
        self.id = attr["id"].int ?? -1
        self.title = attr["title"].string ?? ""
        self.detail = attr["detail"].string ?? ""
    }
    
    public func valid() -> Bool {
        return self.id >= 0 && self.title != "" && self.detail != ""
    }
    
    public func toString() -> String {
        return "\(self.title) \(self.detail)"
    }
}

