//
//  ContainerViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-15.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

protocol ContainerViewDelegate {
    func toggleSidePanel()
    
    func sidePanelIsOpen() -> Bool
}

class ContainerViewController: UIViewController {
    var centerNavigationController: UINavigationController!
    var activeTool: SidePanelTool = .payCalcTool
    
    let startingToolDefaultKey = "container_starting_tool_view_controller"
    
    var sidePanelOpen: Bool = false {
        didSet {
            showShadowForCenterViewController(sidePanelOpen)
        }
    }
    var leftViewController: SidePanelViewController?
    
    let centerPanelExpandedOffset: CGFloat = 80
    
    var sidePanelButton: UIBarButtonItem!
    
    static let SidePanelCloseIcon = UIImage(named: "toolboxArrowClose")!
    static let SidePanelOpenIcon = UIImage(named: "toolboxArrowOpen")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sidePanelButton = UIBarButtonItem(image: ContainerViewController.SidePanelOpenIcon, style: .plain, target: self, action: #selector(ContainerViewController.toggleSidePanel))
        
        activeTool = loadActiveTool() ?? activeTool
        
        let startingViewController = SidePanelTool.toolViewController(activeTool)!
        
        centerNavigationController = UINavigationController(rootViewController: startingViewController)
        centerNavigationController.isToolbarHidden = false
        
        //let toolbarSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: .UIApplicationWillResignActive, object: nil)
        notificationCenter.addObserver(self, selector: #selector(switchToCallouts), name: Notification.Name(rawValue: CalloutAPNS.observerKey), object: nil)
        notificationCenter.addObserver(self, selector: #selector(showCalloutPopup), name: Notification.Name(rawValue: CalloutAPNS.popupKey), object: nil)

        view.addSubview(centerNavigationController.view)
        addChildViewController(centerNavigationController)
        
        startingViewController.didMove(toParentViewController: self)
        
        startingViewController.toolbarItems = [sidePanelButton]
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    
        /*
        coordinator.animateAlongsideTransition(nil, completion: {(context: UIViewControllerTransitionCoordinatorContext!) in print("Somehow this works.")})
        */

        collapseSidePanels()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func appMovedToBackground() {
        UserDefaults.standard.set(activeTool.ordinal, forKey: startingToolDefaultKey)
    }
    
    @objc func switchToCallouts() {
        toolSelected(.calloutTool)
        CalloutAPNS.notifyCalloutRefresh()
    }
    
    @objc func showCalloutPopup(notification: Notification) {
        var message = "New calls matching your filters are on the callout."
        
        if let userInfo = notification.userInfo, let badge = userInfo["badge"] {
            message = "\(badge) new calls matching your filters are on the callout."
        }
        
        let newCallPopup = UIAlertController(title: "New Matching Calls", message: message, preferredStyle: .alert)
        
        newCallPopup.addAction(UIAlertAction(title: "Show Callout", style: .default) { action in
            self.switchToCallouts()
        })
        newCallPopup.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        
        self.present(newCallPopup, animated: true)
    }
    
    private func loadActiveTool() -> SidePanelTool? {
        return SidePanelTool.panelToolList()[UserDefaults.standard.integer(forKey: startingToolDefaultKey)]
    }
}

extension ContainerViewController: ContainerViewDelegate {
    
    @objc func toggleSidePanel() {
        if !sidePanelOpen {
            addLeftPanelViewController()
            
            sidePanelButton.image! = ContainerViewController.SidePanelCloseIcon
        } else {
            sidePanelButton.image! = ContainerViewController.SidePanelOpenIcon
        }
        
        animateLeftPanel(shouldExpand: !sidePanelOpen)
    }
    
    func sidePanelIsOpen() -> Bool {
        return sidePanelOpen
    }
    
    func collapseSidePanels() {
        if sidePanelOpen {
            toggleSidePanel()
        }
    }
    
    func addLeftPanelViewController(){
        if leftViewController == nil {
            leftViewController = UIStoryboard.leftViewController
            leftViewController!.tools = SidePanelTool.panelToolList()
            
            addChildSidePanel(leftViewController!)
        }
    }
    
    func addChildSidePanel(_ sidePanelController: SidePanelViewController){
        sidePanelController.delegate = self
        
        view.insertSubview(sidePanelController.view, at: 0)
        
        addChildViewController(sidePanelController)
        sidePanelController.didMove(toParentViewController: self)
    }
    
    func animateLeftPanel(shouldExpand: Bool) {
        if shouldExpand {
            sidePanelOpen = true
            
            animateCenterPanelXPosition(targetPosition: centerNavigationController.view.frame.width - centerPanelExpandedOffset)
        } else {
            animateCenterPanelXPosition(targetPosition: 0) {
                finished in self.sidePanelOpen = false
                
                if self.leftViewController != nil {
                    self.leftViewController!.view.removeFromSuperview()
                    self.leftViewController = nil;
                }
            }
        }
    }
    
    func animateCenterPanelXPosition(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,
                       options: [.curveEaseIn, .curveEaseOut],
            animations: {self.centerNavigationController.view.frame.origin.x = targetPosition}, completion: completion)
    }

    func showShadowForCenterViewController(_ shouldShowShadow: Bool) {
        if(shouldShowShadow) {
            centerNavigationController.view.layer.shadowOpacity = 0.8
        } else {
            centerNavigationController.view.layer.shadowOpacity = 0.0
        }
    }
}


extension ContainerViewController: SidePanelViewControllerDelegate {
    func toolSelected(_ tool: SidePanelTool) {
        let newVC = SidePanelTool.toolViewController(tool)!
        centerNavigationController.viewControllers = [newVC]
        newVC.toolbarItems = [sidePanelButton]
        collapseSidePanels()
        
        activeTool = tool
    }
}


private extension UIStoryboard {
    class var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    class var leftViewController: SidePanelViewController? {
        return mainStoryboard.instantiateViewController(withIdentifier: "LeftViewController") as? SidePanelViewController
    }
}
