//
//  DayPickTableViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-10.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class DayPickTableViewController: UITableViewController {
    // MARK: Properties

    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var hoursTextField: UITextField!
    @IBOutlet weak var hoursCell: UITableViewCell!
    @IBOutlet weak var holidayCell: UITableViewCell!
    @IBOutlet weak var holidaySwitch: UISwitch!

    @IBOutlet weak var fourTensCell: UITableViewCell!
    @IBOutlet weak var nightRateCell: UITableViewCell!
    @IBOutlet weak var weekendCell: UITableViewCell!
    
    private var customHoursOn = false
    private var weekendLabel = "No"
    private var nightShiftRate = "0"
    private var fourTensLabel = "No"
    private var standardHourPicker: UIPickerView!
    fileprivate var customHourPicker: UIPickerView!
    private let dayToolbar = UIToolbar()
    
    
    // Set by the showing view controller
    var payCalcData: PayCalcData!
    var dayRef: DayReference!
    
    // Read by the showing view controller after
    var selectedHours: [String]?
    var holidayOn: Bool?
    
    static var customHourStrings: [String] {
        get{
            var hourArr = [String]()
            for i in 0...48{
                hourArr.append("\(Float(i) / Float(2))")
            }
            return hourArr
        }
    }
    static let standardHoursStrings = ["0.0", "8.0", "10.0", "12.0", "13.0"]
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setLabels()
        setupPickers()
        
        
    }
    
    private func setLabels(){
        // label values set in initData
        fourTensCell.textLabel!.text = "Four Tens"
        fourTensCell.detailTextLabel!.text = fourTensLabel
        nightRateCell.textLabel!.text = "Nightshift Rate"
        nightRateCell.detailTextLabel!.text = nightShiftRate
        weekendCell.textLabel!.text = "Weekend"
        weekendCell.detailTextLabel!.text = weekendLabel
        self.navigationItem.title = dayRef!.name + " Hours"
    }
    
    private func setupPickers(){
        dayToolbar.barStyle = UIBarStyle.default
        dayToolbar.isTranslucent = true
        dayToolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DayPickTableViewController.donePicker))
        let barSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let customButton = UIBarButtonItem(title: "Custom", style: UIBarButtonItemStyle.plain, target: self, action: #selector(DayPickTableViewController.toggleCustomHours))
        dayToolbar.setItems([doneButton, barSpace, customButton], animated: false)
        dayToolbar.isUserInteractionEnabled = true
        
        hoursTextField.inputAccessoryView = dayToolbar
        
        self.standardHourPicker = createCommonPicker()
        self.customHourPicker = createCommonPicker()
        
        updateCustomHourPicker()
        
        holidaySwitch.setOn(dayRef!.isHoliday, animated: false)
        holidaySwitch.isUserInteractionEnabled = !dayRef!.isWeekend
        holidayCell.isUserInteractionEnabled = !dayRef!.isWeekend
        holidayCell.backgroundColor = !dayRef!.isWeekend ? nil: UIColor(white: 0.95, alpha: 1)
        holidaySwitch.isEnabled = !dayRef!.isWeekend
        
        var hoursString = [String]()
        for val: Float in dayRef!.hours{
            hoursString.append(String(val))
        }
        self.selectedHours = hoursString
        if dayRef!.hours.count == 1{
            hoursTextField.text = standardDaySplit(selectedHours![0])
            if let matchIndex = DayPickTableViewController.standardHoursStrings.index(of: hoursString[0]){
                standardHourPicker.selectRow(matchIndex, inComponent: 0, animated: false)
            }
        } else {
            hoursTextField.text = hoursString.joined(separator: ", ")
            
            for (index, str) in hoursString.enumerated(){
                if let matchIndex = DayPickTableViewController.customHourStrings.index(of: str){
                    customHourPicker.selectRow(matchIndex, inComponent: index, animated: false)
                }
            }
            customHoursOn = true
            updateCustomHourPicker()
        }
        
        hoursTextField.becomeFirstResponder()
    }
    
    private func createCommonPicker() -> UIPickerView{
        let picker = UIPickerView()
        picker.showsSelectionIndicator = true
        picker.dataSource = self
        picker.delegate = self

        return picker
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TableView functions
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath {
        case tableView.indexPath(for: hoursCell)!:
            hoursTextField.becomeFirstResponder()
        case tableView.indexPath(for: holidayCell)!:
            if holidaySwitch.isEnabled{
                holidaySwitch.setOn(!holidaySwitch.isOn, animated: true)
                toggleHoliday(holidaySwitch)
            }
        default: break
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: Actions
    
    @IBAction func toggleHoliday(_ sender: UISwitch){
        dayRef.isHoliday = sender.isOn
        donePicker()
    }
    
    @objc func toggleCustomHours(){
        customHoursOn = !customHoursOn
        updateCustomHourPicker()
        
        hoursTextField.resignFirstResponder()
        hoursTextField.becomeFirstResponder()
    }
    
    @objc func donePicker(){
        self.selectedHours = [String]()
        if customHoursOn {
            for compIndex: Int in 0 ..< customHourPicker.numberOfComponents {
                selectedHours!.append(DayPickTableViewController.customHourStrings[customHourPicker.selectedRow(inComponent: compIndex)])
            }
            hoursTextField.text = selectedHours!.joined(separator: ", ")
        } else {
            selectedHours = [DayPickTableViewController.standardHoursStrings[standardHourPicker.selectedRow(inComponent: 0)]]
            hoursTextField.text = standardDaySplit(selectedHours![0])
        }
        
        hoursTextField.resignFirstResponder()
    }
    
    func initData(_ payCalcData: PayCalcData, dayRef: DayReference){
        self.payCalcData = payCalcData
        self.dayRef = dayRef
        
        self.nightShiftRate = payCalcData.nightRateDiscription
        self.fourTensLabel = payCalcData.screenValues.isFourTens ? "Yes" : "No" 
        self.weekendLabel = dayRef.isWeekend ? "Yes" : "No"
    }

    private func updateCustomHourPicker() {
        hoursTextField.inputView = customHoursOn ? customHourPicker: standardHourPicker
        dayToolbar.items![2].title! = customHoursOn ? "Standard": "Custom"
    }
    
    private func standardDaySplit(_ selectedString: String) -> String {
        let pickerDayRef = DayReference(resID: dayRef!.resID, name: dayRef!.name, shortHand: dayRef!.shortHand)
        pickerDayRef.hours = [Float(selectedString)!]
        pickerDayRef.isHoliday = holidayOn ?? dayRef.isHoliday
        
        let hoursSplit = payCalcData!.taxManager.splitShiftHours(dayRef: pickerDayRef, fourTens: payCalcData!.screenValues.isFourTens)
        let joined = hoursSplit.map({return String($0)}).joined(separator: ", ")
        
        return joined
    }
    

    // MARK: Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        donePicker()
        
        if sender as? UIBarButtonItem == cancelButton {
            self.selectedHours = nil
            self.holidayOn = nil
        }

    }

}

// MARK: UIPickerViewDataSource
extension DayPickTableViewController: UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == customHourPicker {
            return 3
        }
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == customHourPicker {
            return DayPickTableViewController.customHourStrings.count
        } else {
            return DayPickTableViewController.standardHoursStrings.count
        }
    }
}

// MARK: UIPickerViewDelegate
extension DayPickTableViewController: UIPickerViewDelegate{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == customHourPicker {
            return DayPickTableViewController.customHourStrings[row]
        } else {
            return DayPickTableViewController.standardHoursStrings[row]
        }
    }
}
