//
//  SpriteUtils.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class SpriteUtils{
    
    /**
     Gets a series of textures from an atlas in numbered order (ex. myTexture_0, myTexture_1...)
     
     - parameter atlas: Sprite Kit Texture Atlas containing the textures.
     - parameter prefix: String that will match the prefix of the image series.
     
     - returns: array of Sprite Kit Textures in ascending order.
    */
    open static func texturesFromAtlas(_ atlas: SKTextureAtlas, prefix: String) -> Array<SKTexture>{
        var retTextures = Array<SKTexture>()
        
        let sortedNames = atlas.textureNames.sorted(by: { (s1, s2) in
            return s1.localizedStandardCompare(s2) == ComparisonResult.orderedAscending
        })
        
        for textureName: String in sortedNames {
            if textureName.contains(prefix) {
                //print("Adding \(textureName) from atlas...")
                retTextures.append(atlas.textureNamed(textureName))
            }
        }
        
        return retTextures
    }
}

extension CGPoint{
    public func sub(_ point: CGPoint) -> CGPoint{
        return CGPoint(x: x - point.x, y: y - point.y)
    }
    
    public func sub(_ x: CGFloat, y: CGFloat) -> CGPoint {
        return CGPoint(x: self.x - x, y: self.y - y)
    }
    
    public func sub(_ x: Float, y: Float) -> CGPoint {
        return self.sub(CGFloat(x), y: CGFloat(y))
    }
    
    public func add(_ point: CGPoint) -> CGPoint{
        return CGPoint(x: x + point.x, y: y + point.y)
    }
    
    public func add(_ x: CGFloat, y: CGFloat) -> CGPoint {
        return CGPoint(x: self.x + x, y: self.y + y)
    }
    
    public func add(_ x: Float, y: Float) -> CGPoint {
        return self.add(CGFloat(x), y: CGFloat(y))
    }
    
    public func scl(_ factor: Float) -> CGPoint {
        return CGPoint(x: x * CGFloat(factor), y: y * CGFloat(factor))
    }
    
}
