//
//  CounterViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-26.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

class CounterViewController: UIViewController {
    
    @IBOutlet weak var spriteView: SKView!
    
    fileprivate var cashCounterData: CashCounterData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cashCounterData = CounterViewController.loadCashData()
        
        let scene = CounterGameScene(size: spriteView.bounds.size, cashData: cashCounterData)
        //spriteView.showsFPS = true
        //spriteView.showsNodeCount = true
        spriteView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        spriteView.presentScene(scene)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        PauseFlags.cashCounter = .paused
        
        saveCashData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(PauseFlags.cashCounter == .paused){
            PauseFlags.cashCounter = .unPaused
        }
    }
    
    @IBAction func rewindCashSettings(_ sender: UIStoryboardSegue){
        if let cashSettings = sender.source as? CashSettingsTVC {
            cashCounterData.setShiftStartDate(cashSettings.shiftStartDate!)
            cashCounterData.lockScene = cashSettings.lockScene
            saveCashData()
        }
    }
    
    @IBAction override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navController = segue.destination as? UINavigationController,
            let cashSettings = navController.viewControllers.first as? CashSettingsTVC {
            cashSettings.shiftStartDate = cashCounterData.shiftStartDate()
            cashSettings.lockScene = cashCounterData.lockScene
        }
    }
    
    // MARK: NSCoding
    
    func saveCashData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(cashCounterData, toFile: CashCounterData.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save CashData.")
        }
    }
    
    static func loadCashData() -> CashCounterData {
        if let loadedCashData = NSKeyedUnarchiver.unarchiveObject(withFile: CashCounterData.ArchiveURL.path) as? CashCounterData {
            return loadedCashData
        }
        print("CounterViewController Failed to load CashData... Instantiating new one.")
        return CashCounterData()
    }
    
    fileprivate func resetCashData() {
        print("Resetting cashData...");
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(atPath: CashCounterData.ArchiveURL.path)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        self.cashCounterData = CashCounterData();
    }
}
