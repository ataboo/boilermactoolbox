//
//  CashCounterData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-03-26.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit


class CashCounterData: NSObject, NSCoding {
    enum EarningType: String {
        case StraightTime = "Straight Time"
        case Overtime = "Overtime"
        case WeekDouble = "Double Time"
        case WeekendDouble = "Weekend Double"
        case Holiday = "Holiday"
        case OffShift = "Off Shift"
    }
    
    enum CounterSceneType: Int {
        case oilDrip = 0
        case pulpMill = 1
        case hydroDam = 2
        case nukePlant = 3
        
        static let allVals = [oilDrip, pulpMill, hydroDam, nukePlant]
        
        func getScene(_ parentScene: SKScene) -> CounterScene {
            switch self {
            case .oilDrip:
                return OilDropScene(parentScene: parentScene)
            case .pulpMill:
                return PulpMillScene(parentScene: parentScene)
            case .hydroDam:
                return HydroDamScene(parentScene: parentScene)
            case .nukePlant:
                return NukePlantScene(parentScene: parentScene)
            }
        }
        
        func nextScene(_ parentScene: SKScene) -> CounterSceneType {
            let nextSceneIndex = self.rawValue < CounterSceneType.allVals.count - 1 ? self.rawValue + 1 : 0
            
            return CounterSceneType.allVals[nextSceneIndex]
        }
    }
    
    enum SaveKeys: String {
        case ShiftStartHour = "cash_count_shiftstart_hour"
        case ShiftStartMin = "cash_count_shiftstart_min"
        case SceneTypeIndex = "cash_count_scene_type"
        case LockScene = "cash_count_lock_scene"

    }
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("flangedata")
    
    var shiftStartHr = 7
    var shiftStartMin = 0
    var activeSceneType = CounterSceneType.pulpMill
    var lockScene = false
    
    private var days: Array<DayReference>?
    private var nightPremium = Float(0)
    private var nightOT = false
    private var wageRate = 44.81
    private var isFourTens = false
    
    private var payCalcData: PayCalcData!
    
    override init(){
        super.init()
        
        payCalcData = loadPayCalcData()
        getDataFromPayCalc()
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        
        if !aDecoder.containsValue(forKey: SaveKeys.SceneTypeIndex.rawValue){
            print("CashCounterData: no saved data...")
            return
        }
        
        let sceneIndex = aDecoder.decodeInteger(forKey: SaveKeys.SceneTypeIndex.rawValue)
        if let sceneType = CounterSceneType(rawValue: sceneIndex){
            activeSceneType = sceneType
        } else {
            print("CashCounterData: SceneType had bad index val saved.")
        }
        
        shiftStartHr = aDecoder.decodeInteger(forKey: SaveKeys.ShiftStartHour.rawValue)
        shiftStartMin = aDecoder.decodeInteger(forKey: SaveKeys.ShiftStartMin.rawValue)
        lockScene = aDecoder.decodeBool(forKey: SaveKeys.LockScene.rawValue)
    }
    
    func nextSceneType(_ parent: SKScene) -> CounterScene {
        let newScene = activeSceneType.nextScene(parent)

        activeSceneType = newScene
        
        return newScene.getScene(parent)
        }
    
    func shiftStartDate() -> Date{
        let unitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute]
        
        var components = (Calendar.current as NSCalendar).components(unitFlags, from: Date())
        components.hour = shiftStartHr
        components.minute = shiftStartMin
        components.second = 0
        
        return Calendar.current.date(from: components)!
    }
    
    func setShiftStartDate(_ shiftStart: Date) {
        let unitFlags: NSCalendar.Unit = [.hour, .minute]
        
        let components = (Calendar.current as NSCalendar).components(unitFlags, from: shiftStart)
        
        shiftStartHr = components.hour!
        shiftStartMin = components.minute!
        
        print("set hr to: \(shiftStartHr), set min to: \(shiftStartMin)")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(activeSceneType.rawValue, forKey: SaveKeys.SceneTypeIndex.rawValue)
        aCoder.encode(shiftStartHr, forKey: SaveKeys.ShiftStartHour.rawValue)
        aCoder.encode(shiftStartMin, forKey: SaveKeys.ShiftStartMin.rawValue)
        aCoder.encode(lockScene, forKey: SaveKeys.LockScene.rawValue)
    }
    
    
    func refreshPayCalcData(){
        getDataFromPayCalc()
    }
    
    func shiftLength(_ day: Int) -> Float{
        let dayRef: DayReference = getDay(day)
        if dayRef.hours.count == 1{
            return dayRef.hours[0]
        }
        
        if dayRef.hours.count == 3 {
            return dayRef.hours[0] + dayRef.hours[1] + dayRef.hours[2]
        }
        
        return 0.0
    }
    
    func getEarnings(_ secondsIntoShift: Double, weekday: Int) -> (earnings: Float, earningType: EarningType){
        let hoursIntoShift = Float(secondsIntoShift / 3600)
        let dayRef = getDay(weekday)
        
        let shiftHours = getSplitHours(dayRef)

        let doubleHours = AtaUtils.clamp(hoursIntoShift - Float(shiftHours[0]) - Float(shiftHours[1]), floor: 0, ceiling: shiftHours[2])
        let otHours = AtaUtils.clamp(hoursIntoShift - Float(shiftHours[0]) - shiftHours[2], floor: 0, ceiling: shiftHours[1])
        let straightHours = AtaUtils.clamp(hoursIntoShift, floor: 0, ceiling: shiftHours[0])
        
        let wageEarnings = roundCent((straightHours + otHours * 1.5 + doubleHours * 2.0) * Float(wageRate))
        
        var earningsType: EarningType = otHours > 0 ? .Overtime : straightHours > 0 ? .StraightTime : .OffShift
        
        if doubleHours > 0 {
            earningsType = dayRef.isHoliday ? .Holiday : dayRef.isWeekend ? .WeekendDouble : .WeekDouble
        }
        
        if hoursIntoShift >= shiftHours[0] + shiftHours[1] + shiftHours[2]{
            earningsType = .OffShift
        }
        
        return (wageEarnings, earningsType)
    }
    
    private func getSplitHours(_ dayRef: DayReference) -> [Float]{
        if dayRef.hours.count == 1 {
            return payCalcData.taxManager.splitShiftHours(dayRef: dayRef, fourTens: isFourTens)
        } else {
            return dayRef.hours
        }
    }
 
    
    private func roundCent(_ value: Float) -> Float {
        return round(value * 100) / 100
    }
    
    private func getDataFromPayCalc() {
        var days = Array<DayReference>()
        for key: String in PayCalcData.dayKeys{
            days.append((payCalcData.dayRefs[key])!)
        }
        
        if let wageKey = payCalcData.screenValues.selectedWageKey, let payCalcWage = payCalcData.wages[wageKey] {
            wageRate = payCalcWage
        }
        let nightOT = payCalcData.screenValues.isNightShift ? payCalcData.screenValues.nightOT : false
        let nightsRate = !nightOT && payCalcData.screenValues.isNightShift ? payCalcData.screenValues.nightsRate : 0.0
        
        self.days = days
        nightPremium = nightsRate
        self.nightOT = nightOT
        isFourTens = payCalcData.screenValues.isFourTens
    }
    
    private func loadPayCalcData() -> PayCalcData? {
        if let loadedPayCalc = NSKeyedUnarchiver.unarchiveObject(withFile: PayCalcData.ArchiveURL.path) as? PayCalcData {
            return loadedPayCalc
        }
        print("CashCounterData failed to load payCalcData.")
        return nil
    }
    
    func saveData(){
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(self, toFile: CashCounterData.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save CashData.")
        }
    }
    
    private func getDay(_ dayInt: Int) -> DayReference{
        if days == nil || days!.count <= dayInt {
            let defaultDay = DayReference(resID: "defaultDay", name: "defaultDay", shortHand: "defDay")
            defaultDay.hours = [8, 2, 2]
            return defaultDay
        }
        
        return days![dayInt]
    }
}
