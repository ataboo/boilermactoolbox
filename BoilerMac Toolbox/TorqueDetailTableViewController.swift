//
//  TorqueDetailTableViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-26.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

class TorqueDetailTableViewController: UITableViewController {

    // MARK: Properties
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    var torqueData: TorqueData!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //if torqueData == nil {
        //    fatalError("TorqueDetailTableViewController should have torqueData set before load.")
        //}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return torqueData.patternCount
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "torqueProto", for: indexPath)

        let selectedPattern = torqueData.patternAt((indexPath as NSIndexPath).row)
        cell.textLabel!.text = selectedPattern.title
        cell.imageView!.image = selectedPattern.image

        return cell
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let senderCell = sender as? UITableViewCell{
            let selectedPattern = torqueData.patternAt((tableView.indexPath(for: senderCell)! as NSIndexPath).row)
            torqueData.selectedPatternSet(selectedPattern)
        }
        
        if let senderButton = sender as? UIBarButtonItem , senderButton != cancelButton {
            return
        }
    }
}
