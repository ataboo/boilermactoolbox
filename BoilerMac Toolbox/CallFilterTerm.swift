//
//  CallFilterTerm.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-10.
//  Copyright © 2016 atasoft. All rights reserved.
//

import Foundation
import SwiftyJSON

public class CallFilterTerm: NSObject, NSCoding {
    
    public enum MatchLevel: Int {
        case none = 0
        case someInclusive = 1
        case someExclusive = -1
        case all = 42
        
        static func fromInt(_ intVal: Int) -> MatchLevel {
            return intVal > 0 ? someInclusive : intVal < 0 ? someExclusive : none
        }
    }

    
    public enum TermType: String {
        case jobId = "job_id"
        case jobName = "job_name"
        case contractor = "contractor"
        case dateTime = "date_time"
        case openTo = "open_to"
        case dayShift = "is_dayshift"
        case workType = "work_type"
        case hours = "hours"
        case duration = "duration"
        case accomodation = "accomodation"
        case drugTesting = "drug_testing"
        case comments = "comments"
        case nameHire = "name_hires"
        case manpower = "manpower"
        
        static var values: [TermType] = [
            jobName,
            contractor,
            dateTime,
            openTo,
            dayShift,
            workType,
            hours,
            duration,
            accomodation,
            drugTesting,
            comments,
            jobId,
            manpower,
            nameHire
        ]
        
        var string: String {
            get {
                switch self {
                case .jobId:
                    return "Job Id#"
                case .jobName:
                    return "Job Name"
                case .contractor:
                    return "Contractor"
                case .dateTime:
                    return "Date and Time"
                case .openTo:
                    return "Open To"
                case .dayShift:
                    return "Shift"
                case .workType:
                    return "Work Type"
                case .hours:
                    return "Hours"
                case .duration:
                    return "Duration"
                case .accomodation:
                    return "Accomodation"
                case .drugTesting:
                    return "Drug Testing"
                case .comments:
                    return "Comments"
                case .nameHire:
                    return "Name Hires"
                case .manpower:
                    return "Manpower"
                }
            }
        }
    
        var suggestions: [(String, values: String)] {
            get {
                switch self {
                case .dayShift:
                    return [("Day Shift", values: "DAYSHIFT"), ("Night Shift", values: "NIGHTSHIFT")]
                case .openTo:
                    return [("Members", values: "MEMBERS"), ("Travel Cards", values: "TRAVEL CARDS")]
                case .manpower:
                    return [("Welder", values: CalloutConst.KeyWords.welder.joined(separator: " ")),
                            ("B Welder", values: CalloutConst.KeyWords.bWelder.joined(separator: " ")),
                            ("Fitter", values: CalloutConst.KeyWords.fitter.joined(separator: " ")),
                            ("Rigger", values: CalloutConst.KeyWords.rigger.joined(separator: " ")),
                            ("Apprentice", values: CalloutConst.KeyWords.allApprentice.joined(separator: " "))]
                default:
                    return []
                }
            }
        }
    }
    
    let termType: TermType
    var keyWord: String
    var inclusive: Bool
    
    var json: JSON {
        get {
            return JSON(["term_type": self.termType.rawValue, "key_words": self.keyWord, "inclusive": self.inclusive])
        }
    }
    
    init(termType: TermType, keyWord: String, inclusive: Bool) {
        self.termType = termType
        self.keyWord = keyWord
        self.inclusive = inclusive
    }
    
    public required init?(coder aDecoder: NSCoder) {
        self.termType = TermType(rawValue:
            aDecoder.decodeObject(forKey: CalloutConst.DefaultKeys.termType) as? String ?? TermType.jobId.rawValue)!
        self.keyWord = aDecoder.decodeObject(forKey: CalloutConst.DefaultKeys.keyWords) as? String ?? ""
        self.inclusive = aDecoder.decodeBool(forKey: CalloutConst.DefaultKeys.inclusive)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(termType.rawValue, forKey: CalloutConst.DefaultKeys.termType)
        aCoder.encode(keyWord, forKey: CalloutConst.DefaultKeys.keyWords)
        aCoder.encode(inclusive, forKey: CalloutConst.DefaultKeys.inclusive)
    }
    
    // 0 - no match, 1 - inclusive match, -1, exclusive match
    static func getCallMatches(_ callObject: CalloutObject, filters: [CallFilterTerm]) -> (matchLevel: MatchLevel, matchMap:[TermType: [MatchLevel]]) {
        var matchMap = [TermType: [MatchLevel]]()
        var matchCounts = [0, 0]
        
        for filter in filters {
            var matchList = matchMap[filter.termType] ?? [.none]

            for (index, callVal) in getValsForTerm(filter.termType, callObject: callObject).enumerated() {
                if matchList.count <= index {
                    matchList.append(.none)
                }
                
                let keyWords = filter.keyWord.components(separatedBy: " ")
                if callVal.uppercased().containsAnyOfTheseWords(words: keyWords) {
                    if filter.inclusive {
                        matchCounts[0] += 1
                        matchList[index] = .someInclusive
                    } else {
                        matchCounts[1] += 1
                        matchList[index] = .someExclusive
                    }
                }
            }
            matchMap[filter.termType] = matchList
        }
        let matchSum = matchCounts[0] + matchCounts[1]
        let matchLevel: MatchLevel = matchCounts[1] > 0 ? .someExclusive :
            matchSum == 0 ? .none :
            matchSum == filters.count ? .all : .someInclusive
        
        return (matchLevel: matchLevel, matchMap: matchMap)
    }
    
    static func getValsForTerm(_ termType: TermType, callObject: CalloutObject) -> [String] {
        switch termType {
        case .jobName:
            return [callObject.jobName]
        case .jobId:
            return ["\(callObject.jobId)"]
        case .contractor:
            return [callObject.contractor]
        case .dayShift:
            return [callObject.shiftString]
        case .dateTime:
            return ["\(callObject.dateTime)"]
        case .duration:
            return [callObject.duration]
        case .hours:
            return [callObject.hours]
        case .openTo:
            return [callObject.openTo]
        case .workType:
            return [callObject.workType]
        case .drugTesting:
            return [callObject.drugTesting]
        case .comments:
            return [callObject.comments]
        case .accomodation:
            return [callObject.accommodation]
        case .manpower:
            return callObject.manpower
        case .nameHire:
            return callObject.nameHires
        }
    }
}
