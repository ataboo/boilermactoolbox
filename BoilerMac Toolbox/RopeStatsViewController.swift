//
//  RopeStatsViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

protocol DataStore {
    func decodeData() -> NSObject
    func encodeData(_ dataObject: NSObject)
    func resetData()
}

class RopeStatsViewController: UITableViewController {

    @IBOutlet weak var ropeTypeCell: UITableViewCell!
    @IBOutlet weak var ropeDiameterCell: UITableViewCell!
    @IBOutlet weak var safetyFactorCell: UITableViewCell!
    @IBOutlet weak var breakingStrengthCell: UITableViewCell!
    @IBOutlet weak var wLLCell: UITableViewCell!
    @IBOutlet weak var clipSpacingCell: UITableViewCell!
    @IBOutlet weak var clipCountCell: UITableViewCell!
    
    var ropeDataStore: RopeDataStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ropeDataStore = decodeData() as! RopeDataStore
        
        updateOutput()
    }
    
    // MARK: - Navigation

    @IBAction func rewindFromRopeDetail(_ sender: UIStoryboardSegue){
        if let ropeDetailTVC = sender.source as? RopeDetailViewController,
            let selectedIndex = ropeDetailTVC.selectedIndex {
            switch ropeDetailTVC.selectionType! {
            case .RopeType:
                ropeDataStore.activeRopeTypeIndex = selectedIndex
            case .RopeDiameter:
                ropeDataStore.activeDiameterVal =
                    ropeDataStore.standardDiameters()[selectedIndex].floatVal
            case .SafetyFactor:
                ropeDataStore.activeSafetyFactorIndex = selectedIndex
            }
        }
        
        encodeData(ropeDataStore)
        
        updateOutput()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailTVC = segue.destination as? RopeDetailViewController {
            switch sender as! UITableViewCell {
            case ropeDiameterCell:
                detailTVC.selectItems = ropeDataStore.standardDiameterStrings()
                detailTVC.selectionType = RopeDetailViewController.SelectionType.RopeDiameter
            case ropeTypeCell:
                detailTVC.selectItems = ropeDataStore.ropeTypeNames()
                detailTVC.selectionType = RopeDetailViewController.SelectionType.RopeType
            case safetyFactorCell:
                detailTVC.selectItems = ropeDataStore.safetyFactorNames()
                detailTVC.selectionType = RopeDetailViewController.SelectionType.SafetyFactor
            default:
                fatalError("RopeStatsViewController: unknown sender for segue to detail.")
            }
        }
    }
    
    func updateOutput(){
        let ropeType = ropeDataStore.valAtIndex(ropeDataStore.activeRopeTypeIndex)
        let diameter = RopeStats.RopeDiameter(floatVal: ropeDataStore.activeDiameterVal)
        let safetyFactor = ropeType.safetyFactorList[ropeDataStore.activeSafetyFactorIndex]
        let breakingStrength = diameter.breakStrength(ropeType)
        let workingLoadLimit = diameter.workingLoadLimit(ropeType, safetyFactor: safetyFactor)
        
        ropeTypeCell.detailTextLabel?.text = ropeType.name
        ropeDiameterCell.detailTextLabel?.text = diameter.stringOutput
        safetyFactorCell.detailTextLabel?.text = safetyFactor.name
        breakingStrengthCell.detailTextLabel?.text = String(format: "%.2f tons", breakingStrength)
        wLLCell.detailTextLabel?.text = String(format: "%.2f tons", workingLoadLimit)
        clipCountCell.detailTextLabel?.text = "\(diameter.clipCount)"
        clipSpacingCell.detailTextLabel?.text = String(format: "%.2f\"", diameter.clipSpacing)
        
    }
}

extension RopeStatsViewController: DataStore {
    func encodeData(_ dataObject: NSObject) {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(dataObject, toFile: RopeDataStore.ArchiveURL.path)
        
        if(!isSuccessfulSave){
            print("Failed to save data...")
        }
    }
    
    func decodeData() -> NSObject {
        if let loadedData = NSKeyedUnarchiver.unarchiveObject(withFile: RopeDataStore.ArchiveURL.path) as? RopeDataStore {
            return loadedData
        }
        print("RopeStatsViewController Failed to load RopeDataStore... Instantiating new one.")
        return RopeDataStore()
    }
    
    func resetData() {
        print("Resetting RopeData...");
        let fileMgr = FileManager.default
        do{
            try fileMgr.removeItem(atPath: RopeDataStore.ArchiveURL.path)
        } catch let e as NSError {
            print("File Remove Error: \(e.localizedDescription)")
        }
        self.ropeDataStore = RopeDataStore()
    }

}
