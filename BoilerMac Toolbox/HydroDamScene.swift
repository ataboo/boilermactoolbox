//
//  PulpMillScene.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class HydroDamScene: CounterScene {
    fileprivate static let atlasName: String = "HydroDam"
    fileprivate static let salmonAtlasName: String = "HydroSalmon"
    fileprivate static let backgroundName: String = "HydroBackground"
    
    
    fileprivate var powerTextures: Array<SKTexture>
    fileprivate var salmonTextures: Array<SKTexture>
    
    public required init(parentScene: SKScene){
        
        powerTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: HydroDamScene.atlasName), prefix: "HydroPower_")
        
        salmonTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: HydroDamScene.salmonAtlasName), prefix: "HydroSalmon_")
        
        super.init(parentScene: parentScene, atlasName: HydroDamScene.atlasName, backgroundName: HydroDamScene.backgroundName)
        
        earningsCounter.posOffset = CGPoint(x: 0, y: 0.33)
        
        self.aspectRatio = 1024/720
        
        let waterLoop = LoopAnimation(atlas: SKTextureAtlas(named: HydroDamScene.atlasName), prefix: "HydroWater_", frameLength: 80, sprite: centeredSprite("HydroWater_03"))
        addSpriteObject(waterLoop)
    }
    
    public required convenience init(parentScene: SKScene, atlasName: String, backgroundName: String) {
        self.init(parentScene: parentScene)
    }
    
    override open func nextFineAnim(_ startTime: Int, totalValue: Float, description: String) {
        let difference = Int((totalValue - earningsCounter.earnings) * 100)
        
        let powerAnim = OnceAnimation(textures: powerTextures, frameLength: 40, sprite: centeredSprite("HydroPower_00"))
        powerAnim.startAnimation(startTime - 500)
        
        addSpriteObject(powerAnim)
        
        makeFineLabel(startTime, contents: "\(difference)¢")
        
        earningsCounter.addEarningAnimation(startTime, newValue: totalValue, earningDescription: description)
    }
    
    fileprivate func makeFineLabel(_ startTime: Int, contents: String) {
        let fineLabel = OnceLabel(animProps: LabelAnimProps.sceneLabel(centerPos, screenSize: sceneSize, sceneType: .hydroDam), startTime: startTime, parentScene: parentScene)
        fineLabel.label.zPosition = -1
        fineLabel.label.text = contents
        
        spriteObjects.append(fineLabel)
    }
    
    override open func nextCoarseAnim(_ startTime: Int) {
        let salmonAnim = OnceAnimation(textures: salmonTextures, frameLength: 60, sprite: centeredSprite("HydroSalmon_00"))
        
        salmonAnim.sprite.zPosition = 5
        
        salmonAnim.startAnimation(startTime - 500)
        
        addSpriteObject(salmonAnim)
    }
}
