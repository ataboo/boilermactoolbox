//
//  HourPickViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-14.
//  Copyright © 2015 atasoft. All rights reserved.
//


import UIKit

class PickerViewController: UIViewController, UIPickerViewDelegate {
    
    // MARK: Properties
    /// Toolbar above picker with done button
    var pickerToolbar = UIToolbar()
    /// for choosing wage rate by name ex: Journeyperson, etc.
    var wagePicker: UIPickerView!
    /// for choosing number of days to apply bonus ex: LOA or Meal bonuses
    var bonusPicker: UIPickerView!
    /// Dummy textfield to display picker
    weak var pickerTextField: UITextField!
    
    var payCalcViewController: PayCalcViewController
    /// Stores last active picker
    var lastPushedResID: String?

    // MARK: Initialization
    init(payCalcViewController: PayCalcViewController){
        self.payCalcViewController = payCalcViewController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder){
        fatalError("NSCoding not supported by PickerViewController.")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupPickers()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Pickers will be set as input for the invisible textfield
    fileprivate func setupPickers(){
        pickerToolbar.barStyle = UIBarStyle.default
        pickerToolbar.isTranslucent = true
        pickerToolbar.sizeToFit()
        let doneWageButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(PickerViewController.donePicker))
        let spaceWageButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        pickerToolbar.setItems([doneWageButton, spaceWageButton], animated: false)
        pickerToolbar.isUserInteractionEnabled = true
        
        self.wagePicker = commonPickerValues("wagePicker")
        self.bonusPicker = commonPickerValues("bonusPicker")
    }
    
    fileprivate func commonPickerValues(_ resID: String) -> UIPickerView {
        let newPicker = UIPickerView()
        newPicker.restorationIdentifier = resID
        newPicker.showsSelectionIndicator = true
        newPicker.dataSource = self
        newPicker.delegate = self
        return newPicker
    }

    // MARK: Actions
    
    // Passed by payCalcViewController when a picker button is clicked.
    func openPickerButton(_ sender: UIButton) {
        pickerTextField.resignFirstResponder()
        
        if sender.restorationIdentifier == nil {
            print("PickerViewController Warning: openPickerButton passed a button that doesn't have a resource ID")
            return
        }
        let resID = sender.restorationIdentifier!
        self.lastPushedResID = resID
        
        switch resID{
        // Wage Button
        case payCalcViewController.wageButton.restorationIdentifier!:
            // refresh incase province has changed
            wagePicker.reloadAllComponents()
            pickerTextField.inputView = wagePicker
            pickerTextField.inputAccessoryView = pickerToolbar
            // Set picker to displayed value (should only matter on first run or reload)
            let lastKey = payCalcViewController.payCalcData.screenValues.selectedWageKey!
            let indexOfKey = payCalcViewController.payCalcData.wages.keys.index(of: lastKey)!
            wagePicker.selectRow(indexOfKey, inComponent: 0, animated: false)
        // Meal or LOA Button
        case payCalcViewController.mealButton.restorationIdentifier!, payCalcViewController.subButton.restorationIdentifier!:
            pickerTextField.inputView = bonusPicker
            pickerTextField.inputAccessoryView = pickerToolbar
            let savedRow = resID == payCalcViewController.mealButton.restorationIdentifier! ?
                    payCalcViewController.payCalcData.screenValues.mealDays:
                    payCalcViewController.payCalcData.screenValues.loaDays
            bonusPicker.selectRow(savedRow, inComponent: 0, animated: false)
        default:
            print("PickerViewController WARNING: Button not recognized.")
            return
        }
        
        pickerTextField.becomeFirstResponder()
    }
    
    // Called when picker done button clicked or background view clicked
    @objc func donePicker(){
        if !pickerTextField.isFirstResponder{
            return
        }
        
        let pickerView = pickerTextField.inputView as! UIPickerView
        
        switch(pickerView){
        case wagePicker:
            payCalcViewController.setSelectedWage(payCalcViewController.payCalcData.wages.keys[wagePicker.selectedRow(inComponent: 0)])
        case bonusPicker:
            payCalcViewController.setMealLoa(bonusPicker.selectedRow(inComponent: 0), resID: lastPushedResID!)
        default:
            print("PickerViewController WARNING: called done picker from unrecognized picker.")
            break
        }
        
        pickerTextField.resignFirstResponder()
    }
}

// MARK: UIPickerViewDataSource
extension PickerViewController: UIPickerViewDataSource {
    // Number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Number of rows in Component
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch(pickerView){
        case wagePicker:
            return payCalcViewController.payCalcData.wages.count
        case bonusPicker:
            return 8 // 0...7 days
        default:
            return 0
        }
    }
    
    // Title for Row
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch(pickerView){
        case wagePicker:
            return payCalcViewController.payCalcData.wages.keys[row]
        case bonusPicker:
            return row == 1 ? "\(row) day" : "\(row) days"
        default:
            return nil
        }
    }
}
