//
//  StaticAnimation.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class StaticSprite: NSObject {
    open var active = true
    open var sprite: SKSpriteNode
    
    public init(sprite: SKSpriteNode){
        self.sprite = sprite
    }
}

extension StaticSprite: SpriteObj {
    public func setIsActive(_ active: Bool) {
        self.active = active
    }
    
    public func updatePosSize(_ centerPos: CGPoint, size: CGSize){
        sprite.position = centerPos
        sprite.size = size
    }
    
    public func dispose(){
        sprite.removeFromParent()
    }
}
