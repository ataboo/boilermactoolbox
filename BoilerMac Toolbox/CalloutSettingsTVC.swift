//
//  CalloutSettingsTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-05.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutSettingsTVC: UITableViewController {
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var progressSpinner: UIActivityIndicatorView!
    @IBOutlet weak var testSpinner: UIActivityIndicatorView!
    
    let calloutAPNS = CalloutAPNS()
    
    var filterTerms: [CallFilterTerm] = []
    var hideNonMatching: Bool = false
    var hideExcluded: Bool = true
    var receiveNotifications: Bool = false
    
    var waitingForAPNSChange = false

    @IBOutlet weak var hideNonMatchingCell: UITableViewCell!
    @IBOutlet weak var hideNonMatchingSwitch: UISwitch!
    
    @IBOutlet weak var hideExcludedCell: UITableViewCell!
    @IBOutlet weak var hideExcludedSwitch: UISwitch!
    
    @IBOutlet weak var editFiltersCell: UITableViewCell!
    
    @IBOutlet weak var notificationActiveCell: UITableViewCell!
    @IBOutlet weak var notificationActiveSwitch: UISwitch!
    
    @IBOutlet weak var sendTestCell: UITableViewCell!
    @IBOutlet weak var sendTestText: UILabel!
    
    class DefaultKeys {
        public static let filtersActive = "callout_filters_active"
        public static let receiveNotifications = "callout_receive_notifications"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calloutAPNS.apnDelegate = self
        
        hidesBottomBarWhenPushed = true
        
        let storedSettings = CalloutDataManager.getCalloutSettings()
        
        hideNonMatching = storedSettings.hideNonMatching
        hideExcluded = storedSettings.hideExcluded
        receiveNotifications = storedSettings.notificationsActive
        
        hideNonMatchingSwitch.setOn(hideNonMatching, animated: false)
        hideExcludedSwitch.setOn(hideExcluded, animated: false)
        
        notificationActiveSwitch.setOn(receiveNotifications, animated: false)
        updateCellActive(sendTestCell, isActive: notificationActiveSwitch.isOn)
        
        tableView.register(UINib(nibName: "CalloutTitleCell", bundle: nil),
                              forHeaderFooterViewReuseIdentifier: "HeaderCell")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell = self.tableView(tableView, cellForRowAt: indexPath)
        
        if selectedCell == hideNonMatchingCell {
            hideNonMatchingSwitch.setOn(!hideNonMatchingSwitch.isOn, animated: true)
            self.tableView.deselectRow(at: indexPath, animated: true)
        } else if selectedCell === hideExcludedCell {
            hideExcludedSwitch.setOn(!hideExcludedSwitch.isOn, animated: true)
            self.tableView.deselectRow(at: indexPath, animated: true)
        } else if selectedCell == notificationActiveCell {
            toggleAPNS()
        } else if selectedCell == sendTestCell {
            sendTestNotification()
        }
    }
    
    private func toggleAPNS() {
        guard !waitingForAPNSChange else {
            tableView.deselectRow(at: tableView.indexPath(for: notificationActiveCell)!, animated: true)
            return
        }
        
        waitingForAPNSChange = true
        
        progressSpinner.startAnimating()
        
        if notificationActiveSwitch.isOn {
            calloutAPNS.removeAPNS()
        } else {
            calloutAPNS.addAPNS()
        }
        
    }
    
    fileprivate func updateCellActive(_ cell: UITableViewCell, isActive: Bool) {
        cell.isUserInteractionEnabled = isActive
        //cell.backgroundColor = isActive ? CalloutConst.white : CalloutConst.grayLight
        sendTestText.textColor = isActive ? CalloutConst.tungsten : CalloutConst.aluminum
    }
    
    private func sendTestNotification() {
        setAPNKey(testNotification: true)
    }
    
    fileprivate func commitSettings() {
        CalloutDataManager.commitCalloutSettings(hideNonMatching: hideNonMatchingSwitch.isOn,
                                                 hideExcluded: hideExcludedSwitch.isOn,
                                                 notificationsActive: notificationActiveSwitch.isOn)
    }
    
    private func setAPNKey(testNotification: Bool = false) {
        guard !waitingForAPNSChange else {
            tableView.deselectRow(at: tableView.indexPath(for: sendTestCell)!, animated: true)
            return
        }
        
        waitingForAPNSChange = true
        
        testSpinner.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        print("Send test notification.")
        
        if testNotification {
            calloutAPNS.sendTestNotification()
        } else {
            calloutAPNS.addAPNS()
        }
    }

    @IBAction func onCancel(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Discard Changes", message: "Are you sure you want to leave without saving any changes?",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Discard", style: .destructive, handler: { _ in
            self.cancelChanges()
        }))
        
        alert.addAction(UIAlertAction(title: "Keep", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.receiveNotifications = notificationActiveSwitch.isOn
        self.hideNonMatching = hideNonMatchingSwitch.isOn
        self.hideExcluded = hideExcludedSwitch.isOn
        CalloutDataManager.commitCalloutSettings(hideNonMatching: hideNonMatching,
                                                 hideExcluded: hideExcluded,
                                                 notificationsActive: receiveNotifications)
    }
    
    func cancelChanges() {
        performSegue(withIdentifier: "rewindFromCalloutSettings", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
        
        headerCell?.textLabel?.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        return headerCell
    }
    
    @IBAction func rewindFromCalloutSettings(_ sender: UIStoryboardSegue) {
        if let filtersTVC = sender.source as? CalloutFiltersTVC {
            self.filterTerms = filtersTVC.filterTerms
            
            if receiveNotifications {
                setAPNKey()
            }
            
            commitSettings()
        }
    }
    
    fileprivate func showErrorAlert(_ error: CalloutClient.CallError?) {
        guard let callError = error else {
            return
        }
        
        if callError == .denied {
            let alert = UIAlertController(title: "Notifications Permission Disabled",
                                          message: "In order to receive callout notifications, \"Allow Notifications\" must be enabled in   settings.",
                                          preferredStyle: .alert)
        
            alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { alertAction in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.open(appSettings)
                }
                
            })
        
            self.present(alert, animated: true, completion: nil)
        } else if callError == .network {
            let alert = UIAlertController(title: "Network Unreachable",
                                          message: "Failed to detect a network connection.  Please check your network connection and try again.",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Server Unreachable", message: "Failed to enable Callout Notifications.  Please try again later.",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension CalloutSettingsTVC: APNDelegate {
    func doneAPNCall(success: Bool, error: CalloutClient.CallError?) {
        guard waitingForAPNSChange else {
            return
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        waitingForAPNSChange = false
        
        if notificationActiveCell.isSelected {
            self.progressSpinner.stopAnimating()
            self.tableView.deselectRow(at: self.tableView.indexPath(for: self.notificationActiveCell)!, animated: true)
            if success {
                self.notificationActiveSwitch.setOn(!notificationActiveSwitch.isOn, animated: true)
                updateCellActive(sendTestCell, isActive: notificationActiveSwitch.isOn)
            }
        } else {
            self.testSpinner.stopAnimating()
            self.tableView.deselectRow(at: self.tableView.indexPath(for: self.sendTestCell)!, animated: true)
        }
        
        if !success {
            showErrorAlert(error)
            
            if error == .denied {
                self.notificationActiveSwitch.setOn(false, animated: true)
            }
        }
        
        commitSettings()
    }
}
