import Foundation

class ABTaxCalc: TaxCalc {
    var province: TaxManager.Province = .AB
    var fullName: String = "Alberta"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func provTax(anGross: Double) -> Double {
        return basicProvincial(anGross: anGross)
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
}
