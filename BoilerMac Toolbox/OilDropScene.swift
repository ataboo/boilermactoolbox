//
//  PulpMillScene.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class OilDropScene: CounterScene {
    
    fileprivate static let atlasName: String = "Drip"
    fileprivate static let backgroundName: String = "OilDripBackground"
    
    
    fileprivate var dripTextures: Array<SKTexture>
    
    public required init(parentScene: SKScene){
        dripTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: OilDropScene.atlasName), prefix: "OilDrip_")
        
        super.init(parentScene: parentScene, atlasName: "Drip", backgroundName: "OilDripBackground")
        
        self.aspectRatio = 800/600
        
        earningsCounter.posOffset = CGPoint(x: -0.08, y: 0)
    }
    
    public required convenience init(parentScene: SKScene, atlasName: String, backgroundName: String) {
        self.init(parentScene: parentScene)
    }
    
    
    override open func nextFineAnim(_ startTime: Int, totalValue: Float, description: String) {
        let difference = Int((totalValue - earningsCounter.earnings) * 100)
        let dripAnim = OnceAnimation(textures: dripTextures, frameLength: 30, sprite: centeredSprite("OilDrip_00"))
        dripAnim.startAnimation(startTime - 820)
        
        addSpriteObject(dripAnim)
        
        makeFineLabel(startTime, contents: "\(difference)¢")
        
        earningsCounter.addEarningAnimation(startTime, newValue: totalValue, earningDescription: description)
    }
    
    fileprivate func makeFineLabel(_ startTime: Int, contents: String) {
        let fineLabel = OnceLabel(animProps: LabelAnimProps.sceneLabel(centerPos, screenSize: sceneSize, sceneType: .oilDrip), startTime: startTime, parentScene: parentScene)
        fineLabel.label.text = contents
        
        spriteObjects.append(fineLabel)
    }
}
