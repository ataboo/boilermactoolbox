//
//  RopeDetailViewController.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class RopeDetailViewController: UITableViewController {

    enum SelectionType: String {
        case RopeDiameter = "Select Rope Diameter"
        case RopeType = "Select Rope Type"
        case SafetyFactor = "Select Safety Factor"
    }
    
    /// All 3 set externally prep for segue
    var selectedIndex: Int?
    var selectItems: Array<String>?
    var selectionType: SelectionType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectionType?.rawValue
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectItems != nil {
            return selectItems!.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCell", for: indexPath)
        // Configure the cell...
        if selectItems != nil {
            cell.textLabel?.text = selectItems![(indexPath as NSIndexPath).row]
        }
        
        return cell
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectCell = sender as? UITableViewCell {
            selectedIndex = (tableView.indexPath(for: selectCell)! as NSIndexPath).row
        }
    }
}
