//
//  ScreenValues.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-24.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation

/// Holds values for PayCalcData.
class ScreenValues: NSObject, NSCoding {
    // MARK: Properties
    
    struct Bonus {
        init(value: Double, isTaxable: Bool){
            self.value = value
            self.isTaxable = isTaxable
        }
        var value: Double
        var isTaxable: Bool
        var taxable: Double{
            return isTaxable ? value: 0.0
        }
        var exempt: Double{
            return isTaxable ? 0.0: value
        }
    }
    
    struct Deductions{
        var federalTax = 0.0
        var provincialTax = 0.0
        var cpp = 0.0
        var ei = 0.0
        var monthlyDues = 0.0
        var fieldDues = 0.0
        
        var sumTax: Double {
            get{
                return federalTax + provincialTax
            }
        }
        
        var cppEi : Double {
            return cpp + ei
        }
        
        var sumDues: Double{
            return monthlyDues + fieldDues
        }
        
        var total : Double {
            get {
                return sumTax + cppEi + sumDues
            }
        }
    }
    
    struct Earnings {
        var wageEarnings = 0.0
        var vacBonus = 0.0
        var nightsBonus = Bonus(value: 0.0, isTaxable: true)
        var mealBonus = Bonus(value: 0.0, isTaxable: false)
        var weeklyTravel = Bonus(value: 0.0, isTaxable: false)
        var dailyTravel = Bonus(value: 0.0, isTaxable: false)
        var loaBonus = Bonus(value: 0.0, isTaxable: false)
        var net = 0.0
        
        var gross: Double {
            return taxable + exempt
        }
        
        var taxable: Double {
            return wageEarnings + vacBonus + nightsBonus.taxable + mealBonus.taxable +
                weeklyTravel.taxable + dailyTravel.taxable + loaBonus.taxable
        }
        
        var exempt: Double{
            return nightsBonus.exempt + mealBonus.exempt + weeklyTravel.exempt + dailyTravel.exempt + loaBonus.exempt
        }
    }
    
    enum SaveKeys: String {
        case FOUR_TENS = "isFourTens"
        case NIGHT_SHIFT = "isNightShift"
        case WEEK_TRAVEL = "isWeekTravel"
        case DAY_TRAVEL = "isDayTravel"
        case TAXED = "isTaxed"
        case CPP_ON = "isCppOn"
        case SELECTED_WAGE = "selectedWageKey"
        case LOA_DAYS = "loaDays"
        case MEAL_DAYS = "mealDays"
        case MONTH_DUES = "monthlyDues"
        case HOUR_DUES = "fieldDues"
        case PROV = "activeProvince"
        case YEAR = "activeYear"
        case DAY_TRAV_RATE = "dailyTravelRate"
        case WEEK_TRAV_RATE = "weeklyTravelRate"
        case LOA_RATE = "loaRate"
        case MEAL_RATE = "mealRate"
        case CUST_WAGE = "customWage"
        case CUST_VAC_RATE = "custVacationRate"
        case CUST_MONTH_DUES = "custMonthlyDues"
        case CUST_FIELD_DUES = "custFieldDues"
        case CUST_NIGHT_PREM = "custNightPrem"
        case NIGHTS_RATE = "nightshift_rate"
    }
    
    // Persistant vars
    var isFourTens = false
    var isNightShift = false
    var isWeekTravel = false
    var isDayTravel = false
    var isTaxed = true
    var isCppOn = true
    var isMonthDuesOn = false
    var isHourDuesOn = true
    var selectedWageKey: String?
    var loaDays = 0
    var mealDays = 0
    var activeProv: TaxManager.Province = TaxManager.Province.AB
    var activeYear: String = TaxManager.defaultYearName
    var nightsRate: Float = 0
    
    // Changed via settings
    var mealRate: Float = 25
    var weekTravRate: Float = 220
    var dayTravRate: Float = 20
    var loaRate: Float = 215
    var custWageRate: Float = 40.10
    // Will be nil if inactive
    var customVacRate: Float?
    var custMonthDues: Float?
    var custFieldDues: Float?
    var custNightShift: Float?
    
    // Calculated vars
    var hoursTotal = [0.0, 0.0, 0.0]
    var wageRate = 0.0
    var fieldDuesRate = Float(0.0375)
    var activeVacRate = Float(0.10)
    var nightOT = false
    
    var earnings = Earnings()
    var deductions = Deductions()
    
    // MARK: Innitialization
    init(defaultWageKey: String?){
        self.selectedWageKey = defaultWageKey
    }
    
    func updateCalc(_ payCalcData: PayCalcData, dayRefs: Dictionary<String, DayReference>){
        let taxManager = payCalcData.taxManager
        let activeProvVal = taxManager.activeProvince.provTaxVal
        
        // Total hours [Straight, OT, Double Time]
        self.hoursTotal = [0.0, 0.0, 0.0]
        // Used to calculate daily travel
        var daysWorked = 0
        
        // TODO: Override with preference values
        self.nightOT = activeProvVal.nightOT
        
        
        // Calculate hours from dayRefs
        for dayRef: DayReference in dayRefs.values {
            // Sets label to sum of hours without multipliers
            dayRef.updateButtonLabel()
            
            let hourSum = dayRef.hours.reduce(0, +)
            if hourSum > 0 {
                daysWorked += 1
            } else {
                continue
            }
            
            let splitHours = taxManager.splitShiftHours(dayRef: dayRef, fourTens: isFourTens)
            
            for i in 0..<splitHours.count {
                hoursTotal[i] += Double(splitHours[i])
            }
        }
        let hoursWorked = hoursTotal.reduce(0, +)
        
        // Set vals from provincial data
        if let wageKey = selectedWageKey, let rate = payCalcData.wages[wageKey]{
            wageRate = rate
        } else {
            print("ScreenValues/updateCalc: Erroneous selected wage key.")
        }
        
        self.activeVacRate = activeProvVal.vacRate!
        if customVacRate != nil{
            self.activeVacRate = customVacRate! / Float(100)
        }
        
        // Earnings
        earnings.wageEarnings = (hoursTotal[0] + 1.5 * hoursTotal[1] + 2.0 * hoursTotal[2]) * wageRate
        earnings.mealBonus = Bonus(value: Double(mealRate) * Double(mealDays), isTaxable: false)
        earnings.loaBonus = Bonus(value: Double(loaRate) * Double(loaDays), isTaxable: false)
        nightsRate = custNightShift ?? activeProvVal.nightsRate ?? Float(0)
        earnings.nightsBonus = isNightShift ? Bonus(value: Double(nightsRate) * hoursWorked, isTaxable: true) :
                Bonus(value: 0.0, isTaxable: true)
        earnings.vacBonus = Double(activeVacRate) * (earnings.wageEarnings + earnings.nightsBonus.value)
        earnings.weeklyTravel = isWeekTravel ? Bonus(value: Double(weekTravRate), isTaxable: false) :
                Bonus(value: 0.0, isTaxable: false)
        earnings.dailyTravel = isDayTravel ? Bonus(value: Double(dayTravRate) * Double(daysWorked), isTaxable: false) :
                Bonus(value: 0.0, isTaxable: false)
        
        // Deductions
        let monthlyDuesRate = custMonthDues ?? activeProvVal.monthlyDuesRate!
        deductions.monthlyDues = isMonthDuesOn ? Double(monthlyDuesRate) : 0.0
        
        self.fieldDuesRate = activeProvVal.fieldDuesRate!
        if custFieldDues != nil {
            self.fieldDuesRate = custFieldDues! / Float(100)
        }
        deductions.fieldDues = isHourDuesOn ? Double(fieldDuesRate) * (earnings.wageEarnings + earnings.nightsBonus.value) : 0.0
        
        let taxReturn = taxManager.getTax(taxableGross: earnings.taxable, taxExempt: deductions.fieldDues + deductions.monthlyDues)
        deductions.federalTax = isTaxed ? taxReturn.fedTax: 0.0
        deductions.provincialTax = isTaxed ? taxReturn.provTax: 0.0
        
        deductions.cpp = isCppOn ? taxReturn.cppVal: 0.0
        deductions.ei = isCppOn ? taxReturn.eiVal: 0.0
        
        earnings.net = earnings.gross - deductions.total
    }
    
    // MARK: NSCoding
    required convenience init?(coder aDecoder: NSCoder){
        self.init(defaultWageKey: nil)
        
        // Prevents default value from being overwritten if key doesn't exist
        self.isFourTens = decodeBoolOrNil(aDecoder, key: SaveKeys.FOUR_TENS.rawValue) ?? isFourTens
        self.isNightShift = decodeBoolOrNil(aDecoder, key: SaveKeys.NIGHT_SHIFT.rawValue) ?? isNightShift
        self.isWeekTravel = decodeBoolOrNil(aDecoder, key: SaveKeys.WEEK_TRAVEL.rawValue) ?? isWeekTravel
        self.isDayTravel = decodeBoolOrNil(aDecoder, key: SaveKeys.DAY_TRAVEL.rawValue) ?? isDayTravel
        self.isTaxed = decodeBoolOrNil(aDecoder, key: SaveKeys.TAXED.rawValue) ?? isTaxed
        self.isCppOn = decodeBoolOrNil(aDecoder, key: SaveKeys.CPP_ON.rawValue) ?? isCppOn
        self.isHourDuesOn = decodeBoolOrNil(aDecoder, key: SaveKeys.HOUR_DUES.rawValue) ?? isHourDuesOn
        self.isMonthDuesOn = decodeBoolOrNil(aDecoder, key: SaveKeys.MONTH_DUES.rawValue) ?? isMonthDuesOn
        
        
        self.loaDays = decodeIntOrNil(aDecoder, key: SaveKeys.LOA_DAYS.rawValue) ?? loaDays
        self.mealDays = decodeIntOrNil(aDecoder, key: SaveKeys.MEAL_DAYS.rawValue) ?? mealDays
        
        self.mealRate = decodeFloatOrNil(aDecoder, key: SaveKeys.MEAL_RATE.rawValue) ?? mealRate
        self.weekTravRate = decodeFloatOrNil(aDecoder, key: SaveKeys.WEEK_TRAV_RATE.rawValue) ?? weekTravRate
        self.dayTravRate = decodeFloatOrNil(aDecoder, key: SaveKeys.DAY_TRAV_RATE.rawValue) ?? dayTravRate
        self.loaRate = decodeFloatOrNil(aDecoder, key: SaveKeys.LOA_RATE.rawValue) ?? loaRate
        self.custWageRate = decodeFloatOrNil(aDecoder, key: SaveKeys.CUST_WAGE.rawValue) ?? custWageRate
        self.nightsRate = decodeFloatOrNil(aDecoder, key: SaveKeys.NIGHTS_RATE.rawValue) ?? nightsRate
        
        self.customVacRate = decodeFloatOrNil(aDecoder, key: SaveKeys.CUST_VAC_RATE.rawValue)
        self.custMonthDues = decodeFloatOrNil(aDecoder, key: SaveKeys.CUST_MONTH_DUES.rawValue)
        self.custFieldDues = decodeFloatOrNil(aDecoder, key: SaveKeys.CUST_FIELD_DUES.rawValue)
        self.custNightShift = decodeFloatOrNil(aDecoder, key: SaveKeys.CUST_NIGHT_PREM.rawValue)
        
        if let provRaw = aDecoder.decodeObject(forKey: SaveKeys.PROV.rawValue) as? String, let provEnum = TaxManager.Province(rawValue: provRaw){
            self.activeProv = provEnum
        } else {
            print("ScreenValues Warning: couldn't load activeProv")
        }
        
        self.activeYear = aDecoder.decodeObject(forKey: SaveKeys.YEAR.rawValue) as? String ?? activeYear
        
        // selectedWageKey can end up nil if no value is decoded.        
        self.selectedWageKey = aDecoder.decodeObject(forKey: SaveKeys.SELECTED_WAGE.rawValue) as? String
        
    }
    
    func encode(with aCoder: NSCoder) {
        //aCoder.encodeBool(isHol, forKey: holKey)
        
        //var isMonthDuesOn = false
        //var isHourDuesOn = true
        
        aCoder.encode(isFourTens, forKey: SaveKeys.FOUR_TENS.rawValue)
        aCoder.encode(isNightShift, forKey: SaveKeys.NIGHT_SHIFT.rawValue)
        aCoder.encode(isWeekTravel, forKey: SaveKeys.WEEK_TRAVEL.rawValue)
        aCoder.encode(isDayTravel, forKey: SaveKeys.DAY_TRAVEL.rawValue)
        aCoder.encode(isTaxed, forKey: SaveKeys.TAXED.rawValue)
        aCoder.encode(isCppOn, forKey: SaveKeys.CPP_ON.rawValue)
        aCoder.encode(isMonthDuesOn, forKey: SaveKeys.MONTH_DUES.rawValue)
        aCoder.encode(isHourDuesOn, forKey: SaveKeys.HOUR_DUES.rawValue)
        
        aCoder.encode(loaDays, forKey: SaveKeys.LOA_DAYS.rawValue)
        aCoder.encode(mealDays, forKey: SaveKeys.MEAL_DAYS.rawValue)
        
        aCoder.encode(activeProv.rawValue, forKey: SaveKeys.PROV.rawValue)
        aCoder.encode(activeYear, forKey: SaveKeys.YEAR.rawValue)
        
        
        aCoder.encode(mealRate, forKey: SaveKeys.MEAL_RATE.rawValue)
        aCoder.encode(weekTravRate, forKey: SaveKeys.WEEK_TRAV_RATE.rawValue)
        aCoder.encode(dayTravRate, forKey: SaveKeys.DAY_TRAV_RATE.rawValue)
        aCoder.encode(loaRate, forKey: SaveKeys.LOA_RATE.rawValue)
        aCoder.encode(custWageRate, forKey: SaveKeys.CUST_WAGE.rawValue)
        aCoder.encode(nightsRate, forKey: SaveKeys.NIGHTS_RATE.rawValue)
        
        if customVacRate != nil{
            aCoder.encode(customVacRate!, forKey: SaveKeys.CUST_VAC_RATE.rawValue)
        }
        if custMonthDues != nil{
            aCoder.encode(custMonthDues!, forKey: SaveKeys.CUST_MONTH_DUES.rawValue)
        }
        if custFieldDues != nil{
            aCoder.encode(custFieldDues!, forKey: SaveKeys.CUST_FIELD_DUES.rawValue)
        }
        if custNightShift != nil{
            aCoder.encode(custNightShift!, forKey: SaveKeys.CUST_NIGHT_PREM.rawValue)
        }
        
        // Just incase it's coded as a string that says nil or something
        if selectedWageKey != nil {
            aCoder.encode(selectedWageKey, forKey: SaveKeys.SELECTED_WAGE.rawValue)
        }
    }
    
    fileprivate func decodeBoolOrNil(_ aDecoder: NSCoder, key: String) -> Bool? {
        if aDecoder.containsValue(forKey: key) {
            return aDecoder.decodeBool(forKey: key)
        }
        print("ScreenValues: No value for key \(key) in decoder.")
        return nil
    }
    
    fileprivate func decodeIntOrNil(_ aDecoder: NSCoder, key: String) -> Int? {
        if aDecoder.containsValue(forKey: key) {
            return aDecoder.decodeInteger(forKey: key)
        }
        return nil
    }
    
    fileprivate func decodeFloatOrNil(_ aDecoder: NSCoder, key: String) -> Float? {
        if aDecoder.containsValue(forKey: key){
            return aDecoder.decodeFloat(forKey: key)
        }
        return nil
    }
}
