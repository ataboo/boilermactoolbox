//
//  CalloutDetailTVC.swift
//  
//
//  Created by Alex Raboud on 2016-09-07.
//
//

import UIKit

class CalloutDetailTVC: UITableViewController {
    enum Sections: Int {
        case jobs = 0
        case nameHires
        case details
        
        static var values: [Sections] {
            get {
                var sections: [Sections] = []
                var i = 0
                while let section = Sections(rawValue: i) {
                    sections.append(section)
                    i += 1
                }
                return sections
            }
        }
    }
    
    var activeSections: [Sections] = [.details]
    
    var callData: CalloutObject!
    
    private var detailEntries: [CallFilterTerm.TermType: String] = [:]
    
    private var detailEntriesOrdered: [CallFilterTerm.TermType] {
        return CallFilterTerm.TermType.values.filter({
            detailEntries.keys.contains($0)})
    }
    
    private var jobEntries = [JobJect]()
    private var namehireEntries = [JobJect]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if callData == nil {
            self.dismiss(animated: false, completion: nil)
        }
        
        setupTableView()
        
        setCellText()
    }
    
    private func setupTableView() {
        self.title = "Job#: \(callData.jobId)"
        
        if callData.nameHires.count > 0 {
            activeSections.append(.nameHires)
        }
        
        if callData.manpower.count > 0 {
            activeSections.append(.jobs)
        }
        
        activeSections.sort(by: {$0.rawValue < $1.rawValue})
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = CalloutConst.grayLight
        tableView.bounces = false
    }
    
    private func setCellText() {
        tableView.register(UINib(nibName: "CalloutDetailCell", bundle: nil),
                              forCellReuseIdentifier:"CalloutDetailCell")
        tableView.register(UINib(nibName: "CalloutJobCell", bundle: nil),
                              forCellReuseIdentifier:"CalloutJobCell")
        
        tableView.register(UINib(nibName: "CalloutTitleCell", bundle: nil),
                                         forHeaderFooterViewReuseIdentifier: "HeaderCell")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, YYYY - HH:mm"
        
        detailEntries = [
            .jobName : callData.jobName.capitalizeNonAcronyms(),
            .contractor : callData.contractor.capitalizeNonAcronyms(),
            .openTo : callData.openTo.capitalizeNonAcronyms(),
            .dateTime : formatter.string(from: callData.dateTime),
            .hours : callData.scheduleShift.capitalizeNonAcronyms(),
            .duration : callData.duration.capitalizeNonAcronyms(),
            .accomodation : callData.accommodation.capitalizeNonAcronyms(),
            .drugTesting : callData.drugTesting.capitalizeNonAcronyms(),
            .comments : callData.comments
                .capitalizeNonAcronyms()
                .trimmingCharacters(in: CharacterSet(charactersIn: "-"))
                .replacingOccurrences(of: "--", with: "\n\n")
        ]
        
        for (index, value) in callData.manpower.enumerated() {
            let jobJect = JobJect(jobDescription: value)
            if let manpowerMatches = callData.matchMap[.manpower] {
                jobJect.matchLevel = manpowerMatches[index]
            }
            jobEntries.append(jobJect)
        }
        for (index, value) in callData.nameHires.enumerated() {
            let jobJect = JobJect(nameHire: value)
            if let nameHireMatches = callData.matchMap[.nameHire] {
                jobJect.matchLevel = nameHireMatches[index]
            }
            namehireEntries.append(jobJect)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return activeSections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionEnum = activeSections[section]
        
        switch sectionEnum {
        case .details:
            return detailEntries.count
        case .jobs:
            return jobEntries.count
        default:
            return namehireEntries.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionEnum = activeSections[indexPath.section]
        
        switch sectionEnum {
        case .details:
            return getDetailCell(indexPath)
        default:
            return getJobCell(section: sectionEnum, indexPath: indexPath)
        }
    }
    
    private func getDetailCell(_ indexPath: IndexPath) -> CalloutDetailCell {
        let detailCell = tableView.dequeueReusableCell(withIdentifier: "CalloutDetailCell", for: indexPath) as! CalloutDetailCell
        
        let detailType = detailEntriesOrdered[indexPath.row]
        
        detailCell.textLabel?.text = detailType.string
        detailCell.detailTextLabel?.text = detailEntries[detailType]!
        detailCell.isUserInteractionEnabled = false
        var backColor = CalloutConst.white
        if let detailMatch = callData.matchMap[detailType] {
            detailCell.setChecked(detailMatch.first!.rawValue)
            backColor = CalloutConst.backColors[detailMatch.first!.rawValue]!
        }
        detailCell.contentView.backgroundColor = backColor
        
        return detailCell
    }
    
    private func getJobCell(section: Sections,  indexPath: IndexPath) -> CalloutJobCell {
        let jobJectList = section == .jobs ? jobEntries : namehireEntries
        
        let jobJect = jobJectList[indexPath.row]
        
        let jobCell = tableView.dequeueReusableCell(withIdentifier: "CalloutJobCell",
                                                                  for: indexPath) as! CalloutJobCell
        jobCell.isUserInteractionEnabled = false
        
        jobCell.setJobJect(jobJect)
        
        return jobCell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch activeSections[section] {
        case .jobs:
            if jobEntries.count > 0 {
                return "Calls:"
            }
        case .nameHires:
            if namehireEntries.count > 0 {
                return "Name Hires:"
            }
        case .details:
            return "Details:"
    
        }
        return nil
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let headerText = self.tableView(tableView, titleForHeaderInSection: section) {
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
            cell?.textLabel?.text = headerText
            return cell
        }
        
        return nil
    }
}
