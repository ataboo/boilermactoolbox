import Foundation

class QCTaxCalc {
    var province: TaxManager.Province = .QC
    var fullName: String = "Quebec"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName
}

extension QCTaxCalc: TaxCalc {
    func fedTax(anGross: Double) -> Double {
        let fedTax = fedTaxVal
        
        let bracketIndex = getBracketIndex(anGross, brackets: fedTax.brackets!)
        let taxRate = Double(fedTax.rates![bracketIndex])
        let constKVal = Double(fedTax.constK![bracketIndex])
        let basicAmount = anGross * taxRate
        
        let taxCred = getQCTaxCredit(anGross: anGross, provVal: provTaxVal, fedVal: fedTax)
            
        var anTax = basicAmount - taxCred - constKVal
        //16.5% Abatement
        anTax  -= 0.165 * anTax
            
        return max(anTax, 0.0)
    }
    
    func provTax(anGross: Double) -> Double {
        let provVal = provTaxVal
        
        let deduction = min(0.06 * anGross, Double(provVal.empDeduction!))
        
        let taxableAnnual = anGross - deduction
        let taxBracket = getBracketIndex(taxableAnnual, brackets: provVal.brackets!)
        
        let basicTax = taxableAnnual * Double(provVal.rates![taxBracket]) - Double(provVal.constK![taxBracket])
        let claimReduction = Double(provVal.claimAmount!) * Double(provVal.rates![0])
        let healthPrem = getQCHealthPrem(taxableAnnual, provVal: provVal)
        
        return basicTax - claimReduction + healthPrem
    }
    
    private func getQCTaxCredit(anGross: Double, provVal: TaxVal, fedVal: TaxVal) -> Double {
        let qppQpip = getQppQpip(anGross)
        var qppCont = qppQpip.qppVal
        var qpipCont = qppQpip.qpipVal
        var eiCont = qppQpip.eiVal
        
        let maxQppCont = Double(provVal.qppMax!)
        let maxQpipCont = Double(provVal.qpipMax!)
        let maxEICont = Double(provVal.cppEi![1])
        let claimAmount = Double(fedVal.claimAmount!)
        
        qppCont = qppCont > maxQppCont ? maxQppCont : qppCont
        qpipCont = qpipCont > maxQpipCont ? maxQpipCont : qpipCont
        eiCont = eiCont > maxEICont ? maxEICont : eiCont
        
        let taxCredit = (claimAmount + qppCont + qpipCont + eiCont) * Double(fedVal.rates![0])
        
        return taxCredit
    }
    
    private func getQCHealthPrem(_ anGross: Double, provVal: TaxVal) -> Double{
        guard let healthBrackets = provVal.healthBrackets else {
            return 0.0
        }
        
        let healthIndex = getBracketIndex(anGross, brackets: healthBrackets)
        if healthIndex == 0 {return 0.0}
        
        let addFlat = healthIndex == 1 ? 0.0 : Double(provVal.healthAmounts![healthIndex - 1])
        let healthCalc = (anGross - Double(provVal.healthBrackets![healthIndex])) *
            Double(provVal.healthRates![healthIndex]) + addFlat
        let healthFlat = Double(provVal.healthAmounts![healthIndex])
        return healthCalc < healthFlat ? healthCalc : healthFlat
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        let qpp = getQppQpip(anGross)
        return (cppVal: qpp.qppVal, eiVal: qpp.qpipVal + qpp.eiVal)
    }
    
    private func getQppQpip(_ anGross: Double) -> (qppVal: Double, qpipVal: Double, eiVal: Double){
        let cppExempt = Double(fedTaxVal.cppEi![1])
        var qppCont = (anGross - cppExempt) * Double(provTaxVal.qppRate!)
        var qpipCont = anGross * Double(provTaxVal.qpipRate!)
        var eiCont = Double(provTaxVal.cppEi![0]) * anGross
        qppCont = max(qppCont, 0.0)
        qpipCont = max(qpipCont, 0.0)
        eiCont = max(eiCont, 0.0)
        
        return(qppCont, qpipCont, eiCont)
    }
}
