//
//  CalloutJobCell.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-08.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutJobCell: UITableViewCell {

    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var jobCountLabel: UILabel!
    @IBOutlet weak var iconView: JobIconView!
    
    
    override var textLabel: UILabel? {
        return jobLabel
    }
    
    override var detailTextLabel: UILabel? {
        return jobCountLabel
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setJobJect(_ jobJect: JobJect) {
        iconView.setJobType(jobJect.jobType)
        iconView.highlightState = jobJect.matchLevel.rawValue
        jobLabel.text = jobJect.jobName
        jobCountLabel.text = jobJect.jobCount
        self.contentView.backgroundColor = CalloutConst.backColors[jobJect.matchLevel.rawValue]
    }
}
