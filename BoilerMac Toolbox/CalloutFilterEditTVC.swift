//
//  CalloutFilterEditTVC.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-11.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

/*
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}
 */


class CalloutFilterEditTVC: UITableViewController {
    
    enum Sections: Int {
        case keyword = 0
        case filterType
        case inclusive
        
        var toString: String {
            get {
                switch self {
                case .keyword:
                    return "Keyword"
                case .filterType:
                    return "Section"
                case .inclusive:
                    return "Filter Type"
                }
            }
        }
        
        fileprivate static var valueList: [Sections]?
        
        static var values: [Sections] {
            get {
                if valueList == nil {
                    valueList = [Sections]()
                    var i = 0
                    while let section = Sections(rawValue: i) {
                        valueList!.append(section)
                        i += 1
                    }
                }
                return valueList!
            }
        }
    }
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var filterTerm: CallFilterTerm = CallFilterTerm(termType: .jobName, keyWord: "", inclusive: true)
    var returnFilter: CallFilterTerm? = nil
    
    fileprivate var sectionMap = [Sections: [String]]()
    fileprivate var selectedMap = [Sections: Int]()
    
    // MARK: Initilization
    
    override func viewDidLoad() {
        setupTable()
    }
    
    var selectBG : UIView {
        get {
            let bgView = UIView()
            bgView.backgroundColor = CalloutConst.tranBlue
            bgView.isUserInteractionEnabled = false
            return bgView
        }
    }
    
    fileprivate func setupTable() {
        
        
        tableView.register(UINib(nibName: "CalloutTitleCell", bundle: nil),
                              forHeaderFooterViewReuseIdentifier: "HeaderCell")
        
        tableView.bounces = false
        
        updateSelectedMapFromFilterTerm()
        
        sectionMap[.inclusive] = ["Include", "Exclude"]
        sectionMap[.filterType] = CallFilterTerm.TermType.values.map({$0.rawValue})
        sectionMap[.keyword] = [filterTerm.keyWord]
        
        refreshKeywordCell()
    }
    
    // MARK: TableView Delegate/Data
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return Sections.values.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionMap[Sections(rawValue: section)!]!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = Sections(rawValue: indexPath.section)!
        
        
        let cellId = section == .keyword ? "KeywordCell" : "FilterCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        cell.selectedBackgroundView = selectBG
        cell.imageView?.image = nil
        cell.textLabel?.text = CallFilterTerm.TermType(rawValue: sectionMap[section]![(indexPath).row])?.string
        switch section {
        case .inclusive:
            cell.textLabel?.text = sectionMap[section]![indexPath.row]
            let image = (indexPath as NSIndexPath).row == 0 ? CalloutConst.checkImage : CalloutConst.xImage
            cell.imageView?.image = image?.resizedAndTintable(0.6)
            cell.imageView?.tintColor = (indexPath).row == 0 ? CalloutConst.green : CalloutConst.red
        case .keyword:
            cell.textLabel?.text = "Set Keywords:"
            cell.detailTextLabel?.text = filterTerm.keyWord
            cell.accessoryView = UIImageView(image: UIImage(named: "ic_keyboard_arrow_right"))
        case .filterType:
            break
        }
        
        return cell
    }
    
    // Stops deselecting cell on click selected
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = Sections(rawValue: indexPath.section)!
        
        if section == .keyword {
            launchKeywordPick()
            self.tableView.deselectRow(at: indexPath, animated: true)
            return
        }
        
        let oldSelect = IndexPath(row: selectedMap[section]!, section: indexPath.section)
        
        if indexPath != oldSelect {
            tableView.deselectRow(at: IndexPath(row: (oldSelect as NSIndexPath).row, section: (oldSelect as NSIndexPath).section), animated: true)
            selectedMap[section] = (indexPath as NSIndexPath).row
            
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath, oldSelect], with: .fade)
            tableView.endUpdates()
        
            updateFilterTermFromSelectedMap()
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderCell")
        
        cell?.textLabel?.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Sections(rawValue: section)!.toString
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == Sections.keyword.rawValue {
            return
        }
        
        if selectedMap[Sections(rawValue: (indexPath as NSIndexPath).section)!]! == (indexPath as NSIndexPath).row {
            cell.setSelected(true, animated: false)
        }
    }
    
    fileprivate func updateFilterTermFromSelectedMap() {
        let termType = CallFilterTerm.TermType.values[selectedMap[.filterType]!]
        
        let inclusive = selectedMap[.inclusive] == 0

        filterTerm = CallFilterTerm(termType: termType,
                                    keyWord: filterTerm.keyWord,
                                    inclusive: inclusive)
    }
    
    // Only called on initial setup
    fileprivate func updateSelectedMapFromFilterTerm() {
        selectedMap[.inclusive] = filterTerm.inclusive ? 0 : 1
        selectedMap[.filterType] = CallFilterTerm.TermType.values.index(of: filterTerm.termType)
    }
    
    fileprivate func refreshKeywordCell() {
        tableView.beginUpdates()
        tableView.reloadRows(at: [IndexPath(row: 0,
            section: Sections.keyword.rawValue)], with: .fade)
        tableView.endUpdates()
        
        saveButton.isEnabled = !filterTerm.keyWord.isEmpty
    }
    
    fileprivate func launchKeywordPick(){
        
        let keywordPick = UIAlertController(title: "Set Keyword", message: nil, preferredStyle: .alert)
            
        let doneAction = UIAlertAction(title: "Save", style: .default, handler: { _ in
            let textField = keywordPick.textFields![0] as UITextField
            self.setKeyword(textField.text!)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in})
        
        keywordPick.addTextField(configurationHandler: { textField in
            textField.placeholder = "Enter here or choose"
            textField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
            textField.text = self.filterTerm.keyWord
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange,
                object: textField, queue: OperationQueue.main, using: { notification in
                    doneAction.isEnabled = textField.text!.count < 54
            })
        })
        
        makeActionsForSuggestions().forEach({
            keywordPick.addAction($0)
        })
        
        keywordPick.addAction(doneAction)
        keywordPick.addAction(cancelAction)
        
        self.present(keywordPick, animated: true, completion: nil)
    }
    
    fileprivate func makeActionsForSuggestions() -> [UIAlertAction] {
        var actions: [UIAlertAction] = []
        
        for suggestion in filterTerm.termType.suggestions {
            actions.append(UIAlertAction(title: suggestion.0, style: .default, handler: { _ in
                self.setKeyword(suggestion.values)
            }))
        }
        
        return actions
    }
    
    fileprivate func setKeyword(_ keyword: String) {
        filterTerm.keyWord = keyword
        self.refreshKeywordCell()
    }
    
    // MARK: Navigation
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let senderButton = sender as? UIBarButtonItem {
            if senderButton == saveButton {
                self.returnFilter = filterTerm
            }
        }
    }
}
