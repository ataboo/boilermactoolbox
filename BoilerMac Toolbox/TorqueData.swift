//
//  TorqueData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-12-26.
//  Copyright © 2015 atasoft. All rights reserved.
//

import UIKit

struct Pattern{
    var title: String
    var image: UIImage
    var index: Int
}

class TorqueData: NSObject, NSCoding {
    enum SaveKeys : String {
        case SelectedPattern = "selected_pattern"
        case StudCount = "stud_count"
    }
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("torquedata")
    
    fileprivate let rollFourImage = UIImage(named: "rolling_fourpoint")!
    fileprivate let rollEightImage = UIImage(named: "rolling_eightpoint")!
    fileprivate let revEightImage = UIImage(named: "rolling_eightreverse")!
    
    fileprivate var selectedPattern: Pattern?
    var studCount: Int
    
    fileprivate var patternList = [Pattern]()
    
    var patternCount: Int {
        get{
            return patternList.count
        }
    }
    
    override init(){
        let patternNames = ["Rolling 4-Point", "Rolling 8-Point", "8-Point Reverse"]
        let patternImages = [rollFourImage, rollEightImage, revEightImage]
        for (index, val) in patternNames.enumerated(){
            patternList.append(Pattern(title: val, image: patternImages[index], index: index))
        }
        self.studCount = 8
    }
    
    func patternAt(_ index: Int) -> Pattern {
        return patternList[index]
    }
    
    func selectedPatternSet(_ pattern: Pattern){
        self.selectedPattern = pattern
    }
    
    
    func getSelectedPattern() -> Pattern {
        return selectedPattern ?? patternAt(0)
    }
    
    static func studCountArray() -> [Int]{
        var studArray = [Int]()
        for index: Int in 2...(200 / 4){
            studArray.append(index * 4)
        }
        
        return studArray
    }
    
    func makeSequence(_ pattern: Pattern, studCount: Int) -> String{
        if !validateStudCount(studCount){
            fatalError("makePattern: studCount must be 8 or greater and divisible by 4.")
        }
        
        switch pattern.index{
        case 1:
            return rollingEightPoint(studCount)
        case 2:
            return reverseEightPoint(studCount)
        default:
            return rollingFourPoint(studCount)
        }
    }
    
    fileprivate func validateStudCount(_ studCount: Int) -> Bool{
        return studCount > 7 && studCount % 4 == 0
    }
    
    fileprivate func rollingFourPoint(_ studCount: Int) -> String{
        var vals = [Int]()
        
        for quarter in [1, 3, 2, 4]{
            for count in 0...studCount/4{
                let nextVal = quarter + count * 4
                
                if nextVal <= studCount {
                    vals.append(nextVal)
                }
            }
        }
        return stringFromIntArray(vals)
    }
    
    fileprivate func rollingEightPoint(_ studCount: Int) -> String{
        var vals = [Int]()
        
        for quarter in [1, 5, 3, 7, 2, 6, 4, 8]{
            for count in 0...studCount/8{
                let nextVal = quarter + count * 8
                
                if nextVal <= studCount{
                    vals.append(nextVal)
                }
            }
        }
        
        return stringFromIntArray(vals)
    }
    
    fileprivate func reverseEightPoint(_ studCount: Int) -> String{
        var vals = [Int]()
        
        for quarter in [1, 5, 3, 7, 2, 6, 4, 8]{
            vals.append(quarter)
            for count in 0...((studCount / 8) - 1) {
                let nextVal = quarter + (studCount / 8-count) * 8
                if nextVal <= studCount {
                    vals.append(nextVal)
                }
            }
        }
        
        return stringFromIntArray(vals)
    }
    
    fileprivate func stringFromIntArray(_ valIntArray: [Int]) -> String{
        if valIntArray.count != studCount {
            //fatalError("TorqueData: sequence array length mismatch.")
        }
        
        var valString = ""
        for val in valIntArray{
            if val != valIntArray[valIntArray.count - 1]{
                valString += ("\(val), ")
            } else {
                valString += ("\(val)")
            }
        }
        return valString
    }

    // MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        let patternIndex = aDecoder.decodeInteger(forKey: SaveKeys.SelectedPattern.rawValue)
        self.selectedPattern = patternAt(patternIndex)
        self.studCount = aDecoder.decodeInteger(forKey: SaveKeys.StudCount.rawValue)
        if studCount == 0 {
            self.studCount = 8
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(studCount, forKey: SaveKeys.StudCount.rawValue)
        let patternIndex: Int = getSelectedPattern().index
        aCoder.encode(patternIndex, forKey: SaveKeys.SelectedPattern.rawValue)
    }
}
