//
//  CalloutDetailCell.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-09-08.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

class CalloutDetailCell: UITableViewCell {
    @IBOutlet weak var titleField: UILabel!
    @IBOutlet weak var detailField: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    override var textLabel: UILabel? {
        return titleField
    }
    
    override var detailTextLabel: UILabel? {
        return detailField
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setChecked(0)
    }

    func setChecked(_ checkInt: Int) {
        iconImage.image = checkInt == 0 ? nil : checkInt > 0 ? UIImage(named: "ic_check_circle_48pt") : UIImage(named: "ic_clear_48pt")
        iconImage.image = iconImage.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        iconImage.tintColor = checkInt > 0 ? CalloutConst.green : CalloutConst.red
    }
    
}
