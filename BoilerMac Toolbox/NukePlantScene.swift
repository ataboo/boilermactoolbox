//
//  PulpMillScene.swift
//  SpriteTesting
//
//  Created by Alex Raboud on 2016-03-25.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit
import SpriteKit

open class NukePlantScene: CounterScene {
    fileprivate static let atlasName: String = "NukePlant"
    fileprivate static let smashAtlasName: String = "NukeSmash"
    fileprivate static let backgroundName: String = "NukeBackground"
    
    
    fileprivate var smashTextures: Array<SKTexture>
    fileprivate var electricTextures: Array<SKTexture>
    
    public required init(parentScene: SKScene){
        
        smashTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: NukePlantScene.smashAtlasName), prefix: "NukeSmash_")
        
        electricTextures = SpriteUtils.texturesFromAtlas(SKTextureAtlas(named: NukePlantScene.atlasName), prefix: "NukeElectric_")
        
        super.init(parentScene: parentScene, atlasName: NukePlantScene.atlasName, backgroundName: NukePlantScene.backgroundName)
        
        earningsCounter.posOffset = CGPoint(x: 0.2, y: 0.2)
        
        self.aspectRatio = 1024/720
        
        
        let smokeLoop = LoopAnimation(atlas: SKTextureAtlas(named: NukePlantScene.atlasName), prefix: "NukeSmoke_", frameLength: 80, sprite: centeredSprite("NukeSmoke_03"))
        addSpriteObject(smokeLoop)
        
    }
    
    public required convenience init(parentScene: SKScene, atlasName: String, backgroundName: String) {
        self.init(parentScene: parentScene)
    }
    
    override open func nextFineAnim(_ startTime: Int, totalValue: Float, description: String) {
        let difference = Int((totalValue - earningsCounter.earnings) * 100)
        
        let powerAnim = OnceAnimation(textures: electricTextures, frameLength: 60, sprite: centeredSprite("NukeElectric_00"))
        powerAnim.startAnimation(startTime - 500)
        
        addSpriteObject(powerAnim)
        
        makeFineLabel(startTime, contents: "\(difference)¢")
        
        earningsCounter.addEarningAnimation(startTime, newValue: totalValue, earningDescription: description)
    }
    
    fileprivate func makeFineLabel(_ startTime: Int, contents: String) {
        let fineLabel = OnceLabel(animProps: LabelAnimProps.sceneLabel(centerPos, screenSize: sceneSize, sceneType: .nukePlant), startTime: startTime, parentScene: parentScene)
        fineLabel.label.text = contents
        
        spriteObjects.append(fineLabel)
    }
    
    override open func nextCoarseAnim(_ startTime: Int) {
        let smashAnim = OnceAnimation(textures: smashTextures, frameLength: 80, sprite: centeredSprite("NukeSmash_00"))
        
        smashAnim.sprite.zPosition = 5
        
        smashAnim.startAnimation(startTime - 500)
        
        addSpriteObject(smashAnim)
    }
}
