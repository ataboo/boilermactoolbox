//
//  PayCalcData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2015-11-15.
//  Copyright © 2015 atasoft. All rights reserved.
//

import Foundation
import UIKit

class GrossDataStorage: NSObject, NSCoding {
    // Keys for saving values via NSCoder
    enum SaveKeys: String {
        case PROV = "prov_val"
        case YEAR = "year_val"
        case GROSS = "gross_val"
    }
    
    var provRaw: String!
    var activeYear: String!
    var grossVal: Double!
    
    override init() {
        super.init()
    }
    
    convenience init(provRaw: String, activeYear: String, grossVal: Double) {
        self.init()
        
        self.provRaw = provRaw
        self.activeYear = activeYear
        self.grossVal = grossVal
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.provRaw = aDecoder.decodeObject(forKey: SaveKeys.PROV.rawValue) as? String ?? TaxManager.Province.AB.rawValue
        self.activeYear = aDecoder.decodeObject(forKey: SaveKeys.YEAR.rawValue) as? String ?? "2017"
        self.grossVal = aDecoder.decodeObject(forKey: SaveKeys.GROSS.rawValue) as? Double ?? 0.0
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(provRaw, forKey: SaveKeys.PROV.rawValue)
        aCoder.encode(activeYear, forKey: SaveKeys.YEAR.rawValue)
        aCoder.encode(grossVal, forKey: SaveKeys.GROSS.rawValue)
    }
}

class GrossData: NSObject{
    //MARK: Properties
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("grosscalcdata")
    
    var provNames: [String] {
        get{
            var retArr = [String]()
            for prov: TaxManager.Province in TaxManager.Province.activeProvs {
                retArr.append(taxManager.taxCalcs[prov]!.fullName)
            }
            return retArr
        }
    }
    
    var activeProv = TaxManager.Province.AB
    var activeYear: String
    var grossVal = Double(0.0)
    internal var taxManager: TaxManager
    internal var yearNames: [String]
    internal var provTax = 0.0
    internal var fedTax = 0.0
    internal var netPay = 0.0
    
    //MARK: Initialization
    
    override init(){
        self.taxManager = TaxManager(prov: TaxManager.Province.AB)
        self.yearNames = TaxCSVParser().parseYears()
        activeYear = yearNames[yearNames.count - 1]
        
        super.init()
    }
    
    func loadData() {
        if let loadedStorage = NSKeyedUnarchiver.unarchiveObject(withFile: GrossData.ArchiveURL.path) as? GrossDataStorage {
            activeProv = TaxManager.Province(rawValue: loadedStorage.provRaw)!
            activeYear = loadedStorage.activeYear
            grossVal = loadedStorage.grossVal
            
            taxManager.setProvYear(activeProv, year: activeYear)
        }
    }
    
    func saveData() {
        let storage = GrossDataStorage(provRaw: self.activeProv.rawValue, activeYear: self.activeYear, grossVal: self.grossVal)
        
        if !NSKeyedArchiver.archiveRootObject(storage, toFile: GrossData.ArchiveURL.path) {
            print("Failed to save GrossDataStorage")
        }
    }


    // MARK: Actions
    
    func updateCalc(){
        taxManager.setProvYear(activeProv, year: activeYear)
        let taxReturn = taxManager.getTax(taxableGross: grossVal, taxExempt: 0.0)
        self.fedTax = taxReturn.fedTax
        self.provTax = taxReturn.provTax
        self.netPay = grossVal - fedTax - provTax
    }
}
