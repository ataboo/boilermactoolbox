//
//  RopeData.swift
//  BoilerMac Toolbox
//
//  Created by Alex Raboud on 2016-02-18.
//  Copyright © 2016 atasoft. All rights reserved.
//

import UIKit

struct RopeStats{
    struct RopeType {
        
        let name: String
        let breakStrengthFactor: Float
        let isChain: Bool
    
        static let IWRC = RopeType(name: "Ind. Wire Rope Core",
            breakStrengthFactor: 45, isChain: false)
        
        static let FibreCore = RopeType(name: "Fiber Core Wire Rope",
            breakStrengthFactor: 42, isChain: false)
        
        static let GenericWire = RopeType(name: "Generic Wire Rope",
            breakStrengthFactor: 40, isChain: false)
        
        static let Chain = RopeType(name: "Steel Chain",
            breakStrengthFactor: 24, isChain: true)
        
        static let Nylon = RopeType(name: "Nylon Rope",
            breakStrengthFactor: 12.5, isChain: false)
        
        static let Polyester = RopeType(name: "Polyester Rope",
            breakStrengthFactor: 10, isChain: false)
        
        static let Polypropylene = RopeType(name: "Polypropelene Rope",
            breakStrengthFactor: 7.5, isChain: false)
        
        static let Ethylene = RopeType(name: "Ethylene Rope",
            breakStrengthFactor: 6.25, isChain: false)
        
        static let Manilla = RopeType(name: "Natural Fibre Rope",
            breakStrengthFactor: 5, isChain: false)
        
        static let TypeList = [IWRC, FibreCore, GenericWire, Chain, Nylon, Polyester, Polypropylene, Ethylene, Manilla]
        
        static var IndexOfChain: Int {
            get {
                var index = 0
                for type: RopeType in TypeList {
                    if type.isChain {
                        return index
                    }
                    index += 1
                }
                
                fatalError("RopeDataStore: Couldn't find index of chain in rope types.")
            }
        }
        
        var safetyFactorList: Array<SafetyFactor> {
            get{
                return isChain ? SafetyFactor.ChainFactors : SafetyFactor.RopeFactors
            }
        }
    
        init(name: String, breakStrengthFactor: Float, isChain: Bool) {
            self.name = name
            self.breakStrengthFactor = breakStrengthFactor
            self.isChain = isChain
        }
        
        func WorkingLoadLimit(_ safetyFactor: SafetyFactor) -> Float {
            return breakStrengthFactor / safetyFactor.value
        }
    }
    
    struct SafetyFactor {
        let name: String
        let value: Float
        
        static let Chain = SafetyFactor(name: "Chain (4x)", value: 4)
        static let New = SafetyFactor(name: "New Rope (5x)", value: 5)
        static let Choked = SafetyFactor(name: "Choked Cable (6x)", value: 6)
        static let Used = SafetyFactor (name: "Used Rope (7x)", value: 7)
        static let Old = SafetyFactor(name: "Older Rope (8x)", value: 8)
        static let Manned = SafetyFactor(name: "Men, Ropefall (10x)", value: 10)
        
        static let RopeFactors = [New, Choked, Used, Old, Manned]
        static let ChainFactors = [Chain, Manned]
        
        init(name: String, value: Float) {
            self.name = name
            self.value = value
        }
    }
    
    struct RopeDiameter {
        static let FractionalIncrement = Float(16)
        
        let floatVal: Float
        
        var stringOutput: String {
            get{
                let splitFrac = splitToFraction(floatVal)
                if splitFrac.whole > 0 {
                    if splitFrac.numerator > 0 {
                        return NSString(format: "%.0d-%.0d/%.0d\"", Int(splitFrac.whole), Int(splitFrac.numerator), Int(splitFrac.denominator)) as String
                        
                    } else {
                        return "\(Int(splitFrac.whole))\""
                    }
                }
                
                return NSString(format: "%.0d/%.0d\"", Int(splitFrac.numerator), Int(splitFrac.denominator)) as String
            }
        }
        
        var clipCount: Int {
            let clipCalc = Int(ceil(floatVal * 3 + 1))
            return clipCalc < 3 ? 3 : clipCalc
        }
        
        var clipSpacing: Float {
            return floatVal * 6
        }
        
        init(floatVal: Float) {
            self.floatVal = floatVal
        }
        
        func splitToFraction(_ floatVal: Float) -> (whole: Int, numerator: Int, denominator: Int){
            let whole = Int(floatVal)
            let imperfect = round(floatVal * RopeDiameter.FractionalIncrement)
            var numerator = Int(imperfect - (Float(whole) * RopeDiameter.FractionalIncrement))
            var denominator = Int(RopeDiameter.FractionalIncrement)
            
            if numerator > 0 {
                while numerator % 2 == 0 {
                    numerator /= 2
                    denominator /= 2
                }
            }
            
            return (whole, numerator, denominator)
        }
        
        func breakStrength(_ ropeType: RopeType) -> Float {
            return ropeType.breakStrengthFactor * floatVal * floatVal
        }
        
        func workingLoadLimit(_ ropeType: RopeType, safetyFactor: SafetyFactor) -> Float {
            return ropeType.breakStrengthFactor * floatVal * floatVal / safetyFactor.value
        }
        
        static func diametersInRange(_ startVal: Float, endVal: Float) -> Array<RopeDiameter>{
            var ropeDiameters = Array<RopeDiameter>()
            
            let startNumerator = Int(round(startVal * RopeDiameter.FractionalIncrement))
            let endNumerator = Int(round(endVal * RopeDiameter.FractionalIncrement))
            
            if endNumerator < startNumerator || startNumerator < 0 {
                print("RopeDiameter passed bad range")
                
                return ropeDiameters
            }
            
            for index in startNumerator...endNumerator {
                let ropeDiameter = RopeDiameter(floatVal: Float(index) / RopeDiameter.FractionalIncrement)
                ropeDiameters.append(ropeDiameter)
                //print("Added \(ropeDiameter.stringOutput) with val \(ropeDiameter.floatVal) to rope diameters.")
            }
            
            return ropeDiameters
        }
    }
}

enum SaveKeys: String {
    case activeRopeType = "ropeData_lastRopeType"
    case activeDiameterVal = "ropeData_lastDiameterVal"
    case activeSafetyFactor = "ropeData_lastSafetyFactor"
}

@objc
class RopeDataStore: NSObject {
    var activeRopeTypeIndex = 0 {
        // When RopeType is changed chain factor list is shorter so prevent out of range
        didSet {
            if activeRopeTypeIndex == RopeStats.RopeType.IndexOfChain &&
                activeSafetyFactorIndex >= RopeStats.SafetyFactor.ChainFactors.count {
                activeSafetyFactorIndex = 0
            }
        }
    }
    var activeDiameterVal = Float(0.5)
    var activeSafetyFactorIndex = 0
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("ropeCalcData")
    
    required convenience init?(coder aDecoder: NSCoder){
        self.init()
        
        nsCodeInit(coder: aDecoder)
    }
    
    func ropeTypeNames() -> Array<String> {
        var ropeNames = Array<String>()
        for ropeType: RopeStats.RopeType in RopeStats.RopeType.TypeList {
            ropeNames.append(ropeType.name)
        }
        
        return ropeNames
    }
    
    func valAtIndex(_ index: Int) -> RopeStats.RopeType {
        let ropeVals = RopeStats.RopeType.TypeList
        
        if index < 0 || ropeVals.count <= index {
            fatalError("RopeData: valAtIndex out of range at \(index).")
        }
        
        return ropeVals[index]
    }
    
    func standardDiameters() -> Array<RopeStats.RopeDiameter> {
        return RopeStats.RopeDiameter.diametersInRange(Float(0.25), endVal: Float(3))
    }
    
    func standardDiameterStrings() -> Array<String> {
        var diameterNames = Array<String>()
        
        for diameter: RopeStats.RopeDiameter in standardDiameters(){
            diameterNames.append(diameter.stringOutput)
        }
        
        return diameterNames
    }
    
    func safetyFactorNames() -> Array<String> {
        var factorNames = Array<String>()
        let factorList = activeRopeTypeIndex == RopeStats.RopeType.IndexOfChain ?
            RopeStats.SafetyFactor.ChainFactors : RopeStats.SafetyFactor.RopeFactors
        for factor: RopeStats.SafetyFactor in factorList {
            factorNames.append(factor.name)
        }
        
        return factorNames
    }
}

extension RopeDataStore: TableData {
    func fullNameList() -> Array<String> {
        var nameList = Array<String>()
        for val: RopeStats.RopeType in RopeStats.RopeType.TypeList {
            nameList.append(val.name)
        }
        
        return nameList
    }
    
    func nameAtIndex(_ index: Int) -> String {
        let nameList = fullNameList()
        if index < 0 || nameList.count <= index  {
            return "Out of Index."
        }
        
        return fullNameList()[index]
    }
    
    func lastSelectedIndex() -> Int {
        return activeRopeTypeIndex
    }
    
}

extension RopeDataStore: NSCoding {
    fileprivate func nsCodeInit(coder aDecoder: NSCoder){
        if aDecoder.containsValue(forKey: SaveKeys.activeRopeType.rawValue) {
        activeRopeTypeIndex = aDecoder.decodeInteger(forKey: SaveKeys.activeRopeType.rawValue)
        } else {
            print("No value for lastRopeType raw value.  Insantiating defaults.")
            return
        }

        // Only checking for key existing for first value.
        activeSafetyFactorIndex = aDecoder.decodeInteger(forKey: SaveKeys.activeSafetyFactor.rawValue)
        
        activeDiameterVal = aDecoder.decodeFloat(forKey: SaveKeys.activeDiameterVal.rawValue)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(activeRopeTypeIndex, forKey: SaveKeys.activeRopeType.rawValue)
        aCoder.encode(activeDiameterVal, forKey: SaveKeys.activeDiameterVal.rawValue)
        aCoder.encode(activeSafetyFactorIndex, forKey: SaveKeys.activeSafetyFactor.rawValue)
    }
}
