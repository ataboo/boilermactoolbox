import Foundation

class ONTaxCalc: TaxCalc {
    var province: TaxManager.Province = .ON
    var fullName: String = "Ontario"
    var taxFromProvince: TaxManager.Province? = nil
    var wageFromProvince: TaxManager.Province? = nil
    var yearString: String = TaxManager.defaultYearName

    func provTax(anGross: Double) -> Double {
        let provVal = provTaxVal
        
        // Seems like their tax law is too simple.  Should kick it up a couple notches.
        let basicTax = basicProvincial(anGross: anGross)
        
        let surLowBracket = Double(provVal.surtax![0])
        let surHighBracket = Double(provVal.surtax![1])
        let surLowRate = Double(provVal.surtax![2])
        let surHighRate = Double(provVal.surtax![3])
        
        var surtax = max(basicTax - surLowBracket, 0.0) * surLowRate
        surtax += max(basicTax - surHighBracket, 0.0) * surHighRate
        
        let healthIndex = getBracketIndex(anGross, brackets: provVal.healthBrackets!)
        var healthPrem = 0.0
        if healthIndex > 0 {
            let healthAmount = Double(provVal.healthAmounts![healthIndex])
            let healthRate = Double(provVal.healthRates![healthIndex])
            let healthPremCalc = healthRate * (anGross - Double(provVal.healthBrackets![healthIndex]))
            healthPrem = healthAmount < healthPremCalc ? healthAmount: healthPremCalc
        }
        
        var taxReduction = 2 * Double(provVal.taxRed![0]) < basicTax + surtax ? 2 * Double(provVal.taxRed![0]) : basicTax + surtax
        taxReduction = max(taxReduction - basicTax + surtax, 0.0) //Can't make this stuff up
        
        return basicTax + surtax + healthPrem - taxReduction
    }
    
    func fedTax(anGross: Double) -> Double {
        return basicFedTax(anGross: anGross)
    }
    
    func cppEi(anGross: Double, fedVal: TaxVal) -> (cppVal: Double, eiVal: Double) {
        return basicCppEi(anGross, fedVal: fedVal)
    }
}


