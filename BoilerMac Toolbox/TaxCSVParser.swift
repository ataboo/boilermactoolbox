import Foundation

class TaxCSVParser {
    enum CsvTags: String{
        case BRACKETS = "#brackets"
        case CONST_K = "#const_k"
        case CLAIM = "#claim_amount"
        case CLAIM_NS = "#claim_ns"
        case CPP_EI = "#cpp_ei"
        case HEALTH_BRACKET = "#health_brackets"
        case HEALTH_RATE = "#health_rates"
        case HEALTH_AMOUNT = "#health_amounts"
        case RATES = "#rates"
        case SURTAX = "#surtax"
        case TAX_RED = "#tax_red"
        case VAC_RATE = "#vac_rate"
        case WAGES = "#wages"
        case YEARS = "#years"
        case FIELD_DUES = "#field_dues"
        case MONTH_DUES = "#month_dues"
        case NIGHT_OT = "#night_ot"
        case NIGHT_PREM = "#night_prem"
        case QPP_RATE = "#qpp_rate"
        case QPP_MAX = "#qpp_max"
        case QPIP_RATE = "#qpip_rate"
        case QPIP_MAX = "#qpip_max"
        case EMP_DEDUCTION = "#emp_ded"
        case PAY_WEEKDAY = "#pay_weekday"
        case PAY_WEEKEND = "#pay_weekend"
        case PAY_HOLIDAY = "#pay_holiday"
        case PAY_FT_WEEKDAY = "#pay_ft_weekday"
        case PAY_FT_FRIDAY = "#pay_ft_friday"
        case LEVY_BRACKETS = "#levy_brackets"
        case LEVY_BASE = "#levy_base"
        case LEVY_RATE = "#levy_rate"
    }
    
    private var lineMap: Dictionary<String, [String]>!

    public func parseYears() -> [String] {
        var years = [String]()
        let valArray = csvToArray(path: FedTaxCalc().getCSVPath()!)
        
        for line: String in valArray!{
            
            if line.contains(CsvTags.YEARS.rawValue) {
                let lineSplit = line.components(separatedBy: ",")
                
                for year: String in lineSplit[1..<lineSplit.count] {
                    years.append(year)
                }
                return years.sorted()
            }
        }
        
        fatalError("TaxVal couldn't find years in FED csv.")
    }
    
    public func parseTaxVal(taxCalc: TaxCalc, year: Int) -> TaxVal {
        guard let csvRaw = csvToArray(taxCalc: taxCalc) else {
            fatalError("Failed to load taxval.")
        }
        
        self.lineMap = makeValMap(csvRaw)
        var taxVal = TaxVal(province: taxCalc.province, year: year)
        
        taxVal.brackets = multiYearRow(tag: CsvTags.BRACKETS, yearIndex: year)
        taxVal.constK = multiYearRow(tag: CsvTags.CONST_K, yearIndex: year)
        taxVal.cppEi = multiYearRow(tag: CsvTags.CPP_EI, yearIndex: year)
        
        taxVal.healthRates = multiYearRow(tag: CsvTags.HEALTH_RATE, yearIndex: year)
        taxVal.healthBrackets = multiYearRow(tag: CsvTags.HEALTH_BRACKET, yearIndex: year)
        taxVal.healthAmounts = multiYearRow(tag: CsvTags.HEALTH_AMOUNT, yearIndex: year)
        taxVal.rates = multiYearRow(tag: CsvTags.RATES, yearIndex: year)
        taxVal.surtax = multiYearRow(tag: CsvTags.SURTAX, yearIndex: year)
        taxVal.taxRed = multiYearRow(tag: CsvTags.TAX_RED, yearIndex: year)
        taxVal.claimNs = multiYearRow(tag: CsvTags.CLAIM_NS, yearIndex: year)
        
        taxVal.claimAmount = singleYearRow(tag: .CLAIM, index: year)
        taxVal.qppRate = singleYearRow(tag: .QPP_RATE, index: year)
        taxVal.qppMax = singleYearRow(tag: .QPP_MAX, index: year)
        taxVal.qpipRate = singleYearRow(tag: .QPIP_RATE, index: year)
        taxVal.qpipMax = singleYearRow(tag: .QPIP_MAX, index: year)
        taxVal.empDeduction = singleYearRow(tag: .EMP_DEDUCTION, index: year)
        
        taxVal.payWeekday = intRow(tag: .PAY_WEEKDAY)
        taxVal.payWeekend = intRow(tag: .PAY_WEEKEND)
        taxVal.payHoliday = intRow(tag: .PAY_HOLIDAY)
        taxVal.payFTWeekday = intRow(tag: .PAY_FT_WEEKDAY)
        taxVal.payFTFriday = intRow(tag: .PAY_FT_FRIDAY)
        
        taxVal.vacRate = singleNumber(tag: CsvTags.VAC_RATE)
        taxVal.fieldDuesRate = singleNumber(tag: CsvTags.FIELD_DUES)
        taxVal.monthlyDuesRate = singleNumber(tag: CsvTags.MONTH_DUES)
        taxVal.nightsRate = singleNumber(tag: CsvTags.NIGHT_PREM)
        taxVal.nightOT = singleBool(tag: CsvTags.NIGHT_OT)
        taxVal.wages = setWages(lineMap)
        
        taxVal.levyBrackets = multiYearRow(tag: CsvTags.LEVY_BRACKETS, yearIndex: year - 5)
        taxVal.levyBase = multiYearRow(tag: CsvTags.LEVY_BASE, yearIndex: year - 5)
        taxVal.levyRate = singleYearRow(tag: CsvTags.LEVY_RATE, index: year - 5)
        
        if let taxDonorProvince = taxCalc.taxFromProvince {
            let taxDonor = TaxManager(prov: taxDonorProvince).taxCalcs[taxDonorProvince]!
            taxVal = addTaxFromProv(taxVal, donor: taxDonor)
        }
        
        if let taxDonorProvince = taxCalc.taxFromProvince {
            let taxDonor = TaxManager(prov: taxDonorProvince).taxCalcs[taxDonorProvince]!
            taxVal = addTaxFromProv(taxVal, donor: taxDonor)
        }
        
        return taxVal
    }
    
    private func csvToArray(taxCalc: TaxCalc) -> [String]? {
        guard let path = taxCalc.getCSVPath() else {
            fatalError("CSV not found for taxCals")
        }
        
        return csvToArray(path: path)
    }
    
    private func csvToArray(path: String) -> [String]? {
        do {
            let rawString = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            return rawString.replacingOccurrences(of: "\r", with: "").components(separatedBy: "\n")
        } catch let e as NSError {
            print(e)
            return nil
        }
    }
    
    private func makeValMap(_ csvRaw: [String]) -> Dictionary<String, [String]> {
        var valMap = Dictionary<String, [String]>()
        for csvLine: String in csvRaw {
            if csvLine.range(of: "#") == nil {
                continue
            }
            
            let lineSplit = csvLine.components(separatedBy: ",")
            let lineVals = lineSplit[1..<lineSplit.count].joined(separator: ",")
            let lineKey = lineSplit[0]
            valMap[lineKey] = valMap[lineKey] ?? [String]()
            valMap[lineKey]! += [lineVals]
        }
        
        return valMap
    }
    
    private func multiYearRow(tag: CsvTags, yearIndex: Int) -> [Float]?{
        if let lines = lineMap[tag.rawValue], let line = lines[safe: yearIndex] {
            return floatsFromLine(line)
        }
        return nil
    }
    
    private func singleYearRow(tag: CsvTags, index: Int) -> Float? {
        if let line = lineMap[tag.rawValue]{
            let floatArr = floatsFromLine(line[0])
            return floatArr[index]
        }
        return nil
    }
    
    private func singleNumber(tag: CsvTags) -> Float? {
        if let rowString = singleYearRow(tag: tag, index: 0) {
            return Float(rowString)
        }
        return nil
    }
    
    private func singleBool(tag: CsvTags) -> Bool {
        if let lines = lineMap[tag.rawValue] {
            return lines[0].contains("TRUE")
        }
        
        return false
    }
    
    private func intRow(tag: CsvTags) -> [Int]? {
        if let lines = lineMap[tag.rawValue] {
            return lines[0].components(separatedBy: ",").map{Int($0) ?? 0}
        }
        
        return nil
    }
    
    private func floatsFromLine(_ line: String) -> [Float] {
        let splitLine = line.components(separatedBy: ",")
        var floatArr = [Float]()
        for str: String in splitLine {
            if let parsedFl = Float(str){
                floatArr.append(parsedFl)
            }
        }
        return floatArr
    }
    
    private func setWages(_ lineDic: Dictionary<String, [String]>) -> OrderedDictionary<String, Double>? {
        
        if var wageLines = lineDic[CsvTags.WAGES.rawValue]{
            wageLines = wageLines.reversed()
            var wageVals = OrderedDictionary<String, Double>()
            for wageLine: String in wageLines{
                let splitLine = wageLine.components(separatedBy: ",")
                if let parsedRate = Double(splitLine[1]) {
                    wageVals[splitLine[0]] = parsedRate
                } else {
                    fatalError("Failed to parse wage \(splitLine)")
                }
            }
            // Appended custom rate
            wageVals["Custom Wage"] = 0.0
            
            return wageVals
        }
        return nil
    }
    
    fileprivate static func trimFirstElement(_ splitLine: [String]) -> [String] {
        return Array(splitLine[1..<splitLine.count])
    }
    
    private func addTaxFromProv(_ receiver: TaxVal, donor: TaxCalc) -> TaxVal {
        let taxDonor = TaxValRepo.instance.getProv(prov: donor, yearIndex: receiver.yearIndex)
        var combined = receiver
        
        combined.brackets = taxDonor.brackets
        combined.claimAmount = taxDonor.claimAmount
        combined.constK = taxDonor.constK
        // omitted CppEi as is only federal
        combined.healthRates = taxDonor.healthRates
        combined.healthBrackets = taxDonor.healthBrackets
        combined.healthAmounts = taxDonor.healthAmounts
        combined.rates = taxDonor.rates
        combined.surtax = taxDonor.surtax
        combined.taxRed = taxDonor.taxRed
        
        return combined
    }
    
    private func addWagesFromProv(_ receiver: TaxVal, donor: TaxCalc) -> TaxVal {
        let wageDonor = TaxValRepo.instance.getProv(prov: donor, yearIndex: receiver.yearIndex)
        var combined = receiver
        
        combined.wages = wageDonor.wages
        combined.vacRate = wageDonor.vacRate
        combined.fieldDuesRate = wageDonor.fieldDuesRate
        combined.monthlyDuesRate = wageDonor.monthlyDuesRate
        combined.nightsRate = wageDonor.nightsRate
        combined.nightOT = wageDonor.nightOT
        
        return combined
    }
}
